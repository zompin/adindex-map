var divisions = [{
	name: 'All screens & All media',
	id: 1,
	image: 'asam.png',
	color: '#008200',
	icon: 'asam.png',
	iconSize: '90px auto',
	position: {
		top: 160,
		left: 70
	},
	button: {
		top: 194,
		left: 122,
		width: 273,
		height: 115
	}
}, {
	name: 'Performance Marketing',
	id: 2,
	image: 'pm.png',
	color: '#5a6575',
	icon: 'pm.png',
	iconSize: '85px auto',
	position: {
		top: 60,
		left: 1930,
	},
	button: {
		top: 91,
		left: 1955,
		width: 272,
		height: 207
	}
}, {
	name: 'Big Data',
	id: 3,
	image: 'bg.png',
	color: '#c97f77',
	icon: 'bg.png',
	iconSize: '91px auto',
	position: {
		top: 1120,
		left: 0
	},
	button: {
		top: 1157,
		left: 1087,
		width: 226,
		height: 115
	}
}, {
	name: 'Mobile Solutions',
	id: 4,
	image: 'ms.png',
	color: '#17a1e5',
	icon: 'ms.png',
	iconSize: '89px auto',
	position: {
		top: 900,
		left: 1720
	},
	button: {
		top: 930,
		left: 1741,
		width: 227,
		height: 114
	}
}];