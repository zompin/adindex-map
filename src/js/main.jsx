class App extends React.Component {
	constructor(props) {
		super(props);
		this.showPopup = this.showPopup.bind(this);
		this.hidePopup = this.hidePopup.bind(this);
		this.toggleLegend = this.toggleLegend.bind(this);
		this.resizeApp = this.resizeApp.bind(this);
		this.hideByClickArea 	= this.hideByClickArea.bind(this);
		this.divisions  = divisions;
		this.categories = categories;
		this.logos = logos;
		this.popupCompany = {};
		this.popupCategory = {};
		this.popupDivision = {};
		this.baseImageDir = '/imgs/'

		this.state = {
			companies: companies,
			zoom: 0.7,
			newZoom: 0.7,
			left: 0,
			top: 0,
			showPopup: false,
			showMap: false,
			showDivisions: false,
			showMobileDivisions: true,
			currentDivision: null,
			currentCat: 0,
			showLegend: false,
			mode: 'map',
			showLogo: true,
			showSearch: false
		};
	}

	showPopup(e, company) {
		let id;
		let companies = this.state.companies;

		if (e) {
			id = e.currentTarget.getAttribute('data-id');
		} else {
			id = company;
		}

		for (let i = 0; i < companies.length; i++) {
			if (companies[i].id == id && !companies[i].favorite) {
				this.popupCompany = companies[i];

				this.setState({
					showPopup: true,
				});

				break;
			} else if (companies[i].id == id && companies[i].favorite) {

				if (this.state.mode == 'list') {
					window.open(companies[i].url);
				} else {
					if (companies[i].showCaption) {
						window.open(companies[i].url);
					}
					companies[i].showCaption = !companies[i].showCaption;
				}

				this.setState({
					companies: companies
				});

				break;
			}
		}

		if (this.popupCompany.id) {
			this.popupCategory = getItemById(this.categories, this.popupCompany.categoryId);
		} else {
			this.popupCategory = {};
		}

		if (this.popupCategory) {
			this.popupDivision = getItemById(this.divisions, this.popupCategory.divisionId);
		} else {
			this.popupDivision = {};
		}

		if (e) {
			e.preventDefault();
		}

		this.setState({
			showLegend: false
		});
	}

	hidePopup() {
		this.setState({showPopup: false});
	}

	toggleLegend() {
		let show = this.state.showLegend;

		this.setState({
			showLegend: !show
		});
	}

	resizeApp() {
		if (isMobile()) {
			this.setState({
				showMap: false,
				showDivisions: false,
				showMobileDivisions: true
			});
		} else {
			this.setState({
				showMap: true,
				showDivisions: true,
				showMobileDivisions: false
			});
		}
	}

	hideByClickArea(e) {
		let currentElem = e.target;
		let className;

		try {
			do {
				className = currentElem.className;

				if (className) {
					if (className.indexOf('search') != -1) {
						return;
					}

					if (className.indexOf('legend') != -1) {
						return;
					}
				}

			} while (currentElem = currentElem.parentNode);
		} catch (e) {
			return;
		}

		this.setState({
			showSearch: false,
			showLegend: false
		});
	}

	componentWillMount() {
		window.addEventListener('resize', this.resizeApp, false);
		document.addEventListener('click', this.hideByClickArea, false);
		this.resizeApp();
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.resizeApp, false);
		document.removeEventListener('click', this.hideByClickArea, false);
	}

	render() {
		return (
			<div>
				<Header 			app = {this} />
				<Map 				app = {this} />
				<Divisions 			app = {this} />
				<Popup 				app = {this} />
				<MobileDivisions 	app = {this} />
				<Control 			app = {this} />
				<Legend 			app = {this} />
			</div>
		);
	}
};

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.Change 			= this.Change.bind(this);
		this.Enter 				= this.Enter.bind(this);
		this.Leave 				= this.Leave.bind(this);
		this.ItemClick 			= this.ItemClick.bind(this);
		this.ItemLeave 			= this.ItemLeave.bind(this);
		this.Click 				= this.Click.bind(this);
		this.focus 				= this.focus.bind(this);
		this.filterCompanies 	= this.filterCompanies.bind(this);
		this.goToDivision 		= this.goToDivision.bind(this);
		this.clear 				= this.clear.bind(this);
		this.app 				= props.app;

		this.state = {
			search: '',
			searchList: [],
			searchPlaceholder: '',
			showSearchList: false
		}
	}

	Change(e) {
		let searchList = this.filterCompanies(e.target.value);
		let companies = this.app.state.companies;
		this.setState({
			search: e.target.value,
			searchList: searchList,
			searchPlaceholder: '',
			showSearchList: true
		});

		if (e.target.value.length == 0) {
			for (let i = 0; i < companies.length; i++) {
				companies[i].showCircle = false;
				companies[i].showCaption = false;
			}

			this.app.setState({
				companies: companies
			});
		}
	}

	Enter(e) {
		let id = e.target.getAttribute('data-id');
		let companies = this.app.state.companies;

		for (let i = 0; i < companies.length; i++) {
			if (id == companies[i].id) {
				this.setState({
					searchPlaceholder: companies[i].name,
				});

				companies[i].showCircle = true;

				this.app.setState({
					companies: companies
				});
				break;
			}
		}
	}

	Leave() {
		this.setState({searchPlaceholder: ''});
	}

	Click(e) {
		let show = !this.app.state.showSearch;
		this.app.setState({showSearch: show});
		e.preventDefault();

		if (show && this.state.search) {
			this.setState({
				showSearchList: true
			});
		}
	}

	ItemClick(e) {
		let id 			= e.target.getAttribute('data-id') || e.target.parentNode.getAttribute('data-id');
		let companies 	= this.app.state.companies;
		let contHalfWidth;
		let contHalfHeight;
		let zoom = this.app.state.zoom;
		let left;
		let top;

		if (this.app.container) {
			contHalfWidth = this.app.container.clientWidth / 2;
			contHalfHeight = this.app.container.clientHeight / 2;
		} else  {
			contHalfWidth = 0;
			contHalfHeight = 0;
		}

		for (let i = 0; i < companies.length; i++) {
			if (companies[i].id == id) {
				left = contHalfWidth / zoom - companies[i].position.left;
				top = contHalfHeight / zoom - companies[i].position.top;

				if (isMobile()) {
					for (let j = 0; j < this.app.categories.length; j++) {
						if (companies[i].categoryId == this.app.categories[j].id) {
							
							for (let k = 0; k < this.app.divisions.length; k++) {
								if (this.app.categories[j].divisionId == this.app.divisions[k].id) {
									this.app.popupCompany = companies[i];
									this.app.setState({currentDivision: this.app.divisions[k].id});
								}
							}

							break;
						}
					}
				} else {
					if (this.app.state.mode == 'list') {
						this.app.showPopup(false, companies[i].id);
					} else {

						this.app.setState({
							left: left,
							top:  top,
							zoom: zoom
						});
					}
				}

				companies[i].showCircle  = true;
				companies[i].showCaption = true;
			} else {
				companies[i].showCircle  = false;
				companies[i].showCaption = false;
			}
		}

		this.app.state.currentCompany = id;

		this.app.setState({
			companies: companies
		});

		this.setState({
			showSearchList: false
		});
	}

	ItemLeave(e) {
		let id = e.target.getAttribute('data-id');
		let companies = this.app.state.companies;

		for (let i = 0; i < companies.length; i++) {
			if (companies[i].id == id) {
				companies[i].showCircle = false;

				this.app.setState({
					companies: companies
				});

				break;
			}
		}
	}

	filterCompanies(value) {
		let list 		= [];
		let i 			= 0;
		let count 		= 0;
		let companies 	= this.app.state.companies;
		let categories 	= this.app.categories;

		while (value.length && i < companies.length && count < 10) {
			if (companies[i].name.toLowerCase().indexOf(value.toLowerCase()) == 0) {
				list.push(companies[i]);
				count++;
			}
			i++;
		}

		for (let i = 0; i < list.length; i++) {
			for (let j = 0; j < categories.length; j++) {
				if (list[i].categoryId == categories[j].id) {
					list[i].categoryName = categories[j].nameRu;
					break;
				}
			}
		}

		return list;
	}

	goToDivision(e) {
		let id = e.currentTarget.getAttribute('data-id');
		let mode = this.app.state.mode;
		let zoom = this.app.state.zoom;
		let container = this.app.container;
		let division = this.app.divisions;
		let categories = this.app.categories;
		let companies = this.app.state.companies;
		let contHalfWidth;
		let contHalfHeight;
		let left;
		let top;
		let divisionE;
		let offsetTop;
		let headerE;
		let headerHeight;
		let navE = document.querySelectorAll('.header__nav-a:not(.header__nav-a_search)');

		e.preventDefault();

		if (mode == 'list') {
			divisionE = document.getElementById('division-' + id);
			headerE = document.querySelector('.header');
			offsetTop = divisionE.offsetTop;
			headerHeight = headerE.clientHeight;
			offsetTop -= headerHeight;

			window.scroll({
				top: offsetTop,
				left: 0,
				behavior: 'smooth'
			});
		} else {
			contHalfWidth = container.clientWidth / 2;
			contHalfHeight = container.clientHeight / 2;

			for (let i = 0; i < divisions.length; i++) {
				if (divisions[i].id == id) {
					left = divisions[i].position.left * -1;
					top = divisions[i].position.top * -1;
					break;
				}
			}

			for (let i = 0; i < categories.length; i++) {
				if (categories[i].divisionId == id) {
					for (let j = 0; j < companies.length; j++) {
						if (companies[j].categoryId == categories[i].id) {
							companies[j].showCircle = false;
							companies[j].showCaption = true;
						}
					}
				} else {
					for (let j = 0; j < companies.length; j++) {
						if (companies[j].categoryId == categories[i].id) {
							companies[j].showCircle = false;
							companies[j].showCaption = false;
						}
					}
				}
			}

			this.app.setState({
				top: top,
				left: left,
				companies: companies,
				currentDivision: id
			});
		}

		for (let i = 0; i < navE.length; i++) {
			navE[i].classList.remove('header__nav-a_current');
		}

		e.target.classList.add('header__nav-a_current');
	}

	focus() {
		this.setState({
			showSearchList: true
		});
	}

	clear() {
		let companies = this.app.state.companies;

		for (let i = 0; i < companies.length; i++) {
			companies[i].showCircle = false;
			companies[i].showCaption = false;
		}

		this.setState({
			search: '',
			showSearchList: false,
			searchList: []
		});

		this.app.setState({
			companies: companies
		});
	}

	render() {
		let divisions = this.app.divisions;
		divisions = divisions.map(division => {
			return (
				<a href = "#" className = "header__nav-a" data-id = {division.id} onClick = {this.goToDivision} >
					{division.name}
				</a>
			);
		});

		return (
			<header className = "header">
				<a href="/" className = "header__logo">&nbsp;</a>
				<nav className = "header__nav">
					{divisions}
					<a href="#" className = "header__nav-a header__nav-a_search" onClick = {this.Click} >&nbsp;</a>
					<Search
						show = {this.app.state.showSearch}
						showList = {this.state.showSearchList}
						value = {this.state.search} 
						list = {this.state.searchList}
						placeholder = {this.state.searchPlaceholder}
						change = {this.Change}
						focus = {this.focus}
						enter = {this.Enter}
						leave = {this.Leave}
						click = {this.ItemClick}
						itemleave = {this.ItemLeave}
						clear = {this.clear}
					/>
				</nav>
			</header>
		);
	}
};

function Search(props) {
	let placeholder = props.placeholder;
	let value 		= props.value;
	let style 		= {display: props.show ? 'block' : 'none'};
	let count 		= props.list.length;
	let listClasses = 'search__list';
	let clearClasses = 'search__clear';
	let list 		= props.list.map(company => {
		let name 	= company.name.substr(value.length);

		return (
			<button
				className = "search__list-item"
				data-id = {company.id}
				onMouseEnter = {props.enter}
				onClick = {props.click}
				onMouseLeave = {props.itemleave}
			>
				<span className = "search__list-item-company" >
					<b>{value}</b>{name}
				</span>
				<span className = "search__list-item-category">
					{company.categoryName}
				</span>
			</button>
		);
	});

	if (value.length) {
		switch (count % 10) {
			case 0:
			case 9:
			case 8:
			case 7:
			case 6:
			case 5:
				count = count + " результатов поиска";
			break;
			case 4:
			case 3:
			case 2:
				count = count + " результата поиска";
			break;
			case 1:
				count = count + " результат поиска";
			break;
		}
	} else {
		count = '';
	}

	if (props.showList) {
		listClasses += ' search__list_show';
	}

	if (value.length) {
		clearClasses += ' search__clear_show';
	}

	return (
		<div className = "search" style = {style} >
			<div className = "search__placeholder">
				{placeholder}
			</div>
			<div className = "search__count">
				{count}
			</div>
			<input
				className 	= "search__input"
				type 		= "text"
				value 		= {value}
				onChange 	= {props.change}
				onFocus 	= {props.focus}
				placeholder = "Найти компанию"
			/>
			<div className = {listClasses} onMouseLeave = {props.leave} >{list}</div>
			<button className = {clearClasses} onClick = {props.clear} ></button>
		</div>
	);
}

class Map extends React.Component {
	constructor(props) {
		super(props);
		this.app 			= props.app;
		this.Wheel 			= this.Wheel.bind(this);
		this.mouseDown 		= this.mouseDown.bind(this);
		this.mouseUp 		= this.mouseUp.bind(this);
		this.mouseMove 		= this.mouseMove.bind(this);
		this.mouseOut 		= this.mouseOut.bind(this);
		this.checkTop 		= this.checkTop.bind(this);
		this.getMinTop 		= this.getMinTop.bind(this);
		this.checkLeft 		= this.checkLeft.bind(this);
		this.getMinLeft 	= this.getMinLeft.bind(this);
		this.catClick 		= this.catClick.bind(this);
		this.areaClick 		= this.areaClick.bind(this);
		this.startDrag 		= false;
		this.offsetLeft 	= 0;
		this.offsetTop 		= 0;
		this.zoomStep 		= 0.05;
	}

	Wheel(e) {
		let zoom 		= this.app.state.zoom;
		let zoomStep 	= this.zoomStep;
		let newZoom;

		if (e.deltaY < 0) {
			newZoom = zoom + zoomStep;
		} else {
			newZoom = zoom - zoomStep;
		}

		newZoom = newZoom.toFixed(2) * 1;

		this.app.setState({
			newZoom: newZoom
		});

		e.preventDefault();
	}

	mouseDown(e) {
		this.startDrag 	= true;
		this.offsetLeft = e.pageX - this.app.state.left * this.app.state.zoom;
		this.offsetTop 	= e.pageY - this.app.state.top  * this.app.state.zoom;

		e.preventDefault();
	}

	mouseUp(e) {
		this.startDrag = false;
	}

	mouseMove(e) {
		let top;
		let left;

		if (this.startDrag) {
			top  	= (e.pageY - this.offsetTop)  / this.app.state.zoom;
			left 	= (e.pageX - this.offsetLeft) / this.app.state.zoom;

			this.app.setState({
				top:  top,
				left: left
			});
		}
	}

	mouseOut(e) {
		this.startDrag = false;
	}

	catClick(e) {
		let catId = e.target.getAttribute('data-category-id') || e.target.parentNode.getAttribute('data-category-id');
		let companies;
		let currentCat = this.app.state.currentCat;

		if (!catId) {
			return false;
		}


		companies = this.app.state.companies.map(company => {
			if (company.categoryId != catId || catId == currentCat) {
				company.showCaption = false;
				company.showCircle = false;
			} else {
				company.showCaption = true;
			}

			return company;
		});

		if (catId == currentCat) {
			catId = 0;
		}

		this.app.setState({
			companies: companies,
			currentCat: catId
		});
	}

	areaClick(e) {
		let id = e.target.getAttribute('data-id');
		let currentDivision = this.app.state.currentDivision;
		let categories = this.app.categories;
		let companies = this.app.state.companies;

		for (let i = 0; i < categories.length; i++) {
			if (categories[i].divisionId == id) {
				for (let j = 0; j < companies.length; j++) {
					if (companies[j].categoryId == categories[i].id) {
						companies[j].showCircle = false;
						companies[j].showCaption = true;
					}
				}
			} else {
				for (let j = 0; j < companies.length; j++) {
					if (companies[j].categoryId == categories[i].id) {
						companies[j].showCircle = false;
						companies[j].showCaption = false;
					}
				}
			}
		}

		if (currentDivision == id) {
			for (let j = 0; j < companies.length; j++) {
				companies[j].showCircle = false;
				companies[j].showCaption = false;
			}

			id = 0;
		}

		this.app.setState({
			currentDivision: id
		});
	}

	checkTop(top) {
		let maxTop = 0;
		let minTop;

		minTop 	= this.getMinTop();

		if (top > maxTop) top  = maxTop;
		if (top < minTop) top  = minTop;

		return top;
	}

	getMinTop(zoom) {
		let minTop = 0;

		if (zoom === undefined) {
			zoom = this.app.state.zoom;
		}

		minTop = this.container.clientHeight / zoom - this.map.clientHeight;

		return minTop;
	}

	checkLeft(left) {
		let maxLeft = 0;
		let minLeft;
		let zoom = this.app.state.zoom;

		minLeft = this.getMinLeft();

		if (left > maxLeft) left = maxLeft;
		if (left < minLeft) left = minLeft;

		return left;
	}

	getMinLeft(zoom) {
		let minLeft = 0;

		if (zoom === undefined) {
			zoom = this.app.state.zoom;
		}

		minLeft = this.container.clientWidth /zoom - this.map.clientWidth;

		return minLeft;
	}

	render() {
		let zoom = this.app.state.zoom;
		let newZoom = this.app.state.newZoom;
		let left = this.app.state.left;
		let top  = this.app.state.top;
		let contWidth;
		let contHeight;
		let mapWidth;
		let mapHeight;
		let companies = [];
		let categories = [];
		let areas = [];
		let zoomCond;
		let mapClasses = 'map';

		if (!this.app.state.showMap) {
			return (
				<div />
			);
		}

		if (this.container && this.map) {
			contWidth 	= this.container.clientWidth;
			contHeight 	= this.container.clientHeight;
			mapWidth 	= this.map.clientWidth;
			mapHeight 	= this.map.clientHeight;
		}

		if (newZoom > zoom) {
			if (newZoom > 1) {
				newZoom = zoom;
			}
		}

		if (newZoom < zoom) {
			zoomCond = (contWidth / (newZoom) < mapWidth) && (contHeight / (newZoom) < mapHeight);

			if (!zoomCond) {
				newZoom = zoom;
			}
		}

		if (newZoom != this.app.state.zoom) {
			left = contWidth / 2 / newZoom - (Math.abs(left) + contWidth / 2 / zoom);
			top = contHeight / 2 / newZoom - (Math.abs(top) + contHeight / 2 / zoom);

			this.app.setState({zoom: newZoom});
		}

		if (this.map) {
			left = this.checkLeft(left);
			top  = this.checkTop(top);
			left = left.toFixed(0) * 1;
			top = top.toFixed(0) * 1;

			this.app.state.left = left;
			this.app.state.top  = top;
		}

		let style = {
			transform: 'scale(' + zoom + ') translateX(' + left + 'px) translateY(' + top + 'px)'
		}

		areas = this.app.divisions.map(area => <MapArea data = {area} click = {this.areaClick} />)
		categories = this.app.categories.map(category => <MapButton data = {category} click = {this.catClick} />);
		companies = this.app.state.companies.map(company => <Car app = {this.app} data = {company} click = {this.app.showPopup} />);

		if (this.app.state.mode == 'list') {
			return (
				<div />
			);
		}

		return (
			<div>
				<div 
					className 	= "map-container"
					onWheel 	= {this.Wheel}
					onMouseMove = {this.mouseMove}
					onMouseLeave 	= {this.mouseOut}
					ref 		= {container => this.app.container = this.container = container}
				>
					<div
						className 	= {mapClasses}
						style 		= {style}
						onMouseDown = {this.mouseDown}
						onMouseUp 	= {this.mouseUp}
						ref 		= {map => this.app.map = this.map = map}
					>
						{companies}
						{categories}
						{areas}
					</div>
					<Logos 	app = {this.app} mode = "map" />
				</div>
			</div>
		);
	}
};

function Car(props) {
	let categories = props.app.categories;
	let offset;

	for (let i = 0; i < categories.length; i++) {
		if (props.data.categoryId == categories[i].id) {
			offset = categories[i].offset;
			break;
		}
	}

	return (
		<div className = "car" data-id = {props.data.id} onClick = {props.click} >
			<CarCircle
				position 	= {props.data.position}
				show 		= {props.data.showCircle}
			/>
			<CarImage
				position 		= {props.data.position}
				image 			= {props.data.image}
				baseImageDir 	= {props.app.baseImageDir}
			/>
			<CarCaption
				position 	= {props.data.position}
				show 		= {props.data.showCaption}
				orientation = {props.data.captionOrientation}
				name 		= {props.data.name}
				percent 	= {props.data.percent}
				favorite 	= {props.data.favorite}
				offset 		= {offset}
			/>
		</div>
	);
}

function CarCircle(props) {
	let style = {
		opacity: 	props.show ? 1 : 0,
		top: 		props.position.top,
		left: 		props.position.left
	};

	return (
		<div className = "car__circle" style = {style} ></div>
	);
}

function CarImage(props) {
	let dir = props.baseImageDir + 'cars/';
	let style 	= {
		top: 				props.position.top,
		left:				props.position.left,
		backgroundImage: 	'url(' + dir + props.image + ')'
	};

	return (
		<div className = "car__image" style = {style} ></div>
	);
}

function CarCaption(props) {
	let classes = ['car__caption'];
	let offset 	= props.offset;
	let style 	= {
		top: 		props.position.top,
		left: 		props.position.left,
		opacity: 	props.show ? 1 : 0
	};

	switch (props.orientation) {
		case 'tl':
			classes.push('car__caption_tl');
		break;
		case 'tr':
			classes.push('car__caption_tr');
		break;
		case 'br':
			classes.push('car__caption_br');
		break;
		case 'bl':
			classes.push('car__caption_bl');
		break;
		default:
			classes.push('car__caption_tl');
	}

	if (offset) {
		style.top += offset.top;
		style.left += offset.left;
	}

	if (props.favorite) {
		classes.push('car__caption_favorite');

		return (
			<div className = "car__caption-container" style = {style} >
				<div className = {classes.join(' ')} >
					<div className = "car__caption-company">{props.name}</div>
					<div className = "car__caption-favorite"></div>
				</div>
			</div>
		);
	} else {
		return (
			<div className = "car__caption-container" style = {style} >
				<div className = {classes.join(' ')} >
					<div className = "car__caption-company">{props.name}</div>
					<div className = "car__caption-percent">{props.percent}%</div>
				</div>
			</div>
		);
	}

}

function MapButton(props) {
	let style = {
		left: props.data.buttonPosition.left,
		top:  props.data.buttonPosition.top,
		width: props.data.dimensions.width,
		height: props.data.dimensions.height,
	};

	return (
		<button className = "category" data-category-id = {props.data.id} onClick = {props.click} style = {style} />
	);
}

function MapArea(props) {
	let style = {
		top: props.data.button.top,
		left: props.data.button.left,
		width: props.data.button.width,
		height: props.data.button.height
	}

	return (
		<div className = 'area' style = {style} data-id = {props.data.id} onClick = {props.click} ></div>
	);
}

class Divisions extends React.Component {
	constructor(props) {
		super(props);
		this.app = props.app;
	}

	render() {
		let divisions = this.app.divisions.map(division => <Division division = {division} app = {this.app} />);

		if (!this.app.state.showDivisions) {
			return (<div />);
		}


		if (this.app.state.mode != 'list') {
			return (<div />);
		}

		return (
			<div className="divisions-desktop">
				<Logos app = {this.app} mode = "list" />
				{divisions}
			</div>
		);
	}
};

function Division(props) {
	let categories 	= [];
	let division 	= props.division;

	props.app.categories.forEach(category => {
		if (category.divisionId == division.id) {
			categories.push(<Category companies = {props.app.state.companies} category = {category} app = {props.app} />)
		}
	});
	
	return (
		<section className = "division" id = {'division-' + division.id}>
			<h1  className = "division__header">{division.name}</h1>
			<div className = "division__items">{categories}</div>
		</section>
	);
}

function Category(props) {
	let category 	= props.category;
	let companies 	= [];

	props.companies.forEach(company => {
		if (company.categoryId == category.id) {
			companies.push(company);
		}
	});

	return (
		<div className="division__item">
			<h2 className="division__item-header">
				<span className="division__item-header-ru">{category.nameRu}</span>
				<span className="division__item-header-en">{category.nameEn}</span>
			</h2>
			<CompanyList companies = {companies} app = {props.app} />
		</div>
	);
}

function CompanyList(props) {
	let companies = props.companies.map(company => <CompanyItem company = {company} click = {props.app.showPopup} />);

	return (
		<div className="division__item-list">{companies}</div>
	);
}

function CompanyItem(props) {
	let company = props.company;
	let classes = 'division__item-link';

	if (company.favorite) {
		classes += ' division__item-link_fav';
	}

	return (
		<a href = "#" data-id = {company.id} className = {classes} onClick = {props.click} >{company.name}</a>
	);
}

function Popup(props) {
	let app = props.app;
	let company = app.popupCompany || {};
	let category;
	let division;
	let contStyle = {
		display: app.state.showPopup ? 'flex' : 'none',
	};
	let popupStyle = {
		backgroundImage: 'url(' + app.baseImageDir + 'cars/' + company.image + ')'
	}

	if (!app.popupDivision) {
		return (
			<div />
		);
	}

	return (
		<div className = "popup__container" style = {contStyle} >
			<div className = "popup__close" onClick = {app.hidePopup} ></div>
			<div className = "popup">
				<button className = "popup__close-button" onClick = {app.hidePopup} >&times;</button>
				<div className = "popup__inner">
					<div className = "popup__left" style = {popupStyle} ></div>
					<div className = "popup__right">
						<div className = "popup__company">{company.name}</div>
						<div className = "popup__cats">{app.popupDivision.name}/{app.popupCategory.nameRu}</div>
						{company.url && <a href = {company.url} className = "popup__link" target = "_blank">{company.url}</a>}
						{!company.favorite && <div className = "popup__percent">
							<div className = "popup__percent-title">
								Доля сотрудничества на рынке
							</div>
							<div className = "popup__percent-value">{company.percent}%</div>
						</div>}
						<PopupRate rate = {company.rate} />
					</div>
				</div>
			</div>
		</div>
	);
}

function PopupRate(props) {
	let rate = props.rate;

	if (rate > 0) {
		rate = rate.toFixed(2) + '';
		rate = rate.replace('.', ',');
	}

	if (rate) {
		return (
			<div>
				<div className = "popup__rate">
					<div className = "popup__rate-title">Качество сервиса</div>
					<div className = "popup__rate-value">{props.rate}*</div>
				</div>
				<div className = "popup__note">
					*По 10 балльной шкале. По оценке клиентов компании.
				</div>
			</div>
		);
	} else {
		return (
			<div />
		);
	}
}

function getItemById(items, id) {
	if (!items.length) return null;

	for (let i = 0; i < items.length; i++) {
		if (items[i].id == id) {
			return items[i];
		}
	}
}

class MobileDivisions extends React.Component {
	constructor(props) {
		super(props);
		this.app 					= props.app;
		this.showCurrentDivision 	= this.showCurrentDivision.bind(this);
		this.returnHome 			= this.returnHome.bind(this);
		this.getDivisionCurrent		= this.getDivisionCurrent.bind(this);
	}

	showCurrentDivision(e) {
		let id = e.currentTarget.getAttribute('data-id');
		this.app.setState({currentDivision: id});
	}

	returnHome() {
		this.app.setState({currentDivision: null});
	}

	getDivisionCurrent() {
		let division = null;
		let id = this.app.state.currentDivision;

		for (let i = 0; i < this.app.divisions.length; i++) {
			if (id == this.app.divisions[i].id) {
				division = this.app.divisions[i];
				break;
			}
		}

		return division;
	}

	render() {
		let divisions = this.app.divisions.map(division => {
			return (
				<MobileDivision division = {division} show = {this.showCurrentDivision} baseImageDir = {this.app.baseImageDir} />
			);
		});

		if (!this.app.state.showMobileDivisions) {
			return (<div />);
		}

		if (this.app.state.currentDivision === null) {
			return (
				<div className = "divisions-mobile">
					{divisions}
				</div>
			);
		} else {
			return (
				<div className = "divisions-mobile">
					<CurrentMobileDivision
						division 	= {this.getDivisionCurrent()}
						back 		= {this.returnHome}
						app 		= {this.app}
						divisionId 	= {this.app.state.currentDivision}
					/>
				</div>
			);
		}
	}
}

function MobileDivision(props) {
	let style = {
		backgroundColor: props.division.color
	};

	return (
		<div className = "division-mobile__cover" data-id = {props.division.id} onClick = {props.show} >
			<img src = {props.baseImageDir + props.division.image} />
			<div className = "division-mobile__name" style= {style} >{props.division.name}</div>
		</div>
	);
}

function CurrentMobileDivision(props) {
	let app = props.app;
	let baseImageDir = app.baseImageDir;
	let categories = [];
	let divisionStyle = {
		backgroundColor: props.division.color,
		backgroundImage: 'url(' + baseImageDir + 'icons/' + props.division.icon + ')',
		backgroundSize: props.division.iconSize
	};

	for (let i = 0; i < app.categories.length; i++) {
		if (props.divisionId == app.categories[i].divisionId) {
			categories.push(app.categories[i]);
		}
	}

	categories = categories.map(cat => {
		return <MobileCategory
			divisionName = {props.division.name}
			cat = {cat}
			companies = {app.state.companies}
			app = {app}
		/>
	});

	return (
		<div className = "division-mobile division-mobile_current">
			<button className = "division-mobile__back" onClick = {props.back} style = {divisionStyle} >
				<span>{props.division.name}</span>
			</button>
			{categories}
		</div>
	);
}

function MobileCategory(props) {
	let companies = [];
	let app = props.app;

	for (let i = 0; i < props.companies.length; i++) {
		if (props.cat.id == props.companies[i].categoryId) {
			companies.push(props.companies[i]);
		}
	}

	companies = companies.map(company => {
		return <MobileCompany company = {company} division = {props.divisionName} cat = {props.cat.nameRu} app = {app} />
	});

	return (
		<div className = "category-mobile">
			<div className = "category-mobile__header">
				<div className = "category-mobile__header-ru">{props.cat.nameRu}</div>
				<div className = "category-mobile__header-en">{props.cat.nameEn}</div>
			</div>
			<div className = "category-mobile__list">
				{companies}
			</div>
		</div>
	);
}

function MobileCompany(props) {
	let app = props.app;
	let company = props.company;
	let companyClasses = 'company-mobile company-mobile-' + company.id
	let companyNameClasses = 'company-mobile__name';
	let companyDescClasses = 'company-mobile__desc';
	let showCompany = false;
	let currentCompany = app.state.currentCompany;
	let linkStyle = {
		display: company.url ? 'block' : 'none'
	};

	function toggleCompany(e) {
		e.currentTarget.classList.toggle('company-mobile__name_expand');
		e.currentTarget.parentNode.querySelector('.company-mobile__desc').classList.toggle('company-mobile__desc_expand');
		e.currentTarget.parentNode.classList.toggle('company-mobile_expand');
	}

	function scrollToCompany(e) {
		let headerHeight;
		let offsetTop;
		let backHeight;

		if (!showCompany || !e) {
			return false;
		}

		headerHeight = document.querySelector('.header').clientHeight;
		offsetTop = e.offsetTop;
		backHeight = document.querySelector('.division-mobile__back').clientHeight;

		window.scroll({
			top: offsetTop - headerHeight - backHeight,
			left: 0,
			behavior: 'smooth'
		});
	}

	if (!company.rate) {
		company.rate = 0;
	} else {
		company.rate = parseInt(company.rate, 10);
	}

	if (company.favorite) {
		companyNameClasses += ' company-mobile__name_favorite';
	}

	company.rate = company.rate.toFixed(1) + '';
	company.rate = company.rate.replace('.', ',');

	if (company.id == currentCompany) {
		companyClasses += ' company-mobile_expand';
		companyNameClasses += ' company-mobile__name_expand';
		companyDescClasses += ' company-mobile__desc_expand';
		showCompany = true;

		app.popupCompany = null;
	}

	return (
		<div className = {companyClasses} >
			<button className = {companyNameClasses} onClick = {toggleCompany} ref = {scrollToCompany} >
				<span>{company.name}</span>
			</button>
			<div className = {companyDescClasses} >
				<div className = "company-mobile__division">{props.division} / {props.cat}</div>
				<div className = "company-mobile__market">
					<div className = "company-mobile__market-title">Занимаемый процент рынка</div>
					<div className = "company-mobile__market-value">{company.percent}%</div>
				</div>
				<div className = "company-mobile__rate">
					<div className = "company-mobile__rate-title">Удовлетворенность сервисом</div>
					<div className = "company-mobile__rate-value">
						{company.rate}/10
					</div>
				</div>
				<a href = {company.url} className = "company-mobile__link" style = {linkStyle} >{company.url}</a>
			</div>
		</div>
	);
}

class Control extends React.Component {
	constructor(props) {
		super(props);
		this.legend = this.legend.bind(this);
		this.zoom = this.zoom.bind(this);
		this.mode = this.mode.bind(this);
		this.app = props.app;
	}

	legend() {
		this.app.setState({
			showLegend: !this.app.state.showLegend
		});
	}

	zoom(e) {
		let type = e.currentTarget.getAttribute('data-type');
		let step = 0.1;
		let zoom = this.app.state.zoom;
		let newZoom;

		switch (type) {
			case 'plus':
				newZoom = zoom + step;
			break;
			case 'minus':
				newZoom = zoom - step;
			break;
		}

		this.app.setState({
			newZoom: newZoom
		});
	}

	mode() {
		let mode = this.app.state.mode;
		let navE = document.querySelectorAll('.header__nav-a:not(.header__nav-a_search)');

		if (mode == 'map') {
			mode = 'list';
			this.app.setState({
				showLegend: false
			});
		} else {
			mode = 'map';
		}

		this.app.setState({
			mode: mode
		});

		for (let i = 0; i < navE.length; i++) {
			navE[i].classList.remove('header__nav-a_current');
		}
	}

	render() {

		if (isMobile()) {
			return (
				<div />
			);
		}

		let modeClassName = 'control__button control__button_mode control__button_mode-' + this.app.state.mode;

		return (
			<div className = "control">
				<div className = "control__inner">
					<button className = "control__button control__button_legend" onClick = {this.app.toggleLegend} >
						<span></span>
					</button>
					<ControlZoom app = {this.app} zoom = {this.zoom} />
					<div className = "control__switch" onClick = {this.mode} >
						<button className = {modeClassName} >
							<span></span>
						</button>
					</div>
				</div>
			</div>
		);
	}
}

function ControlZoom(props) {
	let app = props.app;

	if (app.state.mode != 'map') {
		return (
			<div />
		);
	}

	return (
		<div className = "control__zoom">
			<button className = "control__button control__button_plus" data-type="plus" onClick = {props.zoom} >
				<span></span>
			</button>
			<button className = "control__button control__button_minus" data-type="minus" onClick = {props.zoom} >
				<span></span>
			</button>
		</div>
	);
}

function Legend(props) {
	let app = props.app;
	let classes = 'legend';

	if (app.state.showLegend && !isMobile()) {
		classes += ' legend_show';
	}

	return (
		<div className = {classes} >
			<div className = "legend__inner">
				<div className = "legend__body">
					<button className = "legend__close" onClick = {app.toggleLegend} ></button>
					<div className = "legend__logo"></div>
					<div className = "legend__header">
						TECHNOLOGY MAP <span>2017</span>
					</div>
					<div className = "legend__text">
						На карте представлены лидеры по доле рынка среди опрошенных компаний в рейтинге Techology Index 2017, а также партнеры проекта.
					</div>
					<div className = "legend_signs">
						<div className = "legend__sign">
							<div className = "legend__sign-icon legend__sign-icon_division"></div>
							<div className = "legend__sign-text">Секторы рынка</div>
						</div>
						<div className = "legend__sign">
							<div className = "legend__sign-icon legend__sign-icon_category"></div>
							<div className = "legend__sign-text">Сферы деятельности компаний</div>
						</div>
						<div className = "legend__sign">
							<div className = "legend__sign-icon legend__sign-icon_title"></div>
							<div className = "legend__sign-text">Название компании и доля рынка</div>
						</div>
						<div className = "legend__sign">
							<div className = "legend__sign-icon legend__sign-icon_partners"></div>
							<div className = "legend__sign-text">Партнеры проекта</div>
						</div>
					</div>
					<div className = "legend__icons">
						<div className = "legend__icons-col">
							<div className = "legend__icons-header">ПАРТНЕРЫ ПРОЕКТА</div>
							<div className = "legend__icon legend__icon_p1"></div>
							<div className = "legend__icon legend__icon_p2"></div>
							<div className = "legend__icon legend__icon_p3"></div>
							<div className = "legend__icon legend__icon_p4"></div>
							<div className = "legend__icon legend__icon_p5"></div>
							<div className = "legend__icon legend__icon_p6"></div>
							<div className = "legend__icon legend__icon_p7"></div>
							<div className = "legend__icon legend__icon_p8"></div>
						</div>
						<div className = "legend__icons-col">
							<div className = "legend__icons-header">ЛИДЕРЫ РЕЙТИНГА</div>
							<div className = "legend__icon legend__icon_l1"></div>
							<div className = "legend__icon legend__icon_l2"></div>
							<div className = "legend__icon legend__icon_l3"></div>
							<div className = "legend__icon legend__icon_l4"></div>
							<div className = "legend__icon legend__icon_l5"></div>
							<div className = "legend__icon legend__icon_l6"></div>
							<div className = "legend__icon legend__icon_l7"></div>
							<div className = "legend__icon legend__icon_l8"></div>
						</div>
					</div>
				</div>
				<div className = "legend__footer">
					Карта создана
					<a href="http://happylab.ru/" target="_blank" className = "legend__dev"></a>
				</div>
			</div>
		</div>
	);
}

class Logos extends React.Component {
	constructor(props) {
		super(props);
		this.app = props.app;
		this.slick = this.slick.bind(this);
		this.hide = this.hide.bind(this);
		this.whenShow = props.mode;
	}

	slick(e) {
		this.app.logoContainer = e;
		$(e).slick({
			slidesToShow: 9,
			slidesToScroll: 9,
			autoplay: true,
			nextArrow: '.logo__button_next',
			prevArrow: '.logo__button_prev',
			responsive: [{
				breakpoint: 1750,
				settings: {
					slidesToShow: 8,
					slidesToScroll: 8
				}
			}, {
				breakpoint: 1600,
				settings: {
					slidesToShow: 7,
					slidesToScroll: 7
				}
			}, {
				breakpoint: 1250,
				settings: {
					slidesToShow: 6,
					slidesToScroll: 6
				}
			}, {
				breakpoint: 1024,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 5
				}
			}]
		});
	}

	hide() {
		this.app.showLogo = false;
		this.app.setState({
			showLogo: false
		});
	}

	componentWillUnmount() {
		$(this.app.logoContainer).slick('unslick');
	}

	render() {
		let logos = this.app.logos.map(logo => <Logo image = {logo.image} url = {logo.url} border = {logo.border} app = {this.app} />);
		let classes;

		if (this.whenShow == 'list') {
			classes = 'logo_static';
		} else {
			classes = 'logo';
		}

		if (!this.app.state.showLogo) {
			if (this.whenShow == 'list') {
				classes += ' logo_hide-static';
			} else {
				classes += ' logo_hide';
			}
		}

		if (this.whenShow != this.app.state.mode) {
			return (
				<div />
			);
		}

		if (isMobile()) {
			return (
				<div />
			);
		}

		return (
			<div className = {classes} >
				<button className = "logo__close" onClick = {this.hide} ></button>
				<button className = "logo__button logo__button_prev">
					<span></span>
				</button>
				<button className = "logo__button logo__button_next">
					<span></span>
				</button>
				<div className = "logo__items" ref = {this.slick} >
					{logos}
				</div>
			</div>
		);
	}
}

function Logo(props) {
	let baseImageDir = props.app.baseImageDir;
	let image = baseImageDir + 'logo/' + props.image;
	let classes = '';

	if (props.border) {
		classes = 'border';
	}

	return (
		<div className = "logo__item">
			<div className = "logo__item-image">
				<a href = {props.url || '#'} target="_blank" className = {classes} >
					<img src = {image} />
				</a>
			</div>
		</div>
	);
}

function isMobile() {
	let mediaQ = '(max-width: 1023px)';

	return window.matchMedia(mediaQ).matches
}

ReactDOM.render(<App />, document.getElementById('app'));