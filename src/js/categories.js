var categories = [{
	id: 1,
	divisionId: 1,
	nameRu: 'Рекламные видеосети',
	nameEn: 'Video Networks',
	buttonPosition: {
		top: 283,
		left: 464
	},
	dimensions: {
		width: 315,
		height: 70
	},
	offset: {
		top: -7,
		left: 0
	}
}, {
	id: 2,
	divisionId: 1,
	nameRu: 'Рекламные сети',
	nameEn: 'Ad Networks',
	buttonPosition: {
		top: 248,
		left: 1440
	},
	dimensions: {
		width: 243,
		height: 70
	},
	offset: {
		top: -9,
		left: 6
	}
}, {
	id: 3,
	divisionId: 1,
	nameRu: 'Системы управления рекламой',
	nameEn: 'Ad Management Systems',
	buttonPosition: {
		top: 620,
		left: 70
	},
	dimensions: {
		width: 420,
		height: 70
	},
	offset: {
		top: -7,
		left: 1
	}
}, {
	id: 4,
	divisionId: 1,
	nameRu: 'Независимые программатик платформы',
	nameEn: 'Independent DSP',
	buttonPosition: {
		top: 880,
		left: 70
	},
	dimensions: {
		width: 525,
		height: 70
	},
	offset: {
		top: -8,
		left: 0
	}
}, {
	id: 5,
	divisionId: 1,
	nameRu: 'Сети нативной рекламы',
	nameEn: 'Native Networks',
	buttonPosition: {
		top: 433,
		left: 70
	},
	dimensions: {
		width: 339,
		height: 70
	},
	offset: {
		top: -8,
		left: 1
	}
}, {
	id: 6,
	divisionId: 2,
	nameRu: 'Платформы лидогенерации',
	nameEn: 'CPA Platforms',
	buttonPosition: {
		top: 138,
		left: 2282
	},
	dimensions: {
		width: 375,
		height: 70
	},
	offset: {
		top: -12,
		left: 1
	}
}, {
	id: 7,
	divisionId: 2,
	nameRu: 'Автоматизация контекстной рекламы',
	nameEn: 'SEA Automatization',
	buttonPosition: {
		top: 298,
		left: 2269
	},
	dimensions: {
		width: 498,
		height: 70
	},
	offset: {
		top: -12,
		left: 1
	}
}, {
	id: 8,
	divisionId: 2,
	nameRu: 'Ретаргетинг',
	nameEn: 'Retargeting',
	buttonPosition: {
		top: 483,
		left: 2269
	},
	dimensions: {
		width: 200,
		height: 70
	},
	offset: {
		top: -12,
		left: 1
	}
}, {
	id: 9,
	divisionId: 2,
	nameRu: 'Автоматизация таргетированной рекламы',
	nameEn: 'Target Ad Automatization',
	buttonPosition: {
		top: 654,
		left: 2296
	},
	dimensions: {
		width: 545,
		height: 70
	},
	offset: {
		top: -12,
		left: 1
	}
}, {
	id: 10,
	divisionId: 2,
	nameRu: 'Маркетинговые платформы',
	nameEn: 'Marketing Platforms',
	buttonPosition: {
		top: 791,
		left: 2281
	},
	dimensions: {
		width: 378,
		height: 70
	},
	offset: {
		top: -12,
		left: 1
	}
}, {
	id: 11,
	divisionId: 3,
	nameRu: 'Система сквозной аналитики',
	nameEn: 'Cross Plarform Analytics',
	buttonPosition: {
		top: 1258,
		left: 1251
	},
	dimensions: {
		width: 405,
		height: 70
	},
	offset: {
		top: -7,
		left: 6
	}
}, {
	id: 12,
	divisionId: 3,
	nameRu: 'Система отслеживания звонков',
	nameEn: 'Call Tracking',
	buttonPosition: {
		top: 1438,
		left: 1251
	},
	dimensions: {
		width: 432,
		height: 70
	},
	offset: {
		top: -7,
		left: 6
	}
}, {
	id: 13,
	divisionId: 3,
	nameRu: 'Система управления данными',
	nameEn: 'DMP',
	buttonPosition: {
		top: 1580,
		left: 1249
	},
	dimensions: {
		width: 409,
		height: 70
	},
	offset: {
		top: -7,
		left: 6
	}
}, {
	id: 14,
	divisionId: 3,
	nameRu: 'Поставщики готовых данных',
	nameEn: 'Processed Data Suppliers',
	buttonPosition: {
		top: 1738,
		left: 1252
	},
	dimensions: {
		width: 403,
		height: 70
	},
	offset: {
		top: -7,
		left: 6
	}
}, {
	id: 15,
	divisionId: 3,
	nameRu: 'Мониторинг социальных сетей',
	nameEn: 'Social Media Monitoring',
	buttonPosition: {
		top: 1913,
		left: 1251
	},
	dimensions: {
		width: 421,
		height: 70
	},
	offset: {
		top: -7,
		left: 6
	}
}, {
	id: 16,
	divisionId: 4,
	nameRu: 'Мобайл премиум',
	nameEn: 'Mobile Premium',
	buttonPosition: {
		top: 1120,
		left: 1820
	},
	dimensions: {
		width: 256,
		height: 70
	},
	offset: {
		top: -8,
		left: 1
	}
}, {
	id: 17,
	divisionId: 4,
	nameRu: 'Мобайл лидогенерация',
	nameEn: 'Mobile CPA',
	buttonPosition: {
		top: 1340,
		left: 2061
	},
	dimensions: {
		width: 326,
		height: 70
	},
	offset: {
		top: -7,
		left: 0
	}
}, {
	id: 18,
	divisionId: 4,
	nameRu: 'Продвижение мобильных приложений',
	nameEn: 'Mobile CPI',
	buttonPosition: {
		top: 1574,
		left: 2058
	},
	dimensions: {
		width: 341,
		height: 90
	},
	offset: {
		top: -7,
		left: 0
	}
}, {
	id: 19,
	divisionId: 4,
	nameRu: 'Мобайл программатик платформы',
	nameEn: 'Mobile DSP',
	buttonPosition: {
		top: 1833,
		left: 2398
	},
	dimensions: {
		width: 328,
		height: 90
	},
	offset: {
		top: -7,
		left: 0
	}
}, {
	id: 20,
	divisionId: 4,
	nameRu: 'Мобильные сети',
	nameEn: 'Mobile Networks',
	buttonPosition: {
		top: 2176,
		left: 1830
	},
	dimensions: {
		width: 242,
		height: 70
	},
	offset: {
		top: -3,
		left: 4
	}
}];