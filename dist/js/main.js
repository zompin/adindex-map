'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
	_inherits(App, _React$Component);

	function App(props) {
		_classCallCheck(this, App);

		var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

		_this.showPopup = _this.showPopup.bind(_this);
		_this.hidePopup = _this.hidePopup.bind(_this);
		_this.toggleLegend = _this.toggleLegend.bind(_this);
		_this.resizeApp = _this.resizeApp.bind(_this);
		_this.hideByClickArea = _this.hideByClickArea.bind(_this);
		_this.divisions = divisions;
		_this.categories = categories;
		_this.logos = logos;
		_this.popupCompany = {};
		_this.popupCategory = {};
		_this.popupDivision = {};
		_this.baseImageDir = '/imgs/';

		_this.state = {
			companies: companies,
			zoom: 0.7,
			newZoom: 0.7,
			left: 0,
			top: 0,
			showPopup: false,
			showMap: false,
			showDivisions: false,
			showMobileDivisions: true,
			currentDivision: null,
			currentCat: 0,
			showLegend: false,
			mode: 'map',
			showLogo: true,
			showSearch: false
		};
		return _this;
	}

	_createClass(App, [{
		key: 'showPopup',
		value: function showPopup(e, company) {
			var id = void 0;
			var companies = this.state.companies;

			if (e) {
				id = e.currentTarget.getAttribute('data-id');
			} else {
				id = company;
			}

			for (var i = 0; i < companies.length; i++) {
				if (companies[i].id == id && !companies[i].favorite) {
					this.popupCompany = companies[i];

					this.setState({
						showPopup: true
					});

					break;
				} else if (companies[i].id == id && companies[i].favorite) {

					if (this.state.mode == 'list') {
						window.open(companies[i].url);
					} else {
						if (companies[i].showCaption) {
							window.open(companies[i].url);
						}
						companies[i].showCaption = !companies[i].showCaption;
					}

					this.setState({
						companies: companies
					});

					break;
				}
			}

			if (this.popupCompany.id) {
				this.popupCategory = getItemById(this.categories, this.popupCompany.categoryId);
			} else {
				this.popupCategory = {};
			}

			if (this.popupCategory) {
				this.popupDivision = getItemById(this.divisions, this.popupCategory.divisionId);
			} else {
				this.popupDivision = {};
			}

			if (e) {
				e.preventDefault();
			}

			this.setState({
				showLegend: false
			});
		}
	}, {
		key: 'hidePopup',
		value: function hidePopup() {
			this.setState({ showPopup: false });
		}
	}, {
		key: 'toggleLegend',
		value: function toggleLegend() {
			var show = this.state.showLegend;

			this.setState({
				showLegend: !show
			});
		}
	}, {
		key: 'resizeApp',
		value: function resizeApp() {
			if (isMobile()) {
				this.setState({
					showMap: false,
					showDivisions: false,
					showMobileDivisions: true
				});
			} else {
				this.setState({
					showMap: true,
					showDivisions: true,
					showMobileDivisions: false
				});
			}
		}
	}, {
		key: 'hideByClickArea',
		value: function hideByClickArea(e) {
			var currentElem = e.target;
			var className = void 0;

			try {
				do {
					className = currentElem.className;

					if (className) {
						if (className.indexOf('search') != -1) {
							return;
						}

						if (className.indexOf('legend') != -1) {
							return;
						}
					}
				} while (currentElem = currentElem.parentNode);
			} catch (e) {
				return;
			}

			this.setState({
				showSearch: false,
				showLegend: false
			});
		}
	}, {
		key: 'componentWillMount',
		value: function componentWillMount() {
			window.addEventListener('resize', this.resizeApp, false);
			document.addEventListener('click', this.hideByClickArea, false);
			this.resizeApp();
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			window.removeEventListener('resize', this.resizeApp, false);
			document.removeEventListener('click', this.hideByClickArea, false);
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				null,
				React.createElement(Header, { app: this }),
				React.createElement(Map, { app: this }),
				React.createElement(Divisions, { app: this }),
				React.createElement(Popup, { app: this }),
				React.createElement(MobileDivisions, { app: this }),
				React.createElement(Control, { app: this }),
				React.createElement(Legend, { app: this })
			);
		}
	}]);

	return App;
}(React.Component);

;

var Header = function (_React$Component2) {
	_inherits(Header, _React$Component2);

	function Header(props) {
		_classCallCheck(this, Header);

		var _this2 = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));

		_this2.Change = _this2.Change.bind(_this2);
		_this2.Enter = _this2.Enter.bind(_this2);
		_this2.Leave = _this2.Leave.bind(_this2);
		_this2.ItemClick = _this2.ItemClick.bind(_this2);
		_this2.ItemLeave = _this2.ItemLeave.bind(_this2);
		_this2.Click = _this2.Click.bind(_this2);
		_this2.focus = _this2.focus.bind(_this2);
		_this2.filterCompanies = _this2.filterCompanies.bind(_this2);
		_this2.goToDivision = _this2.goToDivision.bind(_this2);
		_this2.clear = _this2.clear.bind(_this2);
		_this2.app = props.app;

		_this2.state = {
			search: '',
			searchList: [],
			searchPlaceholder: '',
			showSearchList: false
		};
		return _this2;
	}

	_createClass(Header, [{
		key: 'Change',
		value: function Change(e) {
			var searchList = this.filterCompanies(e.target.value);
			var companies = this.app.state.companies;
			this.setState({
				search: e.target.value,
				searchList: searchList,
				searchPlaceholder: '',
				showSearchList: true
			});

			if (e.target.value.length == 0) {
				for (var i = 0; i < companies.length; i++) {
					companies[i].showCircle = false;
					companies[i].showCaption = false;
				}

				this.app.setState({
					companies: companies
				});
			}
		}
	}, {
		key: 'Enter',
		value: function Enter(e) {
			var id = e.target.getAttribute('data-id');
			var companies = this.app.state.companies;

			for (var i = 0; i < companies.length; i++) {
				if (id == companies[i].id) {
					this.setState({
						searchPlaceholder: companies[i].name
					});

					companies[i].showCircle = true;

					this.app.setState({
						companies: companies
					});
					break;
				}
			}
		}
	}, {
		key: 'Leave',
		value: function Leave() {
			this.setState({ searchPlaceholder: '' });
		}
	}, {
		key: 'Click',
		value: function Click(e) {
			var show = !this.app.state.showSearch;
			this.app.setState({ showSearch: show });
			e.preventDefault();

			if (show && this.state.search) {
				this.setState({
					showSearchList: true
				});
			}
		}
	}, {
		key: 'ItemClick',
		value: function ItemClick(e) {
			var id = e.target.getAttribute('data-id') || e.target.parentNode.getAttribute('data-id');
			var companies = this.app.state.companies;
			var contHalfWidth = void 0;
			var contHalfHeight = void 0;
			var zoom = this.app.state.zoom;
			var left = void 0;
			var top = void 0;

			if (this.app.container) {
				contHalfWidth = this.app.container.clientWidth / 2;
				contHalfHeight = this.app.container.clientHeight / 2;
			} else {
				contHalfWidth = 0;
				contHalfHeight = 0;
			}

			for (var i = 0; i < companies.length; i++) {
				if (companies[i].id == id) {
					left = contHalfWidth / zoom - companies[i].position.left;
					top = contHalfHeight / zoom - companies[i].position.top;

					if (isMobile()) {
						for (var j = 0; j < this.app.categories.length; j++) {
							if (companies[i].categoryId == this.app.categories[j].id) {

								for (var k = 0; k < this.app.divisions.length; k++) {
									if (this.app.categories[j].divisionId == this.app.divisions[k].id) {
										this.app.popupCompany = companies[i];
										this.app.setState({ currentDivision: this.app.divisions[k].id });
									}
								}

								break;
							}
						}
					} else {
						if (this.app.state.mode == 'list') {
							this.app.showPopup(false, companies[i].id);
						} else {

							this.app.setState({
								left: left,
								top: top,
								zoom: zoom
							});
						}
					}

					companies[i].showCircle = true;
					companies[i].showCaption = true;
				} else {
					companies[i].showCircle = false;
					companies[i].showCaption = false;
				}
			}

			this.app.state.currentCompany = id;

			this.app.setState({
				companies: companies
			});

			this.setState({
				showSearchList: false
			});
		}
	}, {
		key: 'ItemLeave',
		value: function ItemLeave(e) {
			var id = e.target.getAttribute('data-id');
			var companies = this.app.state.companies;

			for (var i = 0; i < companies.length; i++) {
				if (companies[i].id == id) {
					companies[i].showCircle = false;

					this.app.setState({
						companies: companies
					});

					break;
				}
			}
		}
	}, {
		key: 'filterCompanies',
		value: function filterCompanies(value) {
			var list = [];
			var i = 0;
			var count = 0;
			var companies = this.app.state.companies;
			var categories = this.app.categories;

			while (value.length && i < companies.length && count < 10) {
				if (companies[i].name.toLowerCase().indexOf(value.toLowerCase()) == 0) {
					list.push(companies[i]);
					count++;
				}
				i++;
			}

			for (var _i = 0; _i < list.length; _i++) {
				for (var j = 0; j < categories.length; j++) {
					if (list[_i].categoryId == categories[j].id) {
						list[_i].categoryName = categories[j].nameRu;
						break;
					}
				}
			}

			return list;
		}
	}, {
		key: 'goToDivision',
		value: function goToDivision(e) {
			var id = e.currentTarget.getAttribute('data-id');
			var mode = this.app.state.mode;
			var zoom = this.app.state.zoom;
			var container = this.app.container;
			var division = this.app.divisions;
			var categories = this.app.categories;
			var companies = this.app.state.companies;
			var contHalfWidth = void 0;
			var contHalfHeight = void 0;
			var left = void 0;
			var top = void 0;
			var divisionE = void 0;
			var offsetTop = void 0;
			var headerE = void 0;
			var headerHeight = void 0;
			var navE = document.querySelectorAll('.header__nav-a:not(.header__nav-a_search)');

			e.preventDefault();

			if (mode == 'list') {
				divisionE = document.getElementById('division-' + id);
				headerE = document.querySelector('.header');
				offsetTop = divisionE.offsetTop;
				headerHeight = headerE.clientHeight;
				offsetTop -= headerHeight;

				window.scroll({
					top: offsetTop,
					left: 0,
					behavior: 'smooth'
				});
			} else {
				contHalfWidth = container.clientWidth / 2;
				contHalfHeight = container.clientHeight / 2;

				for (var i = 0; i < divisions.length; i++) {
					if (divisions[i].id == id) {
						left = divisions[i].position.left * -1;
						top = divisions[i].position.top * -1;
						break;
					}
				}

				for (var _i2 = 0; _i2 < categories.length; _i2++) {
					if (categories[_i2].divisionId == id) {
						for (var j = 0; j < companies.length; j++) {
							if (companies[j].categoryId == categories[_i2].id) {
								companies[j].showCircle = false;
								companies[j].showCaption = true;
							}
						}
					} else {
						for (var _j = 0; _j < companies.length; _j++) {
							if (companies[_j].categoryId == categories[_i2].id) {
								companies[_j].showCircle = false;
								companies[_j].showCaption = false;
							}
						}
					}
				}

				this.app.setState({
					top: top,
					left: left,
					companies: companies,
					currentDivision: id
				});
			}

			for (var _i3 = 0; _i3 < navE.length; _i3++) {
				navE[_i3].classList.remove('header__nav-a_current');
			}

			e.target.classList.add('header__nav-a_current');
		}
	}, {
		key: 'focus',
		value: function focus() {
			this.setState({
				showSearchList: true
			});
		}
	}, {
		key: 'clear',
		value: function clear() {
			var companies = this.app.state.companies;

			for (var i = 0; i < companies.length; i++) {
				companies[i].showCircle = false;
				companies[i].showCaption = false;
			}

			this.setState({
				search: '',
				showSearchList: false,
				searchList: []
			});

			this.app.setState({
				companies: companies
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var divisions = this.app.divisions;
			divisions = divisions.map(function (division) {
				return React.createElement(
					'a',
					{ href: '#', className: 'header__nav-a', 'data-id': division.id, onClick: _this3.goToDivision },
					division.name
				);
			});

			return React.createElement(
				'header',
				{ className: 'header' },
				React.createElement(
					'a',
					{ href: '/', className: 'header__logo' },
					'\xA0'
				),
				React.createElement(
					'nav',
					{ className: 'header__nav' },
					divisions,
					React.createElement(
						'a',
						{ href: '#', className: 'header__nav-a header__nav-a_search', onClick: this.Click },
						'\xA0'
					),
					React.createElement(Search, {
						show: this.app.state.showSearch,
						showList: this.state.showSearchList,
						value: this.state.search,
						list: this.state.searchList,
						placeholder: this.state.searchPlaceholder,
						change: this.Change,
						focus: this.focus,
						enter: this.Enter,
						leave: this.Leave,
						click: this.ItemClick,
						itemleave: this.ItemLeave,
						clear: this.clear
					})
				)
			);
		}
	}]);

	return Header;
}(React.Component);

;

function Search(props) {
	var placeholder = props.placeholder;
	var value = props.value;
	var style = { display: props.show ? 'block' : 'none' };
	var count = props.list.length;
	var listClasses = 'search__list';
	var clearClasses = 'search__clear';
	var list = props.list.map(function (company) {
		var name = company.name.substr(value.length);

		return React.createElement(
			'button',
			{
				className: 'search__list-item',
				'data-id': company.id,
				onMouseEnter: props.enter,
				onClick: props.click,
				onMouseLeave: props.itemleave
			},
			React.createElement(
				'span',
				{ className: 'search__list-item-company' },
				React.createElement(
					'b',
					null,
					value
				),
				name
			),
			React.createElement(
				'span',
				{ className: 'search__list-item-category' },
				company.categoryName
			)
		);
	});

	if (value.length) {
		switch (count % 10) {
			case 0:
			case 9:
			case 8:
			case 7:
			case 6:
			case 5:
				count = count + " результатов поиска";
				break;
			case 4:
			case 3:
			case 2:
				count = count + " результата поиска";
				break;
			case 1:
				count = count + " результат поиска";
				break;
		}
	} else {
		count = '';
	}

	if (props.showList) {
		listClasses += ' search__list_show';
	}

	if (value.length) {
		clearClasses += ' search__clear_show';
	}

	return React.createElement(
		'div',
		{ className: 'search', style: style },
		React.createElement(
			'div',
			{ className: 'search__placeholder' },
			placeholder
		),
		React.createElement(
			'div',
			{ className: 'search__count' },
			count
		),
		React.createElement('input', {
			className: 'search__input',
			type: 'text',
			value: value,
			onChange: props.change,
			onFocus: props.focus,
			placeholder: '\u041D\u0430\u0439\u0442\u0438 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u044E'
		}),
		React.createElement(
			'div',
			{ className: listClasses, onMouseLeave: props.leave },
			list
		),
		React.createElement('button', { className: clearClasses, onClick: props.clear })
	);
}

var Map = function (_React$Component3) {
	_inherits(Map, _React$Component3);

	function Map(props) {
		_classCallCheck(this, Map);

		var _this4 = _possibleConstructorReturn(this, (Map.__proto__ || Object.getPrototypeOf(Map)).call(this, props));

		_this4.app = props.app;
		_this4.Wheel = _this4.Wheel.bind(_this4);
		_this4.mouseDown = _this4.mouseDown.bind(_this4);
		_this4.mouseUp = _this4.mouseUp.bind(_this4);
		_this4.mouseMove = _this4.mouseMove.bind(_this4);
		_this4.mouseOut = _this4.mouseOut.bind(_this4);
		_this4.checkTop = _this4.checkTop.bind(_this4);
		_this4.getMinTop = _this4.getMinTop.bind(_this4);
		_this4.checkLeft = _this4.checkLeft.bind(_this4);
		_this4.getMinLeft = _this4.getMinLeft.bind(_this4);
		_this4.catClick = _this4.catClick.bind(_this4);
		_this4.areaClick = _this4.areaClick.bind(_this4);
		_this4.startDrag = false;
		_this4.offsetLeft = 0;
		_this4.offsetTop = 0;
		_this4.zoomStep = 0.05;
		return _this4;
	}

	_createClass(Map, [{
		key: 'Wheel',
		value: function Wheel(e) {
			var zoom = this.app.state.zoom;
			var zoomStep = this.zoomStep;
			var newZoom = void 0;

			if (e.deltaY < 0) {
				newZoom = zoom + zoomStep;
			} else {
				newZoom = zoom - zoomStep;
			}

			newZoom = newZoom.toFixed(2) * 1;

			this.app.setState({
				newZoom: newZoom
			});

			e.preventDefault();
		}
	}, {
		key: 'mouseDown',
		value: function mouseDown(e) {
			this.startDrag = true;
			this.offsetLeft = e.pageX - this.app.state.left * this.app.state.zoom;
			this.offsetTop = e.pageY - this.app.state.top * this.app.state.zoom;

			e.preventDefault();
		}
	}, {
		key: 'mouseUp',
		value: function mouseUp(e) {
			this.startDrag = false;
		}
	}, {
		key: 'mouseMove',
		value: function mouseMove(e) {
			var top = void 0;
			var left = void 0;

			if (this.startDrag) {
				top = (e.pageY - this.offsetTop) / this.app.state.zoom;
				left = (e.pageX - this.offsetLeft) / this.app.state.zoom;

				this.app.setState({
					top: top,
					left: left
				});
			}
		}
	}, {
		key: 'mouseOut',
		value: function mouseOut(e) {
			this.startDrag = false;
		}
	}, {
		key: 'catClick',
		value: function catClick(e) {
			var catId = e.target.getAttribute('data-category-id') || e.target.parentNode.getAttribute('data-category-id');
			var companies = void 0;
			var currentCat = this.app.state.currentCat;

			if (!catId) {
				return false;
			}

			companies = this.app.state.companies.map(function (company) {
				if (company.categoryId != catId || catId == currentCat) {
					company.showCaption = false;
					company.showCircle = false;
				} else {
					company.showCaption = true;
				}

				return company;
			});

			if (catId == currentCat) {
				catId = 0;
			}

			this.app.setState({
				companies: companies,
				currentCat: catId
			});
		}
	}, {
		key: 'areaClick',
		value: function areaClick(e) {
			var id = e.target.getAttribute('data-id');
			var currentDivision = this.app.state.currentDivision;
			var categories = this.app.categories;
			var companies = this.app.state.companies;

			for (var i = 0; i < categories.length; i++) {
				if (categories[i].divisionId == id) {
					for (var j = 0; j < companies.length; j++) {
						if (companies[j].categoryId == categories[i].id) {
							companies[j].showCircle = false;
							companies[j].showCaption = true;
						}
					}
				} else {
					for (var _j2 = 0; _j2 < companies.length; _j2++) {
						if (companies[_j2].categoryId == categories[i].id) {
							companies[_j2].showCircle = false;
							companies[_j2].showCaption = false;
						}
					}
				}
			}

			if (currentDivision == id) {
				for (var _j3 = 0; _j3 < companies.length; _j3++) {
					companies[_j3].showCircle = false;
					companies[_j3].showCaption = false;
				}

				id = 0;
			}

			this.app.setState({
				currentDivision: id
			});
		}
	}, {
		key: 'checkTop',
		value: function checkTop(top) {
			var maxTop = 0;
			var minTop = void 0;

			minTop = this.getMinTop();

			if (top > maxTop) top = maxTop;
			if (top < minTop) top = minTop;

			return top;
		}
	}, {
		key: 'getMinTop',
		value: function getMinTop(zoom) {
			var minTop = 0;

			if (zoom === undefined) {
				zoom = this.app.state.zoom;
			}

			minTop = this.container.clientHeight / zoom - this.map.clientHeight;

			return minTop;
		}
	}, {
		key: 'checkLeft',
		value: function checkLeft(left) {
			var maxLeft = 0;
			var minLeft = void 0;
			var zoom = this.app.state.zoom;

			minLeft = this.getMinLeft();

			if (left > maxLeft) left = maxLeft;
			if (left < minLeft) left = minLeft;

			return left;
		}
	}, {
		key: 'getMinLeft',
		value: function getMinLeft(zoom) {
			var minLeft = 0;

			if (zoom === undefined) {
				zoom = this.app.state.zoom;
			}

			minLeft = this.container.clientWidth / zoom - this.map.clientWidth;

			return minLeft;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this5 = this;

			var zoom = this.app.state.zoom;
			var newZoom = this.app.state.newZoom;
			var left = this.app.state.left;
			var top = this.app.state.top;
			var contWidth = void 0;
			var contHeight = void 0;
			var mapWidth = void 0;
			var mapHeight = void 0;
			var companies = [];
			var categories = [];
			var areas = [];
			var zoomCond = void 0;
			var mapClasses = 'map';

			if (!this.app.state.showMap) {
				return React.createElement('div', null);
			}

			if (this.container && this.map) {
				contWidth = this.container.clientWidth;
				contHeight = this.container.clientHeight;
				mapWidth = this.map.clientWidth;
				mapHeight = this.map.clientHeight;
			}

			if (newZoom > zoom) {
				if (newZoom > 1) {
					newZoom = zoom;
				}
			}

			if (newZoom < zoom) {
				zoomCond = contWidth / newZoom < mapWidth && contHeight / newZoom < mapHeight;

				if (!zoomCond) {
					newZoom = zoom;
				}
			}

			if (newZoom != this.app.state.zoom) {
				left = contWidth / 2 / newZoom - (Math.abs(left) + contWidth / 2 / zoom);
				top = contHeight / 2 / newZoom - (Math.abs(top) + contHeight / 2 / zoom);

				this.app.setState({ zoom: newZoom });
			}

			if (this.map) {
				left = this.checkLeft(left);
				top = this.checkTop(top);
				left = left.toFixed(0) * 1;
				top = top.toFixed(0) * 1;

				this.app.state.left = left;
				this.app.state.top = top;
			}

			var style = {
				transform: 'scale(' + zoom + ') translateX(' + left + 'px) translateY(' + top + 'px)'
			};

			areas = this.app.divisions.map(function (area) {
				return React.createElement(MapArea, { data: area, click: _this5.areaClick });
			});
			categories = this.app.categories.map(function (category) {
				return React.createElement(MapButton, { data: category, click: _this5.catClick });
			});
			companies = this.app.state.companies.map(function (company) {
				return React.createElement(Car, { app: _this5.app, data: company, click: _this5.app.showPopup });
			});

			if (this.app.state.mode == 'list') {
				return React.createElement('div', null);
			}

			return React.createElement(
				'div',
				null,
				React.createElement(
					'div',
					{
						className: 'map-container',
						onWheel: this.Wheel,
						onMouseMove: this.mouseMove,
						onMouseLeave: this.mouseOut,
						ref: function ref(container) {
							return _this5.app.container = _this5.container = container;
						}
					},
					React.createElement(
						'div',
						{
							className: mapClasses,
							style: style,
							onMouseDown: this.mouseDown,
							onMouseUp: this.mouseUp,
							ref: function ref(map) {
								return _this5.app.map = _this5.map = map;
							}
						},
						companies,
						categories,
						areas
					),
					React.createElement(Logos, { app: this.app, mode: 'map' })
				)
			);
		}
	}]);

	return Map;
}(React.Component);

;

function Car(props) {
	var categories = props.app.categories;
	var offset = void 0;

	for (var i = 0; i < categories.length; i++) {
		if (props.data.categoryId == categories[i].id) {
			offset = categories[i].offset;
			break;
		}
	}

	return React.createElement(
		'div',
		{ className: 'car', 'data-id': props.data.id, onClick: props.click },
		React.createElement(CarCircle, {
			position: props.data.position,
			show: props.data.showCircle
		}),
		React.createElement(CarImage, {
			position: props.data.position,
			image: props.data.image,
			baseImageDir: props.app.baseImageDir
		}),
		React.createElement(CarCaption, {
			position: props.data.position,
			show: props.data.showCaption,
			orientation: props.data.captionOrientation,
			name: props.data.name,
			percent: props.data.percent,
			favorite: props.data.favorite,
			offset: offset
		})
	);
}

function CarCircle(props) {
	var style = {
		opacity: props.show ? 1 : 0,
		top: props.position.top,
		left: props.position.left
	};

	return React.createElement('div', { className: 'car__circle', style: style });
}

function CarImage(props) {
	var dir = props.baseImageDir + 'cars/';
	var style = {
		top: props.position.top,
		left: props.position.left,
		backgroundImage: 'url(' + dir + props.image + ')'
	};

	return React.createElement('div', { className: 'car__image', style: style });
}

function CarCaption(props) {
	var classes = ['car__caption'];
	var offset = props.offset;
	var style = {
		top: props.position.top,
		left: props.position.left,
		opacity: props.show ? 1 : 0
	};

	switch (props.orientation) {
		case 'tl':
			classes.push('car__caption_tl');
			break;
		case 'tr':
			classes.push('car__caption_tr');
			break;
		case 'br':
			classes.push('car__caption_br');
			break;
		case 'bl':
			classes.push('car__caption_bl');
			break;
		default:
			classes.push('car__caption_tl');
	}

	if (offset) {
		style.top += offset.top;
		style.left += offset.left;
	}

	if (props.favorite) {
		classes.push('car__caption_favorite');

		return React.createElement(
			'div',
			{ className: 'car__caption-container', style: style },
			React.createElement(
				'div',
				{ className: classes.join(' ') },
				React.createElement(
					'div',
					{ className: 'car__caption-company' },
					props.name
				),
				React.createElement('div', { className: 'car__caption-favorite' })
			)
		);
	} else {
		return React.createElement(
			'div',
			{ className: 'car__caption-container', style: style },
			React.createElement(
				'div',
				{ className: classes.join(' ') },
				React.createElement(
					'div',
					{ className: 'car__caption-company' },
					props.name
				),
				React.createElement(
					'div',
					{ className: 'car__caption-percent' },
					props.percent,
					'%'
				)
			)
		);
	}
}

function MapButton(props) {
	var style = {
		left: props.data.buttonPosition.left,
		top: props.data.buttonPosition.top,
		width: props.data.dimensions.width,
		height: props.data.dimensions.height
	};

	return React.createElement('button', { className: 'category', 'data-category-id': props.data.id, onClick: props.click, style: style });
}

function MapArea(props) {
	var style = {
		top: props.data.button.top,
		left: props.data.button.left,
		width: props.data.button.width,
		height: props.data.button.height
	};

	return React.createElement('div', { className: 'area', style: style, 'data-id': props.data.id, onClick: props.click });
}

var Divisions = function (_React$Component4) {
	_inherits(Divisions, _React$Component4);

	function Divisions(props) {
		_classCallCheck(this, Divisions);

		var _this6 = _possibleConstructorReturn(this, (Divisions.__proto__ || Object.getPrototypeOf(Divisions)).call(this, props));

		_this6.app = props.app;
		return _this6;
	}

	_createClass(Divisions, [{
		key: 'render',
		value: function render() {
			var _this7 = this;

			var divisions = this.app.divisions.map(function (division) {
				return React.createElement(Division, { division: division, app: _this7.app });
			});

			if (!this.app.state.showDivisions) {
				return React.createElement('div', null);
			}

			if (this.app.state.mode != 'list') {
				return React.createElement('div', null);
			}

			return React.createElement(
				'div',
				{ className: 'divisions-desktop' },
				React.createElement(Logos, { app: this.app, mode: 'list' }),
				divisions
			);
		}
	}]);

	return Divisions;
}(React.Component);

;

function Division(props) {
	var categories = [];
	var division = props.division;

	props.app.categories.forEach(function (category) {
		if (category.divisionId == division.id) {
			categories.push(React.createElement(Category, { companies: props.app.state.companies, category: category, app: props.app }));
		}
	});

	return React.createElement(
		'section',
		{ className: 'division', id: 'division-' + division.id },
		React.createElement(
			'h1',
			{ className: 'division__header' },
			division.name
		),
		React.createElement(
			'div',
			{ className: 'division__items' },
			categories
		)
	);
}

function Category(props) {
	var category = props.category;
	var companies = [];

	props.companies.forEach(function (company) {
		if (company.categoryId == category.id) {
			companies.push(company);
		}
	});

	return React.createElement(
		'div',
		{ className: 'division__item' },
		React.createElement(
			'h2',
			{ className: 'division__item-header' },
			React.createElement(
				'span',
				{ className: 'division__item-header-ru' },
				category.nameRu
			),
			React.createElement(
				'span',
				{ className: 'division__item-header-en' },
				category.nameEn
			)
		),
		React.createElement(CompanyList, { companies: companies, app: props.app })
	);
}

function CompanyList(props) {
	var companies = props.companies.map(function (company) {
		return React.createElement(CompanyItem, { company: company, click: props.app.showPopup });
	});

	return React.createElement(
		'div',
		{ className: 'division__item-list' },
		companies
	);
}

function CompanyItem(props) {
	var company = props.company;
	var classes = 'division__item-link';

	if (company.favorite) {
		classes += ' division__item-link_fav';
	}

	return React.createElement(
		'a',
		{ href: '#', 'data-id': company.id, className: classes, onClick: props.click },
		company.name
	);
}

function Popup(props) {
	var app = props.app;
	var company = app.popupCompany || {};
	var category = void 0;
	var division = void 0;
	var contStyle = {
		display: app.state.showPopup ? 'flex' : 'none'
	};
	var popupStyle = {
		backgroundImage: 'url(' + app.baseImageDir + 'cars/' + company.image + ')'
	};

	if (!app.popupDivision) {
		return React.createElement('div', null);
	}

	return React.createElement(
		'div',
		{ className: 'popup__container', style: contStyle },
		React.createElement('div', { className: 'popup__close', onClick: app.hidePopup }),
		React.createElement(
			'div',
			{ className: 'popup' },
			React.createElement(
				'button',
				{ className: 'popup__close-button', onClick: app.hidePopup },
				'\xD7'
			),
			React.createElement(
				'div',
				{ className: 'popup__inner' },
				React.createElement('div', { className: 'popup__left', style: popupStyle }),
				React.createElement(
					'div',
					{ className: 'popup__right' },
					React.createElement(
						'div',
						{ className: 'popup__company' },
						company.name
					),
					React.createElement(
						'div',
						{ className: 'popup__cats' },
						app.popupDivision.name,
						'/',
						app.popupCategory.nameRu
					),
					company.url && React.createElement(
						'a',
						{ href: company.url, className: 'popup__link', target: '_blank' },
						company.url
					),
					!company.favorite && React.createElement(
						'div',
						{ className: 'popup__percent' },
						React.createElement(
							'div',
							{ className: 'popup__percent-title' },
							'\u0414\u043E\u043B\u044F \u0441\u043E\u0442\u0440\u0443\u0434\u043D\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043D\u0430 \u0440\u044B\u043D\u043A\u0435'
						),
						React.createElement(
							'div',
							{ className: 'popup__percent-value' },
							company.percent,
							'%'
						)
					),
					React.createElement(PopupRate, { rate: company.rate })
				)
			)
		)
	);
}

function PopupRate(props) {
	var rate = props.rate;

	if (rate > 0) {
		rate = rate.toFixed(2) + '';
		rate = rate.replace('.', ',');
	}

	if (rate) {
		return React.createElement(
			'div',
			null,
			React.createElement(
				'div',
				{ className: 'popup__rate' },
				React.createElement(
					'div',
					{ className: 'popup__rate-title' },
					'\u041A\u0430\u0447\u0435\u0441\u0442\u0432\u043E \u0441\u0435\u0440\u0432\u0438\u0441\u0430'
				),
				React.createElement(
					'div',
					{ className: 'popup__rate-value' },
					props.rate,
					'*'
				)
			),
			React.createElement(
				'div',
				{ className: 'popup__note' },
				'*\u041F\u043E 10 \u0431\u0430\u043B\u043B\u044C\u043D\u043E\u0439 \u0448\u043A\u0430\u043B\u0435. \u041F\u043E \u043E\u0446\u0435\u043D\u043A\u0435 \u043A\u043B\u0438\u0435\u043D\u0442\u043E\u0432 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0438.'
			)
		);
	} else {
		return React.createElement('div', null);
	}
}

function getItemById(items, id) {
	if (!items.length) return null;

	for (var i = 0; i < items.length; i++) {
		if (items[i].id == id) {
			return items[i];
		}
	}
}

var MobileDivisions = function (_React$Component5) {
	_inherits(MobileDivisions, _React$Component5);

	function MobileDivisions(props) {
		_classCallCheck(this, MobileDivisions);

		var _this8 = _possibleConstructorReturn(this, (MobileDivisions.__proto__ || Object.getPrototypeOf(MobileDivisions)).call(this, props));

		_this8.app = props.app;
		_this8.showCurrentDivision = _this8.showCurrentDivision.bind(_this8);
		_this8.returnHome = _this8.returnHome.bind(_this8);
		_this8.getDivisionCurrent = _this8.getDivisionCurrent.bind(_this8);
		return _this8;
	}

	_createClass(MobileDivisions, [{
		key: 'showCurrentDivision',
		value: function showCurrentDivision(e) {
			var id = e.currentTarget.getAttribute('data-id');
			this.app.setState({ currentDivision: id });
		}
	}, {
		key: 'returnHome',
		value: function returnHome() {
			this.app.setState({ currentDivision: null });
		}
	}, {
		key: 'getDivisionCurrent',
		value: function getDivisionCurrent() {
			var division = null;
			var id = this.app.state.currentDivision;

			for (var i = 0; i < this.app.divisions.length; i++) {
				if (id == this.app.divisions[i].id) {
					division = this.app.divisions[i];
					break;
				}
			}

			return division;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this9 = this;

			var divisions = this.app.divisions.map(function (division) {
				return React.createElement(MobileDivision, { division: division, show: _this9.showCurrentDivision, baseImageDir: _this9.app.baseImageDir });
			});

			if (!this.app.state.showMobileDivisions) {
				return React.createElement('div', null);
			}

			if (this.app.state.currentDivision === null) {
				return React.createElement(
					'div',
					{ className: 'divisions-mobile' },
					divisions
				);
			} else {
				return React.createElement(
					'div',
					{ className: 'divisions-mobile' },
					React.createElement(CurrentMobileDivision, {
						division: this.getDivisionCurrent(),
						back: this.returnHome,
						app: this.app,
						divisionId: this.app.state.currentDivision
					})
				);
			}
		}
	}]);

	return MobileDivisions;
}(React.Component);

function MobileDivision(props) {
	var style = {
		backgroundColor: props.division.color
	};

	return React.createElement(
		'div',
		{ className: 'division-mobile__cover', 'data-id': props.division.id, onClick: props.show },
		React.createElement('img', { src: props.baseImageDir + props.division.image }),
		React.createElement(
			'div',
			{ className: 'division-mobile__name', style: style },
			props.division.name
		)
	);
}

function CurrentMobileDivision(props) {
	var app = props.app;
	var baseImageDir = app.baseImageDir;
	var categories = [];
	var divisionStyle = {
		backgroundColor: props.division.color,
		backgroundImage: 'url(' + baseImageDir + 'icons/' + props.division.icon + ')',
		backgroundSize: props.division.iconSize
	};

	for (var i = 0; i < app.categories.length; i++) {
		if (props.divisionId == app.categories[i].divisionId) {
			categories.push(app.categories[i]);
		}
	}

	categories = categories.map(function (cat) {
		return React.createElement(MobileCategory, {
			divisionName: props.division.name,
			cat: cat,
			companies: app.state.companies,
			app: app
		});
	});

	return React.createElement(
		'div',
		{ className: 'division-mobile division-mobile_current' },
		React.createElement(
			'button',
			{ className: 'division-mobile__back', onClick: props.back, style: divisionStyle },
			React.createElement(
				'span',
				null,
				props.division.name
			)
		),
		categories
	);
}

function MobileCategory(props) {
	var companies = [];
	var app = props.app;

	for (var i = 0; i < props.companies.length; i++) {
		if (props.cat.id == props.companies[i].categoryId) {
			companies.push(props.companies[i]);
		}
	}

	companies = companies.map(function (company) {
		return React.createElement(MobileCompany, { company: company, division: props.divisionName, cat: props.cat.nameRu, app: app });
	});

	return React.createElement(
		'div',
		{ className: 'category-mobile' },
		React.createElement(
			'div',
			{ className: 'category-mobile__header' },
			React.createElement(
				'div',
				{ className: 'category-mobile__header-ru' },
				props.cat.nameRu
			),
			React.createElement(
				'div',
				{ className: 'category-mobile__header-en' },
				props.cat.nameEn
			)
		),
		React.createElement(
			'div',
			{ className: 'category-mobile__list' },
			companies
		)
	);
}

function MobileCompany(props) {
	var app = props.app;
	var company = props.company;
	var companyClasses = 'company-mobile company-mobile-' + company.id;
	var companyNameClasses = 'company-mobile__name';
	var companyDescClasses = 'company-mobile__desc';
	var showCompany = false;
	var currentCompany = app.state.currentCompany;
	var linkStyle = {
		display: company.url ? 'block' : 'none'
	};

	function toggleCompany(e) {
		e.currentTarget.classList.toggle('company-mobile__name_expand');
		e.currentTarget.parentNode.querySelector('.company-mobile__desc').classList.toggle('company-mobile__desc_expand');
		e.currentTarget.parentNode.classList.toggle('company-mobile_expand');
	}

	function scrollToCompany(e) {
		var headerHeight = void 0;
		var offsetTop = void 0;
		var backHeight = void 0;

		if (!showCompany || !e) {
			return false;
		}

		headerHeight = document.querySelector('.header').clientHeight;
		offsetTop = e.offsetTop;
		backHeight = document.querySelector('.division-mobile__back').clientHeight;

		window.scroll({
			top: offsetTop - headerHeight - backHeight,
			left: 0,
			behavior: 'smooth'
		});
	}

	if (!company.rate) {
		company.rate = 0;
	} else {
		company.rate = parseInt(company.rate, 10);
	}

	if (company.favorite) {
		companyNameClasses += ' company-mobile__name_favorite';
	}

	company.rate = company.rate.toFixed(1) + '';
	company.rate = company.rate.replace('.', ',');

	if (company.id == currentCompany) {
		companyClasses += ' company-mobile_expand';
		companyNameClasses += ' company-mobile__name_expand';
		companyDescClasses += ' company-mobile__desc_expand';
		showCompany = true;

		app.popupCompany = null;
	}

	return React.createElement(
		'div',
		{ className: companyClasses },
		React.createElement(
			'button',
			{ className: companyNameClasses, onClick: toggleCompany, ref: scrollToCompany },
			React.createElement(
				'span',
				null,
				company.name
			)
		),
		React.createElement(
			'div',
			{ className: companyDescClasses },
			React.createElement(
				'div',
				{ className: 'company-mobile__division' },
				props.division,
				' / ',
				props.cat
			),
			React.createElement(
				'div',
				{ className: 'company-mobile__market' },
				React.createElement(
					'div',
					{ className: 'company-mobile__market-title' },
					'\u0417\u0430\u043D\u0438\u043C\u0430\u0435\u043C\u044B\u0439 \u043F\u0440\u043E\u0446\u0435\u043D\u0442 \u0440\u044B\u043D\u043A\u0430'
				),
				React.createElement(
					'div',
					{ className: 'company-mobile__market-value' },
					company.percent,
					'%'
				)
			),
			React.createElement(
				'div',
				{ className: 'company-mobile__rate' },
				React.createElement(
					'div',
					{ className: 'company-mobile__rate-title' },
					'\u0423\u0434\u043E\u0432\u043B\u0435\u0442\u0432\u043E\u0440\u0435\u043D\u043D\u043E\u0441\u0442\u044C \u0441\u0435\u0440\u0432\u0438\u0441\u043E\u043C'
				),
				React.createElement(
					'div',
					{ className: 'company-mobile__rate-value' },
					company.rate,
					'/10'
				)
			),
			React.createElement(
				'a',
				{ href: company.url, className: 'company-mobile__link', style: linkStyle },
				company.url
			)
		)
	);
}

var Control = function (_React$Component6) {
	_inherits(Control, _React$Component6);

	function Control(props) {
		_classCallCheck(this, Control);

		var _this10 = _possibleConstructorReturn(this, (Control.__proto__ || Object.getPrototypeOf(Control)).call(this, props));

		_this10.legend = _this10.legend.bind(_this10);
		_this10.zoom = _this10.zoom.bind(_this10);
		_this10.mode = _this10.mode.bind(_this10);
		_this10.app = props.app;
		return _this10;
	}

	_createClass(Control, [{
		key: 'legend',
		value: function legend() {
			this.app.setState({
				showLegend: !this.app.state.showLegend
			});
		}
	}, {
		key: 'zoom',
		value: function zoom(e) {
			var type = e.currentTarget.getAttribute('data-type');
			var step = 0.1;
			var zoom = this.app.state.zoom;
			var newZoom = void 0;

			switch (type) {
				case 'plus':
					newZoom = zoom + step;
					break;
				case 'minus':
					newZoom = zoom - step;
					break;
			}

			this.app.setState({
				newZoom: newZoom
			});
		}
	}, {
		key: 'mode',
		value: function mode() {
			var mode = this.app.state.mode;
			var navE = document.querySelectorAll('.header__nav-a:not(.header__nav-a_search)');

			if (mode == 'map') {
				mode = 'list';
				this.app.setState({
					showLegend: false
				});
			} else {
				mode = 'map';
			}

			this.app.setState({
				mode: mode
			});

			for (var i = 0; i < navE.length; i++) {
				navE[i].classList.remove('header__nav-a_current');
			}
		}
	}, {
		key: 'render',
		value: function render() {

			if (isMobile()) {
				return React.createElement('div', null);
			}

			var modeClassName = 'control__button control__button_mode control__button_mode-' + this.app.state.mode;

			return React.createElement(
				'div',
				{ className: 'control' },
				React.createElement(
					'div',
					{ className: 'control__inner' },
					React.createElement(
						'button',
						{ className: 'control__button control__button_legend', onClick: this.app.toggleLegend },
						React.createElement('span', null)
					),
					React.createElement(ControlZoom, { app: this.app, zoom: this.zoom }),
					React.createElement(
						'div',
						{ className: 'control__switch', onClick: this.mode },
						React.createElement(
							'button',
							{ className: modeClassName },
							React.createElement('span', null)
						)
					)
				)
			);
		}
	}]);

	return Control;
}(React.Component);

function ControlZoom(props) {
	var app = props.app;

	if (app.state.mode != 'map') {
		return React.createElement('div', null);
	}

	return React.createElement(
		'div',
		{ className: 'control__zoom' },
		React.createElement(
			'button',
			{ className: 'control__button control__button_plus', 'data-type': 'plus', onClick: props.zoom },
			React.createElement('span', null)
		),
		React.createElement(
			'button',
			{ className: 'control__button control__button_minus', 'data-type': 'minus', onClick: props.zoom },
			React.createElement('span', null)
		)
	);
}

function Legend(props) {
	var app = props.app;
	var classes = 'legend';

	if (app.state.showLegend && !isMobile()) {
		classes += ' legend_show';
	}

	return React.createElement(
		'div',
		{ className: classes },
		React.createElement(
			'div',
			{ className: 'legend__inner' },
			React.createElement(
				'div',
				{ className: 'legend__body' },
				React.createElement('button', { className: 'legend__close', onClick: app.toggleLegend }),
				React.createElement('div', { className: 'legend__logo' }),
				React.createElement(
					'div',
					{ className: 'legend__header' },
					'TECHNOLOGY MAP ',
					React.createElement(
						'span',
						null,
						'2017'
					)
				),
				React.createElement(
					'div',
					{ className: 'legend__text' },
					'\u041D\u0430 \u043A\u0430\u0440\u0442\u0435 \u043F\u0440\u0435\u0434\u0441\u0442\u0430\u0432\u043B\u0435\u043D\u044B \u043B\u0438\u0434\u0435\u0440\u044B \u043F\u043E \u0434\u043E\u043B\u0435 \u0440\u044B\u043D\u043A\u0430 \u0441\u0440\u0435\u0434\u0438 \u043E\u043F\u0440\u043E\u0448\u0435\u043D\u043D\u044B\u0445 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0439 \u0432 \u0440\u0435\u0439\u0442\u0438\u043D\u0433\u0435 Techology Index 2017, \u0430 \u0442\u0430\u043A\u0436\u0435 \u043F\u0430\u0440\u0442\u043D\u0435\u0440\u044B \u043F\u0440\u043E\u0435\u043A\u0442\u0430.'
				),
				React.createElement(
					'div',
					{ className: 'legend_signs' },
					React.createElement(
						'div',
						{ className: 'legend__sign' },
						React.createElement('div', { className: 'legend__sign-icon legend__sign-icon_division' }),
						React.createElement(
							'div',
							{ className: 'legend__sign-text' },
							'\u0421\u0435\u043A\u0442\u043E\u0440\u044B \u0440\u044B\u043D\u043A\u0430'
						)
					),
					React.createElement(
						'div',
						{ className: 'legend__sign' },
						React.createElement('div', { className: 'legend__sign-icon legend__sign-icon_category' }),
						React.createElement(
							'div',
							{ className: 'legend__sign-text' },
							'\u0421\u0444\u0435\u0440\u044B \u0434\u0435\u044F\u0442\u0435\u043B\u044C\u043D\u043E\u0441\u0442\u0438 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0439'
						)
					),
					React.createElement(
						'div',
						{ className: 'legend__sign' },
						React.createElement('div', { className: 'legend__sign-icon legend__sign-icon_title' }),
						React.createElement(
							'div',
							{ className: 'legend__sign-text' },
							'\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0438 \u0438 \u0434\u043E\u043B\u044F \u0440\u044B\u043D\u043A\u0430'
						)
					),
					React.createElement(
						'div',
						{ className: 'legend__sign' },
						React.createElement('div', { className: 'legend__sign-icon legend__sign-icon_partners' }),
						React.createElement(
							'div',
							{ className: 'legend__sign-text' },
							'\u041F\u0430\u0440\u0442\u043D\u0435\u0440\u044B \u043F\u0440\u043E\u0435\u043A\u0442\u0430'
						)
					)
				),
				React.createElement(
					'div',
					{ className: 'legend__icons' },
					React.createElement(
						'div',
						{ className: 'legend__icons-col' },
						React.createElement(
							'div',
							{ className: 'legend__icons-header' },
							'\u041F\u0410\u0420\u0422\u041D\u0415\u0420\u042B \u041F\u0420\u041E\u0415\u041A\u0422\u0410'
						),
						React.createElement('div', { className: 'legend__icon legend__icon_p1' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p2' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p3' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p4' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p5' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p6' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p7' }),
						React.createElement('div', { className: 'legend__icon legend__icon_p8' })
					),
					React.createElement(
						'div',
						{ className: 'legend__icons-col' },
						React.createElement(
							'div',
							{ className: 'legend__icons-header' },
							'\u041B\u0418\u0414\u0415\u0420\u042B \u0420\u0415\u0419\u0422\u0418\u041D\u0413\u0410'
						),
						React.createElement('div', { className: 'legend__icon legend__icon_l1' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l2' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l3' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l4' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l5' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l6' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l7' }),
						React.createElement('div', { className: 'legend__icon legend__icon_l8' })
					)
				)
			),
			React.createElement(
				'div',
				{ className: 'legend__footer' },
				'\u041A\u0430\u0440\u0442\u0430 \u0441\u043E\u0437\u0434\u0430\u043D\u0430',
				React.createElement('a', { href: 'http://happylab.ru/', target: '_blank', className: 'legend__dev' })
			)
		)
	);
}

var Logos = function (_React$Component7) {
	_inherits(Logos, _React$Component7);

	function Logos(props) {
		_classCallCheck(this, Logos);

		var _this11 = _possibleConstructorReturn(this, (Logos.__proto__ || Object.getPrototypeOf(Logos)).call(this, props));

		_this11.app = props.app;
		_this11.slick = _this11.slick.bind(_this11);
		_this11.hide = _this11.hide.bind(_this11);
		_this11.whenShow = props.mode;
		return _this11;
	}

	_createClass(Logos, [{
		key: 'slick',
		value: function slick(e) {
			this.app.logoContainer = e;
			$(e).slick({
				slidesToShow: 9,
				slidesToScroll: 9,
				autoplay: true,
				nextArrow: '.logo__button_next',
				prevArrow: '.logo__button_prev',
				responsive: [{
					breakpoint: 1750,
					settings: {
						slidesToShow: 8,
						slidesToScroll: 8
					}
				}, {
					breakpoint: 1600,
					settings: {
						slidesToShow: 7,
						slidesToScroll: 7
					}
				}, {
					breakpoint: 1250,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 6
					}
				}, {
					breakpoint: 1024,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 5
					}
				}]
			});
		}
	}, {
		key: 'hide',
		value: function hide() {
			this.app.showLogo = false;
			this.app.setState({
				showLogo: false
			});
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			$(this.app.logoContainer).slick('unslick');
		}
	}, {
		key: 'render',
		value: function render() {
			var _this12 = this;

			var logos = this.app.logos.map(function (logo) {
				return React.createElement(Logo, { image: logo.image, url: logo.url, border: logo.border, app: _this12.app });
			});
			var classes = void 0;

			if (this.whenShow == 'list') {
				classes = 'logo_static';
			} else {
				classes = 'logo';
			}

			if (!this.app.state.showLogo) {
				if (this.whenShow == 'list') {
					classes += ' logo_hide-static';
				} else {
					classes += ' logo_hide';
				}
			}

			if (this.whenShow != this.app.state.mode) {
				return React.createElement('div', null);
			}

			if (isMobile()) {
				return React.createElement('div', null);
			}

			return React.createElement(
				'div',
				{ className: classes },
				React.createElement('button', { className: 'logo__close', onClick: this.hide }),
				React.createElement(
					'button',
					{ className: 'logo__button logo__button_prev' },
					React.createElement('span', null)
				),
				React.createElement(
					'button',
					{ className: 'logo__button logo__button_next' },
					React.createElement('span', null)
				),
				React.createElement(
					'div',
					{ className: 'logo__items', ref: this.slick },
					logos
				)
			);
		}
	}]);

	return Logos;
}(React.Component);

function Logo(props) {
	var baseImageDir = props.app.baseImageDir;
	var image = baseImageDir + 'logo/' + props.image;
	var classes = '';

	if (props.border) {
		classes = 'border';
	}

	return React.createElement(
		'div',
		{ className: 'logo__item' },
		React.createElement(
			'div',
			{ className: 'logo__item-image' },
			React.createElement(
				'a',
				{ href: props.url || '#', target: '_blank', className: classes },
				React.createElement('img', { src: image })
			)
		)
	);
}

function isMobile() {
	var mediaQ = '(max-width: 1023px)';

	return window.matchMedia(mediaQ).matches;
}

ReactDOM.render(React.createElement(App, null), document.getElementById('app'));
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanN4Il0sIm5hbWVzIjpbIkFwcCIsInByb3BzIiwic2hvd1BvcHVwIiwiYmluZCIsImhpZGVQb3B1cCIsInRvZ2dsZUxlZ2VuZCIsInJlc2l6ZUFwcCIsImhpZGVCeUNsaWNrQXJlYSIsImRpdmlzaW9ucyIsImNhdGVnb3JpZXMiLCJsb2dvcyIsInBvcHVwQ29tcGFueSIsInBvcHVwQ2F0ZWdvcnkiLCJwb3B1cERpdmlzaW9uIiwiYmFzZUltYWdlRGlyIiwic3RhdGUiLCJjb21wYW5pZXMiLCJ6b29tIiwibmV3Wm9vbSIsImxlZnQiLCJ0b3AiLCJzaG93TWFwIiwic2hvd0RpdmlzaW9ucyIsInNob3dNb2JpbGVEaXZpc2lvbnMiLCJjdXJyZW50RGl2aXNpb24iLCJjdXJyZW50Q2F0Iiwic2hvd0xlZ2VuZCIsIm1vZGUiLCJzaG93TG9nbyIsInNob3dTZWFyY2giLCJlIiwiY29tcGFueSIsImlkIiwiY3VycmVudFRhcmdldCIsImdldEF0dHJpYnV0ZSIsImkiLCJsZW5ndGgiLCJmYXZvcml0ZSIsInNldFN0YXRlIiwid2luZG93Iiwib3BlbiIsInVybCIsInNob3dDYXB0aW9uIiwiZ2V0SXRlbUJ5SWQiLCJjYXRlZ29yeUlkIiwiZGl2aXNpb25JZCIsInByZXZlbnREZWZhdWx0Iiwic2hvdyIsImlzTW9iaWxlIiwiY3VycmVudEVsZW0iLCJ0YXJnZXQiLCJjbGFzc05hbWUiLCJpbmRleE9mIiwicGFyZW50Tm9kZSIsImFkZEV2ZW50TGlzdGVuZXIiLCJkb2N1bWVudCIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJSZWFjdCIsIkNvbXBvbmVudCIsIkhlYWRlciIsIkNoYW5nZSIsIkVudGVyIiwiTGVhdmUiLCJJdGVtQ2xpY2siLCJJdGVtTGVhdmUiLCJDbGljayIsImZvY3VzIiwiZmlsdGVyQ29tcGFuaWVzIiwiZ29Ub0RpdmlzaW9uIiwiY2xlYXIiLCJhcHAiLCJzZWFyY2giLCJzZWFyY2hMaXN0Iiwic2VhcmNoUGxhY2Vob2xkZXIiLCJzaG93U2VhcmNoTGlzdCIsInZhbHVlIiwic2hvd0NpcmNsZSIsIm5hbWUiLCJjb250SGFsZldpZHRoIiwiY29udEhhbGZIZWlnaHQiLCJjb250YWluZXIiLCJjbGllbnRXaWR0aCIsImNsaWVudEhlaWdodCIsInBvc2l0aW9uIiwiaiIsImsiLCJjdXJyZW50Q29tcGFueSIsImxpc3QiLCJjb3VudCIsInRvTG93ZXJDYXNlIiwicHVzaCIsImNhdGVnb3J5TmFtZSIsIm5hbWVSdSIsImRpdmlzaW9uIiwiZGl2aXNpb25FIiwib2Zmc2V0VG9wIiwiaGVhZGVyRSIsImhlYWRlckhlaWdodCIsIm5hdkUiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZ2V0RWxlbWVudEJ5SWQiLCJxdWVyeVNlbGVjdG9yIiwic2Nyb2xsIiwiYmVoYXZpb3IiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJhZGQiLCJtYXAiLCJTZWFyY2giLCJwbGFjZWhvbGRlciIsInN0eWxlIiwiZGlzcGxheSIsImxpc3RDbGFzc2VzIiwiY2xlYXJDbGFzc2VzIiwic3Vic3RyIiwiZW50ZXIiLCJjbGljayIsIml0ZW1sZWF2ZSIsInNob3dMaXN0IiwiY2hhbmdlIiwibGVhdmUiLCJNYXAiLCJXaGVlbCIsIm1vdXNlRG93biIsIm1vdXNlVXAiLCJtb3VzZU1vdmUiLCJtb3VzZU91dCIsImNoZWNrVG9wIiwiZ2V0TWluVG9wIiwiY2hlY2tMZWZ0IiwiZ2V0TWluTGVmdCIsImNhdENsaWNrIiwiYXJlYUNsaWNrIiwic3RhcnREcmFnIiwib2Zmc2V0TGVmdCIsInpvb21TdGVwIiwiZGVsdGFZIiwidG9GaXhlZCIsInBhZ2VYIiwicGFnZVkiLCJjYXRJZCIsIm1heFRvcCIsIm1pblRvcCIsInVuZGVmaW5lZCIsIm1heExlZnQiLCJtaW5MZWZ0IiwiY29udFdpZHRoIiwiY29udEhlaWdodCIsIm1hcFdpZHRoIiwibWFwSGVpZ2h0IiwiYXJlYXMiLCJ6b29tQ29uZCIsIm1hcENsYXNzZXMiLCJNYXRoIiwiYWJzIiwidHJhbnNmb3JtIiwiYXJlYSIsImNhdGVnb3J5IiwiQ2FyIiwib2Zmc2V0IiwiZGF0YSIsImltYWdlIiwiY2FwdGlvbk9yaWVudGF0aW9uIiwicGVyY2VudCIsIkNhckNpcmNsZSIsIm9wYWNpdHkiLCJDYXJJbWFnZSIsImRpciIsImJhY2tncm91bmRJbWFnZSIsIkNhckNhcHRpb24iLCJjbGFzc2VzIiwib3JpZW50YXRpb24iLCJqb2luIiwiTWFwQnV0dG9uIiwiYnV0dG9uUG9zaXRpb24iLCJ3aWR0aCIsImRpbWVuc2lvbnMiLCJoZWlnaHQiLCJNYXBBcmVhIiwiYnV0dG9uIiwiRGl2aXNpb25zIiwiRGl2aXNpb24iLCJmb3JFYWNoIiwiQ2F0ZWdvcnkiLCJuYW1lRW4iLCJDb21wYW55TGlzdCIsIkNvbXBhbnlJdGVtIiwiUG9wdXAiLCJjb250U3R5bGUiLCJwb3B1cFN0eWxlIiwicmF0ZSIsIlBvcHVwUmF0ZSIsInJlcGxhY2UiLCJpdGVtcyIsIk1vYmlsZURpdmlzaW9ucyIsInNob3dDdXJyZW50RGl2aXNpb24iLCJyZXR1cm5Ib21lIiwiZ2V0RGl2aXNpb25DdXJyZW50IiwiTW9iaWxlRGl2aXNpb24iLCJiYWNrZ3JvdW5kQ29sb3IiLCJjb2xvciIsIkN1cnJlbnRNb2JpbGVEaXZpc2lvbiIsImRpdmlzaW9uU3R5bGUiLCJpY29uIiwiYmFja2dyb3VuZFNpemUiLCJpY29uU2l6ZSIsImNhdCIsImJhY2siLCJNb2JpbGVDYXRlZ29yeSIsImRpdmlzaW9uTmFtZSIsIk1vYmlsZUNvbXBhbnkiLCJjb21wYW55Q2xhc3NlcyIsImNvbXBhbnlOYW1lQ2xhc3NlcyIsImNvbXBhbnlEZXNjQ2xhc3NlcyIsInNob3dDb21wYW55IiwibGlua1N0eWxlIiwidG9nZ2xlQ29tcGFueSIsInRvZ2dsZSIsInNjcm9sbFRvQ29tcGFueSIsImJhY2tIZWlnaHQiLCJwYXJzZUludCIsIkNvbnRyb2wiLCJsZWdlbmQiLCJ0eXBlIiwic3RlcCIsIm1vZGVDbGFzc05hbWUiLCJDb250cm9sWm9vbSIsIkxlZ2VuZCIsIkxvZ29zIiwic2xpY2siLCJoaWRlIiwid2hlblNob3ciLCJsb2dvQ29udGFpbmVyIiwiJCIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwiYXV0b3BsYXkiLCJuZXh0QXJyb3ciLCJwcmV2QXJyb3ciLCJyZXNwb25zaXZlIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwibG9nbyIsImJvcmRlciIsIkxvZ28iLCJtZWRpYVEiLCJtYXRjaE1lZGlhIiwibWF0Y2hlcyIsIlJlYWN0RE9NIiwicmVuZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0lBQU1BLEc7OztBQUNMLGNBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSx3R0FDWkEsS0FEWTs7QUFFbEIsUUFBS0MsU0FBTCxHQUFpQixNQUFLQSxTQUFMLENBQWVDLElBQWYsT0FBakI7QUFDQSxRQUFLQyxTQUFMLEdBQWlCLE1BQUtBLFNBQUwsQ0FBZUQsSUFBZixPQUFqQjtBQUNBLFFBQUtFLFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQkYsSUFBbEIsT0FBcEI7QUFDQSxRQUFLRyxTQUFMLEdBQWlCLE1BQUtBLFNBQUwsQ0FBZUgsSUFBZixPQUFqQjtBQUNBLFFBQUtJLGVBQUwsR0FBd0IsTUFBS0EsZUFBTCxDQUFxQkosSUFBckIsT0FBeEI7QUFDQSxRQUFLSyxTQUFMLEdBQWtCQSxTQUFsQjtBQUNBLFFBQUtDLFVBQUwsR0FBa0JBLFVBQWxCO0FBQ0EsUUFBS0MsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsUUFBS0MsWUFBTCxHQUFvQixFQUFwQjtBQUNBLFFBQUtDLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxRQUFLQyxhQUFMLEdBQXFCLEVBQXJCO0FBQ0EsUUFBS0MsWUFBTCxHQUFvQixRQUFwQjs7QUFFQSxRQUFLQyxLQUFMLEdBQWE7QUFDWkMsY0FBV0EsU0FEQztBQUVaQyxTQUFNLEdBRk07QUFHWkMsWUFBUyxHQUhHO0FBSVpDLFNBQU0sQ0FKTTtBQUtaQyxRQUFLLENBTE87QUFNWmxCLGNBQVcsS0FOQztBQU9abUIsWUFBUyxLQVBHO0FBUVpDLGtCQUFlLEtBUkg7QUFTWkMsd0JBQXFCLElBVFQ7QUFVWkMsb0JBQWlCLElBVkw7QUFXWkMsZUFBWSxDQVhBO0FBWVpDLGVBQVksS0FaQTtBQWFaQyxTQUFNLEtBYk07QUFjWkMsYUFBVSxJQWRFO0FBZVpDLGVBQVk7QUFmQSxHQUFiO0FBZmtCO0FBZ0NsQjs7Ozs0QkFFU0MsQyxFQUFHQyxPLEVBQVM7QUFDckIsT0FBSUMsV0FBSjtBQUNBLE9BQUloQixZQUFZLEtBQUtELEtBQUwsQ0FBV0MsU0FBM0I7O0FBRUEsT0FBSWMsQ0FBSixFQUFPO0FBQ05FLFNBQUtGLEVBQUVHLGFBQUYsQ0FBZ0JDLFlBQWhCLENBQTZCLFNBQTdCLENBQUw7QUFDQSxJQUZELE1BRU87QUFDTkYsU0FBS0QsT0FBTDtBQUNBOztBQUVELFFBQUssSUFBSUksSUFBSSxDQUFiLEVBQWdCQSxJQUFJbkIsVUFBVW9CLE1BQTlCLEVBQXNDRCxHQUF0QyxFQUEyQztBQUMxQyxRQUFJbkIsVUFBVW1CLENBQVYsRUFBYUgsRUFBYixJQUFtQkEsRUFBbkIsSUFBeUIsQ0FBQ2hCLFVBQVVtQixDQUFWLEVBQWFFLFFBQTNDLEVBQXFEO0FBQ3BELFVBQUsxQixZQUFMLEdBQW9CSyxVQUFVbUIsQ0FBVixDQUFwQjs7QUFFQSxVQUFLRyxRQUFMLENBQWM7QUFDYnBDLGlCQUFXO0FBREUsTUFBZDs7QUFJQTtBQUNBLEtBUkQsTUFRTyxJQUFJYyxVQUFVbUIsQ0FBVixFQUFhSCxFQUFiLElBQW1CQSxFQUFuQixJQUF5QmhCLFVBQVVtQixDQUFWLEVBQWFFLFFBQTFDLEVBQW9EOztBQUUxRCxTQUFJLEtBQUt0QixLQUFMLENBQVdZLElBQVgsSUFBbUIsTUFBdkIsRUFBK0I7QUFDOUJZLGFBQU9DLElBQVAsQ0FBWXhCLFVBQVVtQixDQUFWLEVBQWFNLEdBQXpCO0FBQ0EsTUFGRCxNQUVPO0FBQ04sVUFBSXpCLFVBQVVtQixDQUFWLEVBQWFPLFdBQWpCLEVBQThCO0FBQzdCSCxjQUFPQyxJQUFQLENBQVl4QixVQUFVbUIsQ0FBVixFQUFhTSxHQUF6QjtBQUNBO0FBQ0R6QixnQkFBVW1CLENBQVYsRUFBYU8sV0FBYixHQUEyQixDQUFDMUIsVUFBVW1CLENBQVYsRUFBYU8sV0FBekM7QUFDQTs7QUFFRCxVQUFLSixRQUFMLENBQWM7QUFDYnRCLGlCQUFXQTtBQURFLE1BQWQ7O0FBSUE7QUFDQTtBQUNEOztBQUVELE9BQUksS0FBS0wsWUFBTCxDQUFrQnFCLEVBQXRCLEVBQTBCO0FBQ3pCLFNBQUtwQixhQUFMLEdBQXFCK0IsWUFBWSxLQUFLbEMsVUFBakIsRUFBNkIsS0FBS0UsWUFBTCxDQUFrQmlDLFVBQS9DLENBQXJCO0FBQ0EsSUFGRCxNQUVPO0FBQ04sU0FBS2hDLGFBQUwsR0FBcUIsRUFBckI7QUFDQTs7QUFFRCxPQUFJLEtBQUtBLGFBQVQsRUFBd0I7QUFDdkIsU0FBS0MsYUFBTCxHQUFxQjhCLFlBQVksS0FBS25DLFNBQWpCLEVBQTRCLEtBQUtJLGFBQUwsQ0FBbUJpQyxVQUEvQyxDQUFyQjtBQUNBLElBRkQsTUFFTztBQUNOLFNBQUtoQyxhQUFMLEdBQXFCLEVBQXJCO0FBQ0E7O0FBRUQsT0FBSWlCLENBQUosRUFBTztBQUNOQSxNQUFFZ0IsY0FBRjtBQUNBOztBQUVELFFBQUtSLFFBQUwsQ0FBYztBQUNiWixnQkFBWTtBQURDLElBQWQ7QUFHQTs7OzhCQUVXO0FBQ1gsUUFBS1ksUUFBTCxDQUFjLEVBQUNwQyxXQUFXLEtBQVosRUFBZDtBQUNBOzs7aUNBRWM7QUFDZCxPQUFJNkMsT0FBTyxLQUFLaEMsS0FBTCxDQUFXVyxVQUF0Qjs7QUFFQSxRQUFLWSxRQUFMLENBQWM7QUFDYlosZ0JBQVksQ0FBQ3FCO0FBREEsSUFBZDtBQUdBOzs7OEJBRVc7QUFDWCxPQUFJQyxVQUFKLEVBQWdCO0FBQ2YsU0FBS1YsUUFBTCxDQUFjO0FBQ2JqQixjQUFTLEtBREk7QUFFYkMsb0JBQWUsS0FGRjtBQUdiQywwQkFBcUI7QUFIUixLQUFkO0FBS0EsSUFORCxNQU1PO0FBQ04sU0FBS2UsUUFBTCxDQUFjO0FBQ2JqQixjQUFTLElBREk7QUFFYkMsb0JBQWUsSUFGRjtBQUdiQywwQkFBcUI7QUFIUixLQUFkO0FBS0E7QUFDRDs7O2tDQUVlTyxDLEVBQUc7QUFDbEIsT0FBSW1CLGNBQWNuQixFQUFFb0IsTUFBcEI7QUFDQSxPQUFJQyxrQkFBSjs7QUFFQSxPQUFJO0FBQ0gsT0FBRztBQUNGQSxpQkFBWUYsWUFBWUUsU0FBeEI7O0FBRUEsU0FBSUEsU0FBSixFQUFlO0FBQ2QsVUFBSUEsVUFBVUMsT0FBVixDQUFrQixRQUFsQixLQUErQixDQUFDLENBQXBDLEVBQXVDO0FBQ3RDO0FBQ0E7O0FBRUQsVUFBSUQsVUFBVUMsT0FBVixDQUFrQixRQUFsQixLQUErQixDQUFDLENBQXBDLEVBQXVDO0FBQ3RDO0FBQ0E7QUFDRDtBQUVELEtBYkQsUUFhU0gsY0FBY0EsWUFBWUksVUFibkM7QUFjQSxJQWZELENBZUUsT0FBT3ZCLENBQVAsRUFBVTtBQUNYO0FBQ0E7O0FBRUQsUUFBS1EsUUFBTCxDQUFjO0FBQ2JULGdCQUFZLEtBREM7QUFFYkgsZ0JBQVk7QUFGQyxJQUFkO0FBSUE7Ozt1Q0FFb0I7QUFDcEJhLFVBQU9lLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLEtBQUtoRCxTQUF2QyxFQUFrRCxLQUFsRDtBQUNBaUQsWUFBU0QsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsS0FBSy9DLGVBQXhDLEVBQXlELEtBQXpEO0FBQ0EsUUFBS0QsU0FBTDtBQUNBOzs7eUNBRXNCO0FBQ3RCaUMsVUFBT2lCLG1CQUFQLENBQTJCLFFBQTNCLEVBQXFDLEtBQUtsRCxTQUExQyxFQUFxRCxLQUFyRDtBQUNBaUQsWUFBU0MsbUJBQVQsQ0FBNkIsT0FBN0IsRUFBc0MsS0FBS2pELGVBQTNDLEVBQTRELEtBQTVEO0FBQ0E7OzsyQkFFUTtBQUNSLFVBQ0M7QUFBQTtBQUFBO0FBQ0Msd0JBQUMsTUFBRCxJQUFXLEtBQU8sSUFBbEIsR0FERDtBQUVDLHdCQUFDLEdBQUQsSUFBUyxLQUFPLElBQWhCLEdBRkQ7QUFHQyx3QkFBQyxTQUFELElBQWMsS0FBTyxJQUFyQixHQUhEO0FBSUMsd0JBQUMsS0FBRCxJQUFXLEtBQU8sSUFBbEIsR0FKRDtBQUtDLHdCQUFDLGVBQUQsSUFBa0IsS0FBTyxJQUF6QixHQUxEO0FBTUMsd0JBQUMsT0FBRCxJQUFZLEtBQU8sSUFBbkIsR0FORDtBQU9DLHdCQUFDLE1BQUQsSUFBVyxLQUFPLElBQWxCO0FBUEQsSUFERDtBQVdBOzs7O0VBOUtnQmtELE1BQU1DLFM7O0FBK0t2Qjs7SUFFS0MsTTs7O0FBQ0wsaUJBQVkxRCxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsK0dBQ1pBLEtBRFk7O0FBRWxCLFNBQUsyRCxNQUFMLEdBQWlCLE9BQUtBLE1BQUwsQ0FBWXpELElBQVosUUFBakI7QUFDQSxTQUFLMEQsS0FBTCxHQUFpQixPQUFLQSxLQUFMLENBQVcxRCxJQUFYLFFBQWpCO0FBQ0EsU0FBSzJELEtBQUwsR0FBaUIsT0FBS0EsS0FBTCxDQUFXM0QsSUFBWCxRQUFqQjtBQUNBLFNBQUs0RCxTQUFMLEdBQW9CLE9BQUtBLFNBQUwsQ0FBZTVELElBQWYsUUFBcEI7QUFDQSxTQUFLNkQsU0FBTCxHQUFvQixPQUFLQSxTQUFMLENBQWU3RCxJQUFmLFFBQXBCO0FBQ0EsU0FBSzhELEtBQUwsR0FBaUIsT0FBS0EsS0FBTCxDQUFXOUQsSUFBWCxRQUFqQjtBQUNBLFNBQUsrRCxLQUFMLEdBQWlCLE9BQUtBLEtBQUwsQ0FBVy9ELElBQVgsUUFBakI7QUFDQSxTQUFLZ0UsZUFBTCxHQUF3QixPQUFLQSxlQUFMLENBQXFCaEUsSUFBckIsUUFBeEI7QUFDQSxTQUFLaUUsWUFBTCxHQUFzQixPQUFLQSxZQUFMLENBQWtCakUsSUFBbEIsUUFBdEI7QUFDQSxTQUFLa0UsS0FBTCxHQUFpQixPQUFLQSxLQUFMLENBQVdsRSxJQUFYLFFBQWpCO0FBQ0EsU0FBS21FLEdBQUwsR0FBZXJFLE1BQU1xRSxHQUFyQjs7QUFFQSxTQUFLdkQsS0FBTCxHQUFhO0FBQ1p3RCxXQUFRLEVBREk7QUFFWkMsZUFBWSxFQUZBO0FBR1pDLHNCQUFtQixFQUhQO0FBSVpDLG1CQUFnQjtBQUpKLEdBQWI7QUFka0I7QUFvQmxCOzs7O3lCQUVNNUMsQyxFQUFHO0FBQ1QsT0FBSTBDLGFBQWEsS0FBS0wsZUFBTCxDQUFxQnJDLEVBQUVvQixNQUFGLENBQVN5QixLQUE5QixDQUFqQjtBQUNBLE9BQUkzRCxZQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQS9CO0FBQ0EsUUFBS3NCLFFBQUwsQ0FBYztBQUNiaUMsWUFBUXpDLEVBQUVvQixNQUFGLENBQVN5QixLQURKO0FBRWJILGdCQUFZQSxVQUZDO0FBR2JDLHVCQUFtQixFQUhOO0FBSWJDLG9CQUFnQjtBQUpILElBQWQ7O0FBT0EsT0FBSTVDLEVBQUVvQixNQUFGLENBQVN5QixLQUFULENBQWV2QyxNQUFmLElBQXlCLENBQTdCLEVBQWdDO0FBQy9CLFNBQUssSUFBSUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJbkIsVUFBVW9CLE1BQTlCLEVBQXNDRCxHQUF0QyxFQUEyQztBQUMxQ25CLGVBQVVtQixDQUFWLEVBQWF5QyxVQUFiLEdBQTBCLEtBQTFCO0FBQ0E1RCxlQUFVbUIsQ0FBVixFQUFhTyxXQUFiLEdBQTJCLEtBQTNCO0FBQ0E7O0FBRUQsU0FBSzRCLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJ0QixnQkFBV0E7QUFETSxLQUFsQjtBQUdBO0FBQ0Q7Ozt3QkFFS2MsQyxFQUFHO0FBQ1IsT0FBSUUsS0FBS0YsRUFBRW9CLE1BQUYsQ0FBU2hCLFlBQVQsQ0FBc0IsU0FBdEIsQ0FBVDtBQUNBLE9BQUlsQixZQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQS9COztBQUVBLFFBQUssSUFBSW1CLElBQUksQ0FBYixFQUFnQkEsSUFBSW5CLFVBQVVvQixNQUE5QixFQUFzQ0QsR0FBdEMsRUFBMkM7QUFDMUMsUUFBSUgsTUFBTWhCLFVBQVVtQixDQUFWLEVBQWFILEVBQXZCLEVBQTJCO0FBQzFCLFVBQUtNLFFBQUwsQ0FBYztBQUNibUMseUJBQW1CekQsVUFBVW1CLENBQVYsRUFBYTBDO0FBRG5CLE1BQWQ7O0FBSUE3RCxlQUFVbUIsQ0FBVixFQUFheUMsVUFBYixHQUEwQixJQUExQjs7QUFFQSxVQUFLTixHQUFMLENBQVNoQyxRQUFULENBQWtCO0FBQ2pCdEIsaUJBQVdBO0FBRE0sTUFBbEI7QUFHQTtBQUNBO0FBQ0Q7QUFDRDs7OzBCQUVPO0FBQ1AsUUFBS3NCLFFBQUwsQ0FBYyxFQUFDbUMsbUJBQW1CLEVBQXBCLEVBQWQ7QUFDQTs7O3dCQUVLM0MsQyxFQUFHO0FBQ1IsT0FBSWlCLE9BQU8sQ0FBQyxLQUFLdUIsR0FBTCxDQUFTdkQsS0FBVCxDQUFlYyxVQUEzQjtBQUNBLFFBQUt5QyxHQUFMLENBQVNoQyxRQUFULENBQWtCLEVBQUNULFlBQVlrQixJQUFiLEVBQWxCO0FBQ0FqQixLQUFFZ0IsY0FBRjs7QUFFQSxPQUFJQyxRQUFRLEtBQUtoQyxLQUFMLENBQVd3RCxNQUF2QixFQUErQjtBQUM5QixTQUFLakMsUUFBTCxDQUFjO0FBQ2JvQyxxQkFBZ0I7QUFESCxLQUFkO0FBR0E7QUFDRDs7OzRCQUVTNUMsQyxFQUFHO0FBQ1osT0FBSUUsS0FBUUYsRUFBRW9CLE1BQUYsQ0FBU2hCLFlBQVQsQ0FBc0IsU0FBdEIsS0FBb0NKLEVBQUVvQixNQUFGLENBQVNHLFVBQVQsQ0FBb0JuQixZQUFwQixDQUFpQyxTQUFqQyxDQUFoRDtBQUNBLE9BQUlsQixZQUFhLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQWhDO0FBQ0EsT0FBSThELHNCQUFKO0FBQ0EsT0FBSUMsdUJBQUo7QUFDQSxPQUFJOUQsT0FBTyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUExQjtBQUNBLE9BQUlFLGFBQUo7QUFDQSxPQUFJQyxZQUFKOztBQUVBLE9BQUksS0FBS2tELEdBQUwsQ0FBU1UsU0FBYixFQUF3QjtBQUN2QkYsb0JBQWdCLEtBQUtSLEdBQUwsQ0FBU1UsU0FBVCxDQUFtQkMsV0FBbkIsR0FBaUMsQ0FBakQ7QUFDQUYscUJBQWlCLEtBQUtULEdBQUwsQ0FBU1UsU0FBVCxDQUFtQkUsWUFBbkIsR0FBa0MsQ0FBbkQ7QUFDQSxJQUhELE1BR1E7QUFDUEosb0JBQWdCLENBQWhCO0FBQ0FDLHFCQUFpQixDQUFqQjtBQUNBOztBQUVELFFBQUssSUFBSTVDLElBQUksQ0FBYixFQUFnQkEsSUFBSW5CLFVBQVVvQixNQUE5QixFQUFzQ0QsR0FBdEMsRUFBMkM7QUFDMUMsUUFBSW5CLFVBQVVtQixDQUFWLEVBQWFILEVBQWIsSUFBbUJBLEVBQXZCLEVBQTJCO0FBQzFCYixZQUFPMkQsZ0JBQWdCN0QsSUFBaEIsR0FBdUJELFVBQVVtQixDQUFWLEVBQWFnRCxRQUFiLENBQXNCaEUsSUFBcEQ7QUFDQUMsV0FBTTJELGlCQUFpQjlELElBQWpCLEdBQXdCRCxVQUFVbUIsQ0FBVixFQUFhZ0QsUUFBYixDQUFzQi9ELEdBQXBEOztBQUVBLFNBQUk0QixVQUFKLEVBQWdCO0FBQ2YsV0FBSyxJQUFJb0MsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtkLEdBQUwsQ0FBUzdELFVBQVQsQ0FBb0IyQixNQUF4QyxFQUFnRGdELEdBQWhELEVBQXFEO0FBQ3BELFdBQUlwRSxVQUFVbUIsQ0FBVixFQUFhUyxVQUFiLElBQTJCLEtBQUswQixHQUFMLENBQVM3RCxVQUFULENBQW9CMkUsQ0FBcEIsRUFBdUJwRCxFQUF0RCxFQUEwRDs7QUFFekQsYUFBSyxJQUFJcUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtmLEdBQUwsQ0FBUzlELFNBQVQsQ0FBbUI0QixNQUF2QyxFQUErQ2lELEdBQS9DLEVBQW9EO0FBQ25ELGFBQUksS0FBS2YsR0FBTCxDQUFTN0QsVUFBVCxDQUFvQjJFLENBQXBCLEVBQXVCdkMsVUFBdkIsSUFBcUMsS0FBS3lCLEdBQUwsQ0FBUzlELFNBQVQsQ0FBbUI2RSxDQUFuQixFQUFzQnJELEVBQS9ELEVBQW1FO0FBQ2xFLGVBQUtzQyxHQUFMLENBQVMzRCxZQUFULEdBQXdCSyxVQUFVbUIsQ0FBVixDQUF4QjtBQUNBLGVBQUttQyxHQUFMLENBQVNoQyxRQUFULENBQWtCLEVBQUNkLGlCQUFpQixLQUFLOEMsR0FBTCxDQUFTOUQsU0FBVCxDQUFtQjZFLENBQW5CLEVBQXNCckQsRUFBeEMsRUFBbEI7QUFDQTtBQUNEOztBQUVEO0FBQ0E7QUFDRDtBQUNELE1BZEQsTUFjTztBQUNOLFVBQUksS0FBS3NDLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVksSUFBZixJQUF1QixNQUEzQixFQUFtQztBQUNsQyxZQUFLMkMsR0FBTCxDQUFTcEUsU0FBVCxDQUFtQixLQUFuQixFQUEwQmMsVUFBVW1CLENBQVYsRUFBYUgsRUFBdkM7QUFDQSxPQUZELE1BRU87O0FBRU4sWUFBS3NDLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJuQixjQUFNQSxJQURXO0FBRWpCQyxhQUFNQSxHQUZXO0FBR2pCSCxjQUFNQTtBQUhXLFFBQWxCO0FBS0E7QUFDRDs7QUFFREQsZUFBVW1CLENBQVYsRUFBYXlDLFVBQWIsR0FBMkIsSUFBM0I7QUFDQTVELGVBQVVtQixDQUFWLEVBQWFPLFdBQWIsR0FBMkIsSUFBM0I7QUFDQSxLQWpDRCxNQWlDTztBQUNOMUIsZUFBVW1CLENBQVYsRUFBYXlDLFVBQWIsR0FBMkIsS0FBM0I7QUFDQTVELGVBQVVtQixDQUFWLEVBQWFPLFdBQWIsR0FBMkIsS0FBM0I7QUFDQTtBQUNEOztBQUVELFFBQUs0QixHQUFMLENBQVN2RCxLQUFULENBQWV1RSxjQUFmLEdBQWdDdEQsRUFBaEM7O0FBRUEsUUFBS3NDLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJ0QixlQUFXQTtBQURNLElBQWxCOztBQUlBLFFBQUtzQixRQUFMLENBQWM7QUFDYm9DLG9CQUFnQjtBQURILElBQWQ7QUFHQTs7OzRCQUVTNUMsQyxFQUFHO0FBQ1osT0FBSUUsS0FBS0YsRUFBRW9CLE1BQUYsQ0FBU2hCLFlBQVQsQ0FBc0IsU0FBdEIsQ0FBVDtBQUNBLE9BQUlsQixZQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQS9COztBQUVBLFFBQUssSUFBSW1CLElBQUksQ0FBYixFQUFnQkEsSUFBSW5CLFVBQVVvQixNQUE5QixFQUFzQ0QsR0FBdEMsRUFBMkM7QUFDMUMsUUFBSW5CLFVBQVVtQixDQUFWLEVBQWFILEVBQWIsSUFBbUJBLEVBQXZCLEVBQTJCO0FBQzFCaEIsZUFBVW1CLENBQVYsRUFBYXlDLFVBQWIsR0FBMEIsS0FBMUI7O0FBRUEsVUFBS04sR0FBTCxDQUFTaEMsUUFBVCxDQUFrQjtBQUNqQnRCLGlCQUFXQTtBQURNLE1BQWxCOztBQUlBO0FBQ0E7QUFDRDtBQUNEOzs7a0NBRWUyRCxLLEVBQU87QUFDdEIsT0FBSVksT0FBUyxFQUFiO0FBQ0EsT0FBSXBELElBQU8sQ0FBWDtBQUNBLE9BQUlxRCxRQUFVLENBQWQ7QUFDQSxPQUFJeEUsWUFBYSxLQUFLc0QsR0FBTCxDQUFTdkQsS0FBVCxDQUFlQyxTQUFoQztBQUNBLE9BQUlQLGFBQWMsS0FBSzZELEdBQUwsQ0FBUzdELFVBQTNCOztBQUVBLFVBQU9rRSxNQUFNdkMsTUFBTixJQUFnQkQsSUFBSW5CLFVBQVVvQixNQUE5QixJQUF3Q29ELFFBQVEsRUFBdkQsRUFBMkQ7QUFDMUQsUUFBSXhFLFVBQVVtQixDQUFWLEVBQWEwQyxJQUFiLENBQWtCWSxXQUFsQixHQUFnQ3JDLE9BQWhDLENBQXdDdUIsTUFBTWMsV0FBTixFQUF4QyxLQUFnRSxDQUFwRSxFQUF1RTtBQUN0RUYsVUFBS0csSUFBTCxDQUFVMUUsVUFBVW1CLENBQVYsQ0FBVjtBQUNBcUQ7QUFDQTtBQUNEckQ7QUFDQTs7QUFFRCxRQUFLLElBQUlBLEtBQUksQ0FBYixFQUFnQkEsS0FBSW9ELEtBQUtuRCxNQUF6QixFQUFpQ0QsSUFBakMsRUFBc0M7QUFDckMsU0FBSyxJQUFJaUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJM0UsV0FBVzJCLE1BQS9CLEVBQXVDZ0QsR0FBdkMsRUFBNEM7QUFDM0MsU0FBSUcsS0FBS3BELEVBQUwsRUFBUVMsVUFBUixJQUFzQm5DLFdBQVcyRSxDQUFYLEVBQWNwRCxFQUF4QyxFQUE0QztBQUMzQ3VELFdBQUtwRCxFQUFMLEVBQVF3RCxZQUFSLEdBQXVCbEYsV0FBVzJFLENBQVgsRUFBY1EsTUFBckM7QUFDQTtBQUNBO0FBQ0Q7QUFDRDs7QUFFRCxVQUFPTCxJQUFQO0FBQ0E7OzsrQkFFWXpELEMsRUFBRztBQUNmLE9BQUlFLEtBQUtGLEVBQUVHLGFBQUYsQ0FBZ0JDLFlBQWhCLENBQTZCLFNBQTdCLENBQVQ7QUFDQSxPQUFJUCxPQUFPLEtBQUsyQyxHQUFMLENBQVN2RCxLQUFULENBQWVZLElBQTFCO0FBQ0EsT0FBSVYsT0FBTyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUExQjtBQUNBLE9BQUkrRCxZQUFZLEtBQUtWLEdBQUwsQ0FBU1UsU0FBekI7QUFDQSxPQUFJYSxXQUFXLEtBQUt2QixHQUFMLENBQVM5RCxTQUF4QjtBQUNBLE9BQUlDLGFBQWEsS0FBSzZELEdBQUwsQ0FBUzdELFVBQTFCO0FBQ0EsT0FBSU8sWUFBWSxLQUFLc0QsR0FBTCxDQUFTdkQsS0FBVCxDQUFlQyxTQUEvQjtBQUNBLE9BQUk4RCxzQkFBSjtBQUNBLE9BQUlDLHVCQUFKO0FBQ0EsT0FBSTVELGFBQUo7QUFDQSxPQUFJQyxZQUFKO0FBQ0EsT0FBSTBFLGtCQUFKO0FBQ0EsT0FBSUMsa0JBQUo7QUFDQSxPQUFJQyxnQkFBSjtBQUNBLE9BQUlDLHFCQUFKO0FBQ0EsT0FBSUMsT0FBTzNDLFNBQVM0QyxnQkFBVCxDQUEwQiwyQ0FBMUIsQ0FBWDs7QUFFQXJFLEtBQUVnQixjQUFGOztBQUVBLE9BQUluQixRQUFRLE1BQVosRUFBb0I7QUFDbkJtRSxnQkFBWXZDLFNBQVM2QyxjQUFULENBQXdCLGNBQWNwRSxFQUF0QyxDQUFaO0FBQ0FnRSxjQUFVekMsU0FBUzhDLGFBQVQsQ0FBdUIsU0FBdkIsQ0FBVjtBQUNBTixnQkFBWUQsVUFBVUMsU0FBdEI7QUFDQUUsbUJBQWVELFFBQVFkLFlBQXZCO0FBQ0FhLGlCQUFhRSxZQUFiOztBQUVBMUQsV0FBTytELE1BQVAsQ0FBYztBQUNibEYsVUFBSzJFLFNBRFE7QUFFYjVFLFdBQU0sQ0FGTztBQUdib0YsZUFBVTtBQUhHLEtBQWQ7QUFLQSxJQVpELE1BWU87QUFDTnpCLG9CQUFnQkUsVUFBVUMsV0FBVixHQUF3QixDQUF4QztBQUNBRixxQkFBaUJDLFVBQVVFLFlBQVYsR0FBeUIsQ0FBMUM7O0FBRUEsU0FBSyxJQUFJL0MsSUFBSSxDQUFiLEVBQWdCQSxJQUFJM0IsVUFBVTRCLE1BQTlCLEVBQXNDRCxHQUF0QyxFQUEyQztBQUMxQyxTQUFJM0IsVUFBVTJCLENBQVYsRUFBYUgsRUFBYixJQUFtQkEsRUFBdkIsRUFBMkI7QUFDMUJiLGFBQU9YLFVBQVUyQixDQUFWLEVBQWFnRCxRQUFiLENBQXNCaEUsSUFBdEIsR0FBNkIsQ0FBQyxDQUFyQztBQUNBQyxZQUFNWixVQUFVMkIsQ0FBVixFQUFhZ0QsUUFBYixDQUFzQi9ELEdBQXRCLEdBQTRCLENBQUMsQ0FBbkM7QUFDQTtBQUNBO0FBQ0Q7O0FBRUQsU0FBSyxJQUFJZSxNQUFJLENBQWIsRUFBZ0JBLE1BQUkxQixXQUFXMkIsTUFBL0IsRUFBdUNELEtBQXZDLEVBQTRDO0FBQzNDLFNBQUkxQixXQUFXMEIsR0FBWCxFQUFjVSxVQUFkLElBQTRCYixFQUFoQyxFQUFvQztBQUNuQyxXQUFLLElBQUlvRCxJQUFJLENBQWIsRUFBZ0JBLElBQUlwRSxVQUFVb0IsTUFBOUIsRUFBc0NnRCxHQUF0QyxFQUEyQztBQUMxQyxXQUFJcEUsVUFBVW9FLENBQVYsRUFBYXhDLFVBQWIsSUFBMkJuQyxXQUFXMEIsR0FBWCxFQUFjSCxFQUE3QyxFQUFpRDtBQUNoRGhCLGtCQUFVb0UsQ0FBVixFQUFhUixVQUFiLEdBQTBCLEtBQTFCO0FBQ0E1RCxrQkFBVW9FLENBQVYsRUFBYTFDLFdBQWIsR0FBMkIsSUFBM0I7QUFDQTtBQUNEO0FBQ0QsTUFQRCxNQU9PO0FBQ04sV0FBSyxJQUFJMEMsS0FBSSxDQUFiLEVBQWdCQSxLQUFJcEUsVUFBVW9CLE1BQTlCLEVBQXNDZ0QsSUFBdEMsRUFBMkM7QUFDMUMsV0FBSXBFLFVBQVVvRSxFQUFWLEVBQWF4QyxVQUFiLElBQTJCbkMsV0FBVzBCLEdBQVgsRUFBY0gsRUFBN0MsRUFBaUQ7QUFDaERoQixrQkFBVW9FLEVBQVYsRUFBYVIsVUFBYixHQUEwQixLQUExQjtBQUNBNUQsa0JBQVVvRSxFQUFWLEVBQWExQyxXQUFiLEdBQTJCLEtBQTNCO0FBQ0E7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQsU0FBSzRCLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJsQixVQUFLQSxHQURZO0FBRWpCRCxXQUFNQSxJQUZXO0FBR2pCSCxnQkFBV0EsU0FITTtBQUlqQlEsc0JBQWlCUTtBQUpBLEtBQWxCO0FBTUE7O0FBRUQsUUFBSyxJQUFJRyxNQUFJLENBQWIsRUFBZ0JBLE1BQUkrRCxLQUFLOUQsTUFBekIsRUFBaUNELEtBQWpDLEVBQXNDO0FBQ3JDK0QsU0FBSy9ELEdBQUwsRUFBUXFFLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLHVCQUF6QjtBQUNBOztBQUVEM0UsS0FBRW9CLE1BQUYsQ0FBU3NELFNBQVQsQ0FBbUJFLEdBQW5CLENBQXVCLHVCQUF2QjtBQUNBOzs7MEJBRU87QUFDUCxRQUFLcEUsUUFBTCxDQUFjO0FBQ2JvQyxvQkFBZ0I7QUFESCxJQUFkO0FBR0E7OzswQkFFTztBQUNQLE9BQUkxRCxZQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQS9COztBQUVBLFFBQUssSUFBSW1CLElBQUksQ0FBYixFQUFnQkEsSUFBSW5CLFVBQVVvQixNQUE5QixFQUFzQ0QsR0FBdEMsRUFBMkM7QUFDMUNuQixjQUFVbUIsQ0FBVixFQUFheUMsVUFBYixHQUEwQixLQUExQjtBQUNBNUQsY0FBVW1CLENBQVYsRUFBYU8sV0FBYixHQUEyQixLQUEzQjtBQUNBOztBQUVELFFBQUtKLFFBQUwsQ0FBYztBQUNiaUMsWUFBUSxFQURLO0FBRWJHLG9CQUFnQixLQUZIO0FBR2JGLGdCQUFZO0FBSEMsSUFBZDs7QUFNQSxRQUFLRixHQUFMLENBQVNoQyxRQUFULENBQWtCO0FBQ2pCdEIsZUFBV0E7QUFETSxJQUFsQjtBQUdBOzs7MkJBRVE7QUFBQTs7QUFDUixPQUFJUixZQUFZLEtBQUs4RCxHQUFMLENBQVM5RCxTQUF6QjtBQUNBQSxlQUFZQSxVQUFVbUcsR0FBVixDQUFjLG9CQUFZO0FBQ3JDLFdBQ0M7QUFBQTtBQUFBLE9BQUcsTUFBTyxHQUFWLEVBQWMsV0FBWSxlQUExQixFQUEwQyxXQUFXZCxTQUFTN0QsRUFBOUQsRUFBa0UsU0FBVyxPQUFLb0MsWUFBbEY7QUFDRXlCLGNBQVNoQjtBQURYLEtBREQ7QUFLQSxJQU5XLENBQVo7O0FBUUEsVUFDQztBQUFBO0FBQUEsTUFBUSxXQUFZLFFBQXBCO0FBQ0M7QUFBQTtBQUFBLE9BQUcsTUFBSyxHQUFSLEVBQVksV0FBWSxjQUF4QjtBQUFBO0FBQUEsS0FERDtBQUVDO0FBQUE7QUFBQSxPQUFLLFdBQVksYUFBakI7QUFDRXJFLGNBREY7QUFFQztBQUFBO0FBQUEsUUFBRyxNQUFLLEdBQVIsRUFBWSxXQUFZLG9DQUF4QixFQUE2RCxTQUFXLEtBQUt5RCxLQUE3RTtBQUFBO0FBQUEsTUFGRDtBQUdDLHlCQUFDLE1BQUQ7QUFDQyxZQUFRLEtBQUtLLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZWMsVUFEeEI7QUFFQyxnQkFBWSxLQUFLZCxLQUFMLENBQVcyRCxjQUZ4QjtBQUdDLGFBQVMsS0FBSzNELEtBQUwsQ0FBV3dELE1BSHJCO0FBSUMsWUFBUSxLQUFLeEQsS0FBTCxDQUFXeUQsVUFKcEI7QUFLQyxtQkFBZSxLQUFLekQsS0FBTCxDQUFXMEQsaUJBTDNCO0FBTUMsY0FBVSxLQUFLYixNQU5oQjtBQU9DLGFBQVMsS0FBS00sS0FQZjtBQVFDLGFBQVMsS0FBS0wsS0FSZjtBQVNDLGFBQVMsS0FBS0MsS0FUZjtBQVVDLGFBQVMsS0FBS0MsU0FWZjtBQVdDLGlCQUFhLEtBQUtDLFNBWG5CO0FBWUMsYUFBUyxLQUFLSztBQVpmO0FBSEQ7QUFGRCxJQUREO0FBdUJBOzs7O0VBeFVtQlosTUFBTUMsUzs7QUF5VTFCOztBQUVELFNBQVNrRCxNQUFULENBQWdCM0csS0FBaEIsRUFBdUI7QUFDdEIsS0FBSTRHLGNBQWM1RyxNQUFNNEcsV0FBeEI7QUFDQSxLQUFJbEMsUUFBVTFFLE1BQU0wRSxLQUFwQjtBQUNBLEtBQUltQyxRQUFVLEVBQUNDLFNBQVM5RyxNQUFNOEMsSUFBTixHQUFhLE9BQWIsR0FBdUIsTUFBakMsRUFBZDtBQUNBLEtBQUl5QyxRQUFVdkYsTUFBTXNGLElBQU4sQ0FBV25ELE1BQXpCO0FBQ0EsS0FBSTRFLGNBQWMsY0FBbEI7QUFDQSxLQUFJQyxlQUFlLGVBQW5CO0FBQ0EsS0FBSTFCLE9BQVN0RixNQUFNc0YsSUFBTixDQUFXb0IsR0FBWCxDQUFlLG1CQUFXO0FBQ3RDLE1BQUk5QixPQUFROUMsUUFBUThDLElBQVIsQ0FBYXFDLE1BQWIsQ0FBb0J2QyxNQUFNdkMsTUFBMUIsQ0FBWjs7QUFFQSxTQUNDO0FBQUE7QUFBQTtBQUNDLGVBQVksbUJBRGI7QUFFQyxlQUFXTCxRQUFRQyxFQUZwQjtBQUdDLGtCQUFnQi9CLE1BQU1rSCxLQUh2QjtBQUlDLGFBQVdsSCxNQUFNbUgsS0FKbEI7QUFLQyxrQkFBZ0JuSCxNQUFNb0g7QUFMdkI7QUFPQztBQUFBO0FBQUEsTUFBTSxXQUFZLDJCQUFsQjtBQUNDO0FBQUE7QUFBQTtBQUFJMUM7QUFBSixLQUREO0FBQ2dCRTtBQURoQixJQVBEO0FBVUM7QUFBQTtBQUFBLE1BQU0sV0FBWSw0QkFBbEI7QUFDRTlDLFlBQVE0RDtBQURWO0FBVkQsR0FERDtBQWdCQSxFQW5CWSxDQUFiOztBQXFCQSxLQUFJaEIsTUFBTXZDLE1BQVYsRUFBa0I7QUFDakIsVUFBUW9ELFFBQVEsRUFBaEI7QUFDQyxRQUFLLENBQUw7QUFDQSxRQUFLLENBQUw7QUFDQSxRQUFLLENBQUw7QUFDQSxRQUFLLENBQUw7QUFDQSxRQUFLLENBQUw7QUFDQSxRQUFLLENBQUw7QUFDQ0EsWUFBUUEsUUFBUSxxQkFBaEI7QUFDRDtBQUNBLFFBQUssQ0FBTDtBQUNBLFFBQUssQ0FBTDtBQUNBLFFBQUssQ0FBTDtBQUNDQSxZQUFRQSxRQUFRLG9CQUFoQjtBQUNEO0FBQ0EsUUFBSyxDQUFMO0FBQ0NBLFlBQVFBLFFBQVEsbUJBQWhCO0FBQ0Q7QUFoQkQ7QUFrQkEsRUFuQkQsTUFtQk87QUFDTkEsVUFBUSxFQUFSO0FBQ0E7O0FBRUQsS0FBSXZGLE1BQU1xSCxRQUFWLEVBQW9CO0FBQ25CTixpQkFBZSxvQkFBZjtBQUNBOztBQUVELEtBQUlyQyxNQUFNdkMsTUFBVixFQUFrQjtBQUNqQjZFLGtCQUFnQixxQkFBaEI7QUFDQTs7QUFFRCxRQUNDO0FBQUE7QUFBQSxJQUFLLFdBQVksUUFBakIsRUFBMEIsT0FBU0gsS0FBbkM7QUFDQztBQUFBO0FBQUEsS0FBSyxXQUFZLHFCQUFqQjtBQUNFRDtBQURGLEdBREQ7QUFJQztBQUFBO0FBQUEsS0FBSyxXQUFZLGVBQWpCO0FBQ0VyQjtBQURGLEdBSkQ7QUFPQztBQUNDLGNBQWEsZUFEZDtBQUVDLFNBQVMsTUFGVjtBQUdDLFVBQVdiLEtBSFo7QUFJQyxhQUFhMUUsTUFBTXNILE1BSnBCO0FBS0MsWUFBWXRILE1BQU1pRSxLQUxuQjtBQU1DLGdCQUFjO0FBTmYsSUFQRDtBQWVDO0FBQUE7QUFBQSxLQUFLLFdBQWE4QyxXQUFsQixFQUErQixjQUFnQi9HLE1BQU11SCxLQUFyRDtBQUE4RGpDO0FBQTlELEdBZkQ7QUFnQkMsa0NBQVEsV0FBYTBCLFlBQXJCLEVBQW1DLFNBQVdoSCxNQUFNb0UsS0FBcEQ7QUFoQkQsRUFERDtBQW9CQTs7SUFFS29ELEc7OztBQUNMLGNBQVl4SCxLQUFaLEVBQW1CO0FBQUE7O0FBQUEseUdBQ1pBLEtBRFk7O0FBRWxCLFNBQUtxRSxHQUFMLEdBQWNyRSxNQUFNcUUsR0FBcEI7QUFDQSxTQUFLb0QsS0FBTCxHQUFnQixPQUFLQSxLQUFMLENBQVd2SCxJQUFYLFFBQWhCO0FBQ0EsU0FBS3dILFNBQUwsR0FBbUIsT0FBS0EsU0FBTCxDQUFleEgsSUFBZixRQUFuQjtBQUNBLFNBQUt5SCxPQUFMLEdBQWlCLE9BQUtBLE9BQUwsQ0FBYXpILElBQWIsUUFBakI7QUFDQSxTQUFLMEgsU0FBTCxHQUFtQixPQUFLQSxTQUFMLENBQWUxSCxJQUFmLFFBQW5CO0FBQ0EsU0FBSzJILFFBQUwsR0FBa0IsT0FBS0EsUUFBTCxDQUFjM0gsSUFBZCxRQUFsQjtBQUNBLFNBQUs0SCxRQUFMLEdBQWtCLE9BQUtBLFFBQUwsQ0FBYzVILElBQWQsUUFBbEI7QUFDQSxTQUFLNkgsU0FBTCxHQUFtQixPQUFLQSxTQUFMLENBQWU3SCxJQUFmLFFBQW5CO0FBQ0EsU0FBSzhILFNBQUwsR0FBbUIsT0FBS0EsU0FBTCxDQUFlOUgsSUFBZixRQUFuQjtBQUNBLFNBQUsrSCxVQUFMLEdBQW1CLE9BQUtBLFVBQUwsQ0FBZ0IvSCxJQUFoQixRQUFuQjtBQUNBLFNBQUtnSSxRQUFMLEdBQWtCLE9BQUtBLFFBQUwsQ0FBY2hJLElBQWQsUUFBbEI7QUFDQSxTQUFLaUksU0FBTCxHQUFtQixPQUFLQSxTQUFMLENBQWVqSSxJQUFmLFFBQW5CO0FBQ0EsU0FBS2tJLFNBQUwsR0FBbUIsS0FBbkI7QUFDQSxTQUFLQyxVQUFMLEdBQW1CLENBQW5CO0FBQ0EsU0FBS3ZDLFNBQUwsR0FBbUIsQ0FBbkI7QUFDQSxTQUFLd0MsUUFBTCxHQUFrQixJQUFsQjtBQWpCa0I7QUFrQmxCOzs7O3dCQUVLekcsQyxFQUFHO0FBQ1IsT0FBSWIsT0FBUyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUE1QjtBQUNBLE9BQUlzSCxXQUFZLEtBQUtBLFFBQXJCO0FBQ0EsT0FBSXJILGdCQUFKOztBQUVBLE9BQUlZLEVBQUUwRyxNQUFGLEdBQVcsQ0FBZixFQUFrQjtBQUNqQnRILGNBQVVELE9BQU9zSCxRQUFqQjtBQUNBLElBRkQsTUFFTztBQUNOckgsY0FBVUQsT0FBT3NILFFBQWpCO0FBQ0E7O0FBRURySCxhQUFVQSxRQUFRdUgsT0FBUixDQUFnQixDQUFoQixJQUFxQixDQUEvQjs7QUFFQSxRQUFLbkUsR0FBTCxDQUFTaEMsUUFBVCxDQUFrQjtBQUNqQnBCLGFBQVNBO0FBRFEsSUFBbEI7O0FBSUFZLEtBQUVnQixjQUFGO0FBQ0E7Ozs0QkFFU2hCLEMsRUFBRztBQUNaLFFBQUt1RyxTQUFMLEdBQWtCLElBQWxCO0FBQ0EsUUFBS0MsVUFBTCxHQUFrQnhHLEVBQUU0RyxLQUFGLEdBQVUsS0FBS3BFLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUksSUFBZixHQUFzQixLQUFLbUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUFqRTtBQUNBLFFBQUs4RSxTQUFMLEdBQWtCakUsRUFBRTZHLEtBQUYsR0FBVSxLQUFLckUsR0FBTCxDQUFTdkQsS0FBVCxDQUFlSyxHQUFmLEdBQXNCLEtBQUtrRCxHQUFMLENBQVN2RCxLQUFULENBQWVFLElBQWpFOztBQUVBYSxLQUFFZ0IsY0FBRjtBQUNBOzs7MEJBRU9oQixDLEVBQUc7QUFDVixRQUFLdUcsU0FBTCxHQUFpQixLQUFqQjtBQUNBOzs7NEJBRVN2RyxDLEVBQUc7QUFDWixPQUFJVixZQUFKO0FBQ0EsT0FBSUQsYUFBSjs7QUFFQSxPQUFJLEtBQUtrSCxTQUFULEVBQW9CO0FBQ25CakgsVUFBUSxDQUFDVSxFQUFFNkcsS0FBRixHQUFVLEtBQUs1QyxTQUFoQixJQUE4QixLQUFLekIsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUFyRDtBQUNBRSxXQUFRLENBQUNXLEVBQUU0RyxLQUFGLEdBQVUsS0FBS0osVUFBaEIsSUFBOEIsS0FBS2hFLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUUsSUFBckQ7O0FBRUEsU0FBS3FELEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJsQixVQUFNQSxHQURXO0FBRWpCRCxXQUFNQTtBQUZXLEtBQWxCO0FBSUE7QUFDRDs7OzJCQUVRVyxDLEVBQUc7QUFDWCxRQUFLdUcsU0FBTCxHQUFpQixLQUFqQjtBQUNBOzs7MkJBRVF2RyxDLEVBQUc7QUFDWCxPQUFJOEcsUUFBUTlHLEVBQUVvQixNQUFGLENBQVNoQixZQUFULENBQXNCLGtCQUF0QixLQUE2Q0osRUFBRW9CLE1BQUYsQ0FBU0csVUFBVCxDQUFvQm5CLFlBQXBCLENBQWlDLGtCQUFqQyxDQUF6RDtBQUNBLE9BQUlsQixrQkFBSjtBQUNBLE9BQUlTLGFBQWEsS0FBSzZDLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVUsVUFBaEM7O0FBRUEsT0FBSSxDQUFDbUgsS0FBTCxFQUFZO0FBQ1gsV0FBTyxLQUFQO0FBQ0E7O0FBR0Q1SCxlQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQWYsQ0FBeUIyRixHQUF6QixDQUE2QixtQkFBVztBQUNuRCxRQUFJNUUsUUFBUWEsVUFBUixJQUFzQmdHLEtBQXRCLElBQStCQSxTQUFTbkgsVUFBNUMsRUFBd0Q7QUFDdkRNLGFBQVFXLFdBQVIsR0FBc0IsS0FBdEI7QUFDQVgsYUFBUTZDLFVBQVIsR0FBcUIsS0FBckI7QUFDQSxLQUhELE1BR087QUFDTjdDLGFBQVFXLFdBQVIsR0FBc0IsSUFBdEI7QUFDQTs7QUFFRCxXQUFPWCxPQUFQO0FBQ0EsSUFUVyxDQUFaOztBQVdBLE9BQUk2RyxTQUFTbkgsVUFBYixFQUF5QjtBQUN4Qm1ILFlBQVEsQ0FBUjtBQUNBOztBQUVELFFBQUt0RSxHQUFMLENBQVNoQyxRQUFULENBQWtCO0FBQ2pCdEIsZUFBV0EsU0FETTtBQUVqQlMsZ0JBQVltSDtBQUZLLElBQWxCO0FBSUE7Ozs0QkFFUzlHLEMsRUFBRztBQUNaLE9BQUlFLEtBQUtGLEVBQUVvQixNQUFGLENBQVNoQixZQUFULENBQXNCLFNBQXRCLENBQVQ7QUFDQSxPQUFJVixrQkFBa0IsS0FBSzhDLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVMsZUFBckM7QUFDQSxPQUFJZixhQUFhLEtBQUs2RCxHQUFMLENBQVM3RCxVQUExQjtBQUNBLE9BQUlPLFlBQVksS0FBS3NELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUMsU0FBL0I7O0FBRUEsUUFBSyxJQUFJbUIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJMUIsV0FBVzJCLE1BQS9CLEVBQXVDRCxHQUF2QyxFQUE0QztBQUMzQyxRQUFJMUIsV0FBVzBCLENBQVgsRUFBY1UsVUFBZCxJQUE0QmIsRUFBaEMsRUFBb0M7QUFDbkMsVUFBSyxJQUFJb0QsSUFBSSxDQUFiLEVBQWdCQSxJQUFJcEUsVUFBVW9CLE1BQTlCLEVBQXNDZ0QsR0FBdEMsRUFBMkM7QUFDMUMsVUFBSXBFLFVBQVVvRSxDQUFWLEVBQWF4QyxVQUFiLElBQTJCbkMsV0FBVzBCLENBQVgsRUFBY0gsRUFBN0MsRUFBaUQ7QUFDaERoQixpQkFBVW9FLENBQVYsRUFBYVIsVUFBYixHQUEwQixLQUExQjtBQUNBNUQsaUJBQVVvRSxDQUFWLEVBQWExQyxXQUFiLEdBQTJCLElBQTNCO0FBQ0E7QUFDRDtBQUNELEtBUEQsTUFPTztBQUNOLFVBQUssSUFBSTBDLE1BQUksQ0FBYixFQUFnQkEsTUFBSXBFLFVBQVVvQixNQUE5QixFQUFzQ2dELEtBQXRDLEVBQTJDO0FBQzFDLFVBQUlwRSxVQUFVb0UsR0FBVixFQUFheEMsVUFBYixJQUEyQm5DLFdBQVcwQixDQUFYLEVBQWNILEVBQTdDLEVBQWlEO0FBQ2hEaEIsaUJBQVVvRSxHQUFWLEVBQWFSLFVBQWIsR0FBMEIsS0FBMUI7QUFDQTVELGlCQUFVb0UsR0FBVixFQUFhMUMsV0FBYixHQUEyQixLQUEzQjtBQUNBO0FBQ0Q7QUFDRDtBQUNEOztBQUVELE9BQUlsQixtQkFBbUJRLEVBQXZCLEVBQTJCO0FBQzFCLFNBQUssSUFBSW9ELE1BQUksQ0FBYixFQUFnQkEsTUFBSXBFLFVBQVVvQixNQUE5QixFQUFzQ2dELEtBQXRDLEVBQTJDO0FBQzFDcEUsZUFBVW9FLEdBQVYsRUFBYVIsVUFBYixHQUEwQixLQUExQjtBQUNBNUQsZUFBVW9FLEdBQVYsRUFBYTFDLFdBQWIsR0FBMkIsS0FBM0I7QUFDQTs7QUFFRFYsU0FBSyxDQUFMO0FBQ0E7O0FBRUQsUUFBS3NDLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJkLHFCQUFpQlE7QUFEQSxJQUFsQjtBQUdBOzs7MkJBRVFaLEcsRUFBSztBQUNiLE9BQUl5SCxTQUFTLENBQWI7QUFDQSxPQUFJQyxlQUFKOztBQUVBQSxZQUFVLEtBQUtkLFNBQUwsRUFBVjs7QUFFQSxPQUFJNUcsTUFBTXlILE1BQVYsRUFBa0J6SCxNQUFPeUgsTUFBUDtBQUNsQixPQUFJekgsTUFBTTBILE1BQVYsRUFBa0IxSCxNQUFPMEgsTUFBUDs7QUFFbEIsVUFBTzFILEdBQVA7QUFDQTs7OzRCQUVTSCxJLEVBQU07QUFDZixPQUFJNkgsU0FBUyxDQUFiOztBQUVBLE9BQUk3SCxTQUFTOEgsU0FBYixFQUF3QjtBQUN2QjlILFdBQU8sS0FBS3FELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUUsSUFBdEI7QUFDQTs7QUFFRDZILFlBQVMsS0FBSzlELFNBQUwsQ0FBZUUsWUFBZixHQUE4QmpFLElBQTlCLEdBQXFDLEtBQUswRixHQUFMLENBQVN6QixZQUF2RDs7QUFFQSxVQUFPNEQsTUFBUDtBQUNBOzs7NEJBRVMzSCxJLEVBQU07QUFDZixPQUFJNkgsVUFBVSxDQUFkO0FBQ0EsT0FBSUMsZ0JBQUo7QUFDQSxPQUFJaEksT0FBTyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUExQjs7QUFFQWdJLGFBQVUsS0FBS2YsVUFBTCxFQUFWOztBQUVBLE9BQUkvRyxPQUFPNkgsT0FBWCxFQUFvQjdILE9BQU82SCxPQUFQO0FBQ3BCLE9BQUk3SCxPQUFPOEgsT0FBWCxFQUFvQjlILE9BQU84SCxPQUFQOztBQUVwQixVQUFPOUgsSUFBUDtBQUNBOzs7NkJBRVVGLEksRUFBTTtBQUNoQixPQUFJZ0ksVUFBVSxDQUFkOztBQUVBLE9BQUloSSxTQUFTOEgsU0FBYixFQUF3QjtBQUN2QjlILFdBQU8sS0FBS3FELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUUsSUFBdEI7QUFDQTs7QUFFRGdJLGFBQVUsS0FBS2pFLFNBQUwsQ0FBZUMsV0FBZixHQUE0QmhFLElBQTVCLEdBQW1DLEtBQUswRixHQUFMLENBQVMxQixXQUF0RDs7QUFFQSxVQUFPZ0UsT0FBUDtBQUNBOzs7MkJBRVE7QUFBQTs7QUFDUixPQUFJaEksT0FBTyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUExQjtBQUNBLE9BQUlDLFVBQVUsS0FBS29ELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUcsT0FBN0I7QUFDQSxPQUFJQyxPQUFPLEtBQUttRCxHQUFMLENBQVN2RCxLQUFULENBQWVJLElBQTFCO0FBQ0EsT0FBSUMsTUFBTyxLQUFLa0QsR0FBTCxDQUFTdkQsS0FBVCxDQUFlSyxHQUExQjtBQUNBLE9BQUk4SCxrQkFBSjtBQUNBLE9BQUlDLG1CQUFKO0FBQ0EsT0FBSUMsaUJBQUo7QUFDQSxPQUFJQyxrQkFBSjtBQUNBLE9BQUlySSxZQUFZLEVBQWhCO0FBQ0EsT0FBSVAsYUFBYSxFQUFqQjtBQUNBLE9BQUk2SSxRQUFRLEVBQVo7QUFDQSxPQUFJQyxpQkFBSjtBQUNBLE9BQUlDLGFBQWEsS0FBakI7O0FBRUEsT0FBSSxDQUFDLEtBQUtsRixHQUFMLENBQVN2RCxLQUFULENBQWVNLE9BQXBCLEVBQTZCO0FBQzVCLFdBQ0MsZ0NBREQ7QUFHQTs7QUFFRCxPQUFJLEtBQUsyRCxTQUFMLElBQWtCLEtBQUsyQixHQUEzQixFQUFnQztBQUMvQnVDLGdCQUFhLEtBQUtsRSxTQUFMLENBQWVDLFdBQTVCO0FBQ0FrRSxpQkFBYyxLQUFLbkUsU0FBTCxDQUFlRSxZQUE3QjtBQUNBa0UsZUFBWSxLQUFLekMsR0FBTCxDQUFTMUIsV0FBckI7QUFDQW9FLGdCQUFhLEtBQUsxQyxHQUFMLENBQVN6QixZQUF0QjtBQUNBOztBQUVELE9BQUloRSxVQUFVRCxJQUFkLEVBQW9CO0FBQ25CLFFBQUlDLFVBQVUsQ0FBZCxFQUFpQjtBQUNoQkEsZUFBVUQsSUFBVjtBQUNBO0FBQ0Q7O0FBRUQsT0FBSUMsVUFBVUQsSUFBZCxFQUFvQjtBQUNuQnNJLGVBQVlMLFlBQWFoSSxPQUFiLEdBQXdCa0ksUUFBekIsSUFBdUNELGFBQWNqSSxPQUFkLEdBQXlCbUksU0FBM0U7O0FBRUEsUUFBSSxDQUFDRSxRQUFMLEVBQWU7QUFDZHJJLGVBQVVELElBQVY7QUFDQTtBQUNEOztBQUVELE9BQUlDLFdBQVcsS0FBS29ELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUUsSUFBOUIsRUFBb0M7QUFDbkNFLFdBQU8rSCxZQUFZLENBQVosR0FBZ0JoSSxPQUFoQixJQUEyQnVJLEtBQUtDLEdBQUwsQ0FBU3ZJLElBQVQsSUFBaUIrSCxZQUFZLENBQVosR0FBZ0JqSSxJQUE1RCxDQUFQO0FBQ0FHLFVBQU0rSCxhQUFhLENBQWIsR0FBaUJqSSxPQUFqQixJQUE0QnVJLEtBQUtDLEdBQUwsQ0FBU3RJLEdBQVQsSUFBZ0IrSCxhQUFhLENBQWIsR0FBaUJsSSxJQUE3RCxDQUFOOztBQUVBLFNBQUtxRCxHQUFMLENBQVNoQyxRQUFULENBQWtCLEVBQUNyQixNQUFNQyxPQUFQLEVBQWxCO0FBQ0E7O0FBRUQsT0FBSSxLQUFLeUYsR0FBVCxFQUFjO0FBQ2J4RixXQUFPLEtBQUs4RyxTQUFMLENBQWU5RyxJQUFmLENBQVA7QUFDQUMsVUFBTyxLQUFLMkcsUUFBTCxDQUFjM0csR0FBZCxDQUFQO0FBQ0FELFdBQU9BLEtBQUtzSCxPQUFMLENBQWEsQ0FBYixJQUFrQixDQUF6QjtBQUNBckgsVUFBTUEsSUFBSXFILE9BQUosQ0FBWSxDQUFaLElBQWlCLENBQXZCOztBQUVBLFNBQUtuRSxHQUFMLENBQVN2RCxLQUFULENBQWVJLElBQWYsR0FBc0JBLElBQXRCO0FBQ0EsU0FBS21ELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZUssR0FBZixHQUFzQkEsR0FBdEI7QUFDQTs7QUFFRCxPQUFJMEYsUUFBUTtBQUNYNkMsZUFBVyxXQUFXMUksSUFBWCxHQUFrQixlQUFsQixHQUFvQ0UsSUFBcEMsR0FBMkMsaUJBQTNDLEdBQStEQyxHQUEvRCxHQUFxRTtBQURyRSxJQUFaOztBQUlBa0ksV0FBUSxLQUFLaEYsR0FBTCxDQUFTOUQsU0FBVCxDQUFtQm1HLEdBQW5CLENBQXVCO0FBQUEsV0FBUSxvQkFBQyxPQUFELElBQVMsTUFBUWlELElBQWpCLEVBQXVCLE9BQVMsT0FBS3hCLFNBQXJDLEdBQVI7QUFBQSxJQUF2QixDQUFSO0FBQ0EzSCxnQkFBYSxLQUFLNkQsR0FBTCxDQUFTN0QsVUFBVCxDQUFvQmtHLEdBQXBCLENBQXdCO0FBQUEsV0FBWSxvQkFBQyxTQUFELElBQVcsTUFBUWtELFFBQW5CLEVBQTZCLE9BQVMsT0FBSzFCLFFBQTNDLEdBQVo7QUFBQSxJQUF4QixDQUFiO0FBQ0FuSCxlQUFZLEtBQUtzRCxHQUFMLENBQVN2RCxLQUFULENBQWVDLFNBQWYsQ0FBeUIyRixHQUF6QixDQUE2QjtBQUFBLFdBQVcsb0JBQUMsR0FBRCxJQUFLLEtBQU8sT0FBS3JDLEdBQWpCLEVBQXNCLE1BQVF2QyxPQUE5QixFQUF1QyxPQUFTLE9BQUt1QyxHQUFMLENBQVNwRSxTQUF6RCxHQUFYO0FBQUEsSUFBN0IsQ0FBWjs7QUFFQSxPQUFJLEtBQUtvRSxHQUFMLENBQVN2RCxLQUFULENBQWVZLElBQWYsSUFBdUIsTUFBM0IsRUFBbUM7QUFDbEMsV0FDQyxnQ0FERDtBQUdBOztBQUVELFVBQ0M7QUFBQTtBQUFBO0FBQ0M7QUFBQTtBQUFBO0FBQ0MsaUJBQWEsZUFEZDtBQUVDLGVBQVksS0FBSytGLEtBRmxCO0FBR0MsbUJBQWUsS0FBS0csU0FIckI7QUFJQyxvQkFBaUIsS0FBS0MsUUFKdkI7QUFLQyxXQUFTO0FBQUEsY0FBYSxPQUFLeEQsR0FBTCxDQUFTVSxTQUFULEdBQXFCLE9BQUtBLFNBQUwsR0FBaUJBLFNBQW5EO0FBQUE7QUFMVjtBQU9DO0FBQUE7QUFBQTtBQUNDLGtCQUFjd0UsVUFEZjtBQUVDLGNBQVcxQyxLQUZaO0FBR0Msb0JBQWUsS0FBS2EsU0FIckI7QUFJQyxrQkFBYyxLQUFLQyxPQUpwQjtBQUtDLFlBQVM7QUFBQSxlQUFPLE9BQUt0RCxHQUFMLENBQVNxQyxHQUFULEdBQWUsT0FBS0EsR0FBTCxHQUFXQSxHQUFqQztBQUFBO0FBTFY7QUFPRTNGLGVBUEY7QUFRRVAsZ0JBUkY7QUFTRTZJO0FBVEYsTUFQRDtBQWtCQyx5QkFBQyxLQUFELElBQVEsS0FBTyxLQUFLaEYsR0FBcEIsRUFBeUIsTUFBTyxLQUFoQztBQWxCRDtBQURELElBREQ7QUF3QkE7Ozs7RUEvUmdCYixNQUFNQyxTOztBQWdTdkI7O0FBRUQsU0FBU29HLEdBQVQsQ0FBYTdKLEtBQWIsRUFBb0I7QUFDbkIsS0FBSVEsYUFBYVIsTUFBTXFFLEdBQU4sQ0FBVTdELFVBQTNCO0FBQ0EsS0FBSXNKLGVBQUo7O0FBRUEsTUFBSyxJQUFJNUgsSUFBSSxDQUFiLEVBQWdCQSxJQUFJMUIsV0FBVzJCLE1BQS9CLEVBQXVDRCxHQUF2QyxFQUE0QztBQUMzQyxNQUFJbEMsTUFBTStKLElBQU4sQ0FBV3BILFVBQVgsSUFBeUJuQyxXQUFXMEIsQ0FBWCxFQUFjSCxFQUEzQyxFQUErQztBQUM5QytILFlBQVN0SixXQUFXMEIsQ0FBWCxFQUFjNEgsTUFBdkI7QUFDQTtBQUNBO0FBQ0Q7O0FBRUQsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFZLEtBQWpCLEVBQXVCLFdBQVc5SixNQUFNK0osSUFBTixDQUFXaEksRUFBN0MsRUFBaUQsU0FBVy9CLE1BQU1tSCxLQUFsRTtBQUNDLHNCQUFDLFNBQUQ7QUFDQyxhQUFhbkgsTUFBTStKLElBQU4sQ0FBVzdFLFFBRHpCO0FBRUMsU0FBVWxGLE1BQU0rSixJQUFOLENBQVdwRjtBQUZ0QixJQUREO0FBS0Msc0JBQUMsUUFBRDtBQUNDLGFBQWMzRSxNQUFNK0osSUFBTixDQUFXN0UsUUFEMUI7QUFFQyxVQUFZbEYsTUFBTStKLElBQU4sQ0FBV0MsS0FGeEI7QUFHQyxpQkFBaUJoSyxNQUFNcUUsR0FBTixDQUFVeEQ7QUFINUIsSUFMRDtBQVVDLHNCQUFDLFVBQUQ7QUFDQyxhQUFhYixNQUFNK0osSUFBTixDQUFXN0UsUUFEekI7QUFFQyxTQUFVbEYsTUFBTStKLElBQU4sQ0FBV3RILFdBRnRCO0FBR0MsZ0JBQWV6QyxNQUFNK0osSUFBTixDQUFXRSxrQkFIM0I7QUFJQyxTQUFVakssTUFBTStKLElBQU4sQ0FBV25GLElBSnRCO0FBS0MsWUFBWTVFLE1BQU0rSixJQUFOLENBQVdHLE9BTHhCO0FBTUMsYUFBYWxLLE1BQU0rSixJQUFOLENBQVczSCxRQU56QjtBQU9DLFdBQVkwSDtBQVBiO0FBVkQsRUFERDtBQXNCQTs7QUFFRCxTQUFTSyxTQUFULENBQW1CbkssS0FBbkIsRUFBMEI7QUFDekIsS0FBSTZHLFFBQVE7QUFDWHVELFdBQVVwSyxNQUFNOEMsSUFBTixHQUFhLENBQWIsR0FBaUIsQ0FEaEI7QUFFWDNCLE9BQU9uQixNQUFNa0YsUUFBTixDQUFlL0QsR0FGWDtBQUdYRCxRQUFRbEIsTUFBTWtGLFFBQU4sQ0FBZWhFO0FBSFosRUFBWjs7QUFNQSxRQUNDLDZCQUFLLFdBQVksYUFBakIsRUFBK0IsT0FBUzJGLEtBQXhDLEdBREQ7QUFHQTs7QUFFRCxTQUFTd0QsUUFBVCxDQUFrQnJLLEtBQWxCLEVBQXlCO0FBQ3hCLEtBQUlzSyxNQUFNdEssTUFBTWEsWUFBTixHQUFxQixPQUEvQjtBQUNBLEtBQUlnRyxRQUFTO0FBQ1oxRixPQUFTbkIsTUFBTWtGLFFBQU4sQ0FBZS9ELEdBRFo7QUFFWkQsUUFBU2xCLE1BQU1rRixRQUFOLENBQWVoRSxJQUZaO0FBR1pxSixtQkFBa0IsU0FBU0QsR0FBVCxHQUFldEssTUFBTWdLLEtBQXJCLEdBQTZCO0FBSG5DLEVBQWI7O0FBTUEsUUFDQyw2QkFBSyxXQUFZLFlBQWpCLEVBQThCLE9BQVNuRCxLQUF2QyxHQUREO0FBR0E7O0FBRUQsU0FBUzJELFVBQVQsQ0FBb0J4SyxLQUFwQixFQUEyQjtBQUMxQixLQUFJeUssVUFBVSxDQUFDLGNBQUQsQ0FBZDtBQUNBLEtBQUlYLFNBQVU5SixNQUFNOEosTUFBcEI7QUFDQSxLQUFJakQsUUFBUztBQUNaMUYsT0FBT25CLE1BQU1rRixRQUFOLENBQWUvRCxHQURWO0FBRVpELFFBQVFsQixNQUFNa0YsUUFBTixDQUFlaEUsSUFGWDtBQUdaa0osV0FBVXBLLE1BQU04QyxJQUFOLEdBQWEsQ0FBYixHQUFpQjtBQUhmLEVBQWI7O0FBTUEsU0FBUTlDLE1BQU0wSyxXQUFkO0FBQ0MsT0FBSyxJQUFMO0FBQ0NELFdBQVFoRixJQUFSLENBQWEsaUJBQWI7QUFDRDtBQUNBLE9BQUssSUFBTDtBQUNDZ0YsV0FBUWhGLElBQVIsQ0FBYSxpQkFBYjtBQUNEO0FBQ0EsT0FBSyxJQUFMO0FBQ0NnRixXQUFRaEYsSUFBUixDQUFhLGlCQUFiO0FBQ0Q7QUFDQSxPQUFLLElBQUw7QUFDQ2dGLFdBQVFoRixJQUFSLENBQWEsaUJBQWI7QUFDRDtBQUNBO0FBQ0NnRixXQUFRaEYsSUFBUixDQUFhLGlCQUFiO0FBZEY7O0FBaUJBLEtBQUlxRSxNQUFKLEVBQVk7QUFDWGpELFFBQU0xRixHQUFOLElBQWEySSxPQUFPM0ksR0FBcEI7QUFDQTBGLFFBQU0zRixJQUFOLElBQWM0SSxPQUFPNUksSUFBckI7QUFDQTs7QUFFRCxLQUFJbEIsTUFBTW9DLFFBQVYsRUFBb0I7QUFDbkJxSSxVQUFRaEYsSUFBUixDQUFhLHVCQUFiOztBQUVBLFNBQ0M7QUFBQTtBQUFBLEtBQUssV0FBWSx3QkFBakIsRUFBMEMsT0FBU29CLEtBQW5EO0FBQ0M7QUFBQTtBQUFBLE1BQUssV0FBYTRELFFBQVFFLElBQVIsQ0FBYSxHQUFiLENBQWxCO0FBQ0M7QUFBQTtBQUFBLE9BQUssV0FBWSxzQkFBakI7QUFBeUMzSyxXQUFNNEU7QUFBL0MsS0FERDtBQUVDLGlDQUFLLFdBQVksdUJBQWpCO0FBRkQ7QUFERCxHQUREO0FBUUEsRUFYRCxNQVdPO0FBQ04sU0FDQztBQUFBO0FBQUEsS0FBSyxXQUFZLHdCQUFqQixFQUEwQyxPQUFTaUMsS0FBbkQ7QUFDQztBQUFBO0FBQUEsTUFBSyxXQUFhNEQsUUFBUUUsSUFBUixDQUFhLEdBQWIsQ0FBbEI7QUFDQztBQUFBO0FBQUEsT0FBSyxXQUFZLHNCQUFqQjtBQUF5QzNLLFdBQU00RTtBQUEvQyxLQUREO0FBRUM7QUFBQTtBQUFBLE9BQUssV0FBWSxzQkFBakI7QUFBeUM1RSxXQUFNa0ssT0FBL0M7QUFBQTtBQUFBO0FBRkQ7QUFERCxHQUREO0FBUUE7QUFFRDs7QUFFRCxTQUFTVSxTQUFULENBQW1CNUssS0FBbkIsRUFBMEI7QUFDekIsS0FBSTZHLFFBQVE7QUFDWDNGLFFBQU1sQixNQUFNK0osSUFBTixDQUFXYyxjQUFYLENBQTBCM0osSUFEckI7QUFFWEMsT0FBTW5CLE1BQU0rSixJQUFOLENBQVdjLGNBQVgsQ0FBMEIxSixHQUZyQjtBQUdYMkosU0FBTzlLLE1BQU0rSixJQUFOLENBQVdnQixVQUFYLENBQXNCRCxLQUhsQjtBQUlYRSxVQUFRaEwsTUFBTStKLElBQU4sQ0FBV2dCLFVBQVgsQ0FBc0JDO0FBSm5CLEVBQVo7O0FBT0EsUUFDQyxnQ0FBUSxXQUFZLFVBQXBCLEVBQStCLG9CQUFvQmhMLE1BQU0rSixJQUFOLENBQVdoSSxFQUE5RCxFQUFrRSxTQUFXL0IsTUFBTW1ILEtBQW5GLEVBQTBGLE9BQVNOLEtBQW5HLEdBREQ7QUFHQTs7QUFFRCxTQUFTb0UsT0FBVCxDQUFpQmpMLEtBQWpCLEVBQXdCO0FBQ3ZCLEtBQUk2RyxRQUFRO0FBQ1gxRixPQUFLbkIsTUFBTStKLElBQU4sQ0FBV21CLE1BQVgsQ0FBa0IvSixHQURaO0FBRVhELFFBQU1sQixNQUFNK0osSUFBTixDQUFXbUIsTUFBWCxDQUFrQmhLLElBRmI7QUFHWDRKLFNBQU85SyxNQUFNK0osSUFBTixDQUFXbUIsTUFBWCxDQUFrQkosS0FIZDtBQUlYRSxVQUFRaEwsTUFBTStKLElBQU4sQ0FBV21CLE1BQVgsQ0FBa0JGO0FBSmYsRUFBWjs7QUFPQSxRQUNDLDZCQUFLLFdBQVksTUFBakIsRUFBd0IsT0FBU25FLEtBQWpDLEVBQXdDLFdBQVc3RyxNQUFNK0osSUFBTixDQUFXaEksRUFBOUQsRUFBa0UsU0FBVy9CLE1BQU1tSCxLQUFuRixHQUREO0FBR0E7O0lBRUtnRSxTOzs7QUFDTCxvQkFBWW5MLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxxSEFDWkEsS0FEWTs7QUFFbEIsU0FBS3FFLEdBQUwsR0FBV3JFLE1BQU1xRSxHQUFqQjtBQUZrQjtBQUdsQjs7OzsyQkFFUTtBQUFBOztBQUNSLE9BQUk5RCxZQUFZLEtBQUs4RCxHQUFMLENBQVM5RCxTQUFULENBQW1CbUcsR0FBbkIsQ0FBdUI7QUFBQSxXQUFZLG9CQUFDLFFBQUQsSUFBVSxVQUFZZCxRQUF0QixFQUFnQyxLQUFPLE9BQUt2QixHQUE1QyxHQUFaO0FBQUEsSUFBdkIsQ0FBaEI7O0FBRUEsT0FBSSxDQUFDLEtBQUtBLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZU8sYUFBcEIsRUFBbUM7QUFDbEMsV0FBUSxnQ0FBUjtBQUNBOztBQUdELE9BQUksS0FBS2dELEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVksSUFBZixJQUF1QixNQUEzQixFQUFtQztBQUNsQyxXQUFRLGdDQUFSO0FBQ0E7O0FBRUQsVUFDQztBQUFBO0FBQUEsTUFBSyxXQUFVLG1CQUFmO0FBQ0Msd0JBQUMsS0FBRCxJQUFPLEtBQU8sS0FBSzJDLEdBQW5CLEVBQXdCLE1BQU8sTUFBL0IsR0FERDtBQUVFOUQ7QUFGRixJQUREO0FBTUE7Ozs7RUF4QnNCaUQsTUFBTUMsUzs7QUF5QjdCOztBQUVELFNBQVMySCxRQUFULENBQWtCcEwsS0FBbEIsRUFBeUI7QUFDeEIsS0FBSVEsYUFBYyxFQUFsQjtBQUNBLEtBQUlvRixXQUFZNUYsTUFBTTRGLFFBQXRCOztBQUVBNUYsT0FBTXFFLEdBQU4sQ0FBVTdELFVBQVYsQ0FBcUI2SyxPQUFyQixDQUE2QixvQkFBWTtBQUN4QyxNQUFJekIsU0FBU2hILFVBQVQsSUFBdUJnRCxTQUFTN0QsRUFBcEMsRUFBd0M7QUFDdkN2QixjQUFXaUYsSUFBWCxDQUFnQixvQkFBQyxRQUFELElBQVUsV0FBYXpGLE1BQU1xRSxHQUFOLENBQVV2RCxLQUFWLENBQWdCQyxTQUF2QyxFQUFrRCxVQUFZNkksUUFBOUQsRUFBd0UsS0FBTzVKLE1BQU1xRSxHQUFyRixHQUFoQjtBQUNBO0FBQ0QsRUFKRDs7QUFNQSxRQUNDO0FBQUE7QUFBQSxJQUFTLFdBQVksVUFBckIsRUFBZ0MsSUFBTSxjQUFjdUIsU0FBUzdELEVBQTdEO0FBQ0M7QUFBQTtBQUFBLEtBQUssV0FBWSxrQkFBakI7QUFBcUM2RCxZQUFTaEI7QUFBOUMsR0FERDtBQUVDO0FBQUE7QUFBQSxLQUFLLFdBQVksaUJBQWpCO0FBQW9DcEU7QUFBcEM7QUFGRCxFQUREO0FBTUE7O0FBRUQsU0FBUzhLLFFBQVQsQ0FBa0J0TCxLQUFsQixFQUF5QjtBQUN4QixLQUFJNEosV0FBWTVKLE1BQU00SixRQUF0QjtBQUNBLEtBQUk3SSxZQUFhLEVBQWpCOztBQUVBZixPQUFNZSxTQUFOLENBQWdCc0ssT0FBaEIsQ0FBd0IsbUJBQVc7QUFDbEMsTUFBSXZKLFFBQVFhLFVBQVIsSUFBc0JpSCxTQUFTN0gsRUFBbkMsRUFBdUM7QUFDdENoQixhQUFVMEUsSUFBVixDQUFlM0QsT0FBZjtBQUNBO0FBQ0QsRUFKRDs7QUFNQSxRQUNDO0FBQUE7QUFBQSxJQUFLLFdBQVUsZ0JBQWY7QUFDQztBQUFBO0FBQUEsS0FBSSxXQUFVLHVCQUFkO0FBQ0M7QUFBQTtBQUFBLE1BQU0sV0FBVSwwQkFBaEI7QUFBNEM4SCxhQUFTakU7QUFBckQsSUFERDtBQUVDO0FBQUE7QUFBQSxNQUFNLFdBQVUsMEJBQWhCO0FBQTRDaUUsYUFBUzJCO0FBQXJEO0FBRkQsR0FERDtBQUtDLHNCQUFDLFdBQUQsSUFBYSxXQUFheEssU0FBMUIsRUFBcUMsS0FBT2YsTUFBTXFFLEdBQWxEO0FBTEQsRUFERDtBQVNBOztBQUVELFNBQVNtSCxXQUFULENBQXFCeEwsS0FBckIsRUFBNEI7QUFDM0IsS0FBSWUsWUFBWWYsTUFBTWUsU0FBTixDQUFnQjJGLEdBQWhCLENBQW9CO0FBQUEsU0FBVyxvQkFBQyxXQUFELElBQWEsU0FBVzVFLE9BQXhCLEVBQWlDLE9BQVM5QixNQUFNcUUsR0FBTixDQUFVcEUsU0FBcEQsR0FBWDtBQUFBLEVBQXBCLENBQWhCOztBQUVBLFFBQ0M7QUFBQTtBQUFBLElBQUssV0FBVSxxQkFBZjtBQUFzQ2M7QUFBdEMsRUFERDtBQUdBOztBQUVELFNBQVMwSyxXQUFULENBQXFCekwsS0FBckIsRUFBNEI7QUFDM0IsS0FBSThCLFVBQVU5QixNQUFNOEIsT0FBcEI7QUFDQSxLQUFJMkksVUFBVSxxQkFBZDs7QUFFQSxLQUFJM0ksUUFBUU0sUUFBWixFQUFzQjtBQUNyQnFJLGFBQVcsMEJBQVg7QUFDQTs7QUFFRCxRQUNDO0FBQUE7QUFBQSxJQUFHLE1BQU8sR0FBVixFQUFjLFdBQVczSSxRQUFRQyxFQUFqQyxFQUFxQyxXQUFhMEksT0FBbEQsRUFBMkQsU0FBV3pLLE1BQU1tSCxLQUE1RTtBQUFxRnJGLFVBQVE4QztBQUE3RixFQUREO0FBR0E7O0FBRUQsU0FBUzhHLEtBQVQsQ0FBZTFMLEtBQWYsRUFBc0I7QUFDckIsS0FBSXFFLE1BQU1yRSxNQUFNcUUsR0FBaEI7QUFDQSxLQUFJdkMsVUFBVXVDLElBQUkzRCxZQUFKLElBQW9CLEVBQWxDO0FBQ0EsS0FBSWtKLGlCQUFKO0FBQ0EsS0FBSWhFLGlCQUFKO0FBQ0EsS0FBSStGLFlBQVk7QUFDZjdFLFdBQVN6QyxJQUFJdkQsS0FBSixDQUFVYixTQUFWLEdBQXNCLE1BQXRCLEdBQStCO0FBRHpCLEVBQWhCO0FBR0EsS0FBSTJMLGFBQWE7QUFDaEJyQixtQkFBaUIsU0FBU2xHLElBQUl4RCxZQUFiLEdBQTRCLE9BQTVCLEdBQXNDaUIsUUFBUWtJLEtBQTlDLEdBQXNEO0FBRHZELEVBQWpCOztBQUlBLEtBQUksQ0FBQzNGLElBQUl6RCxhQUFULEVBQXdCO0FBQ3ZCLFNBQ0MsZ0NBREQ7QUFHQTs7QUFFRCxRQUNDO0FBQUE7QUFBQSxJQUFLLFdBQVksa0JBQWpCLEVBQW9DLE9BQVMrSyxTQUE3QztBQUNDLCtCQUFLLFdBQVksY0FBakIsRUFBZ0MsU0FBV3RILElBQUlsRSxTQUEvQyxHQUREO0FBRUM7QUFBQTtBQUFBLEtBQUssV0FBWSxPQUFqQjtBQUNDO0FBQUE7QUFBQSxNQUFRLFdBQVkscUJBQXBCLEVBQTBDLFNBQVdrRSxJQUFJbEUsU0FBekQ7QUFBQTtBQUFBLElBREQ7QUFFQztBQUFBO0FBQUEsTUFBSyxXQUFZLGNBQWpCO0FBQ0MsaUNBQUssV0FBWSxhQUFqQixFQUErQixPQUFTeUwsVUFBeEMsR0FERDtBQUVDO0FBQUE7QUFBQSxPQUFLLFdBQVksY0FBakI7QUFDQztBQUFBO0FBQUEsUUFBSyxXQUFZLGdCQUFqQjtBQUFtQzlKLGNBQVE4QztBQUEzQyxNQUREO0FBRUM7QUFBQTtBQUFBLFFBQUssV0FBWSxhQUFqQjtBQUFnQ1AsVUFBSXpELGFBQUosQ0FBa0JnRSxJQUFsRDtBQUFBO0FBQXlEUCxVQUFJMUQsYUFBSixDQUFrQmdGO0FBQTNFLE1BRkQ7QUFHRTdELGFBQVFVLEdBQVIsSUFBZTtBQUFBO0FBQUEsUUFBRyxNQUFRVixRQUFRVSxHQUFuQixFQUF3QixXQUFZLGFBQXBDLEVBQWtELFFBQVMsUUFBM0Q7QUFBcUVWLGNBQVFVO0FBQTdFLE1BSGpCO0FBSUUsTUFBQ1YsUUFBUU0sUUFBVCxJQUFxQjtBQUFBO0FBQUEsUUFBSyxXQUFZLGdCQUFqQjtBQUNyQjtBQUFBO0FBQUEsU0FBSyxXQUFZLHNCQUFqQjtBQUFBO0FBQUEsT0FEcUI7QUFJckI7QUFBQTtBQUFBLFNBQUssV0FBWSxzQkFBakI7QUFBeUNOLGVBQVFvSSxPQUFqRDtBQUFBO0FBQUE7QUFKcUIsTUFKdkI7QUFVQyx5QkFBQyxTQUFELElBQVcsTUFBUXBJLFFBQVErSixJQUEzQjtBQVZEO0FBRkQ7QUFGRDtBQUZELEVBREQ7QUF1QkE7O0FBRUQsU0FBU0MsU0FBVCxDQUFtQjlMLEtBQW5CLEVBQTBCO0FBQ3pCLEtBQUk2TCxPQUFPN0wsTUFBTTZMLElBQWpCOztBQUVBLEtBQUlBLE9BQU8sQ0FBWCxFQUFjO0FBQ2JBLFNBQU9BLEtBQUtyRCxPQUFMLENBQWEsQ0FBYixJQUFrQixFQUF6QjtBQUNBcUQsU0FBT0EsS0FBS0UsT0FBTCxDQUFhLEdBQWIsRUFBa0IsR0FBbEIsQ0FBUDtBQUNBOztBQUVELEtBQUlGLElBQUosRUFBVTtBQUNULFNBQ0M7QUFBQTtBQUFBO0FBQ0M7QUFBQTtBQUFBLE1BQUssV0FBWSxhQUFqQjtBQUNDO0FBQUE7QUFBQSxPQUFLLFdBQVksbUJBQWpCO0FBQUE7QUFBQSxLQUREO0FBRUM7QUFBQTtBQUFBLE9BQUssV0FBWSxtQkFBakI7QUFBc0M3TCxXQUFNNkwsSUFBNUM7QUFBQTtBQUFBO0FBRkQsSUFERDtBQUtDO0FBQUE7QUFBQSxNQUFLLFdBQVksYUFBakI7QUFBQTtBQUFBO0FBTEQsR0FERDtBQVdBLEVBWkQsTUFZTztBQUNOLFNBQ0MsZ0NBREQ7QUFHQTtBQUNEOztBQUVELFNBQVNuSixXQUFULENBQXFCc0osS0FBckIsRUFBNEJqSyxFQUE1QixFQUFnQztBQUMvQixLQUFJLENBQUNpSyxNQUFNN0osTUFBWCxFQUFtQixPQUFPLElBQVA7O0FBRW5CLE1BQUssSUFBSUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOEosTUFBTTdKLE1BQTFCLEVBQWtDRCxHQUFsQyxFQUF1QztBQUN0QyxNQUFJOEosTUFBTTlKLENBQU4sRUFBU0gsRUFBVCxJQUFlQSxFQUFuQixFQUF1QjtBQUN0QixVQUFPaUssTUFBTTlKLENBQU4sQ0FBUDtBQUNBO0FBQ0Q7QUFDRDs7SUFFSytKLGU7OztBQUNMLDBCQUFZak0sS0FBWixFQUFtQjtBQUFBOztBQUFBLGlJQUNaQSxLQURZOztBQUVsQixTQUFLcUUsR0FBTCxHQUFnQnJFLE1BQU1xRSxHQUF0QjtBQUNBLFNBQUs2SCxtQkFBTCxHQUE0QixPQUFLQSxtQkFBTCxDQUF5QmhNLElBQXpCLFFBQTVCO0FBQ0EsU0FBS2lNLFVBQUwsR0FBcUIsT0FBS0EsVUFBTCxDQUFnQmpNLElBQWhCLFFBQXJCO0FBQ0EsU0FBS2tNLGtCQUFMLEdBQTJCLE9BQUtBLGtCQUFMLENBQXdCbE0sSUFBeEIsUUFBM0I7QUFMa0I7QUFNbEI7Ozs7c0NBRW1CMkIsQyxFQUFHO0FBQ3RCLE9BQUlFLEtBQUtGLEVBQUVHLGFBQUYsQ0FBZ0JDLFlBQWhCLENBQTZCLFNBQTdCLENBQVQ7QUFDQSxRQUFLb0MsR0FBTCxDQUFTaEMsUUFBVCxDQUFrQixFQUFDZCxpQkFBaUJRLEVBQWxCLEVBQWxCO0FBQ0E7OzsrQkFFWTtBQUNaLFFBQUtzQyxHQUFMLENBQVNoQyxRQUFULENBQWtCLEVBQUNkLGlCQUFpQixJQUFsQixFQUFsQjtBQUNBOzs7dUNBRW9CO0FBQ3BCLE9BQUlxRSxXQUFXLElBQWY7QUFDQSxPQUFJN0QsS0FBSyxLQUFLc0MsR0FBTCxDQUFTdkQsS0FBVCxDQUFlUyxlQUF4Qjs7QUFFQSxRQUFLLElBQUlXLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLbUMsR0FBTCxDQUFTOUQsU0FBVCxDQUFtQjRCLE1BQXZDLEVBQStDRCxHQUEvQyxFQUFvRDtBQUNuRCxRQUFJSCxNQUFNLEtBQUtzQyxHQUFMLENBQVM5RCxTQUFULENBQW1CMkIsQ0FBbkIsRUFBc0JILEVBQWhDLEVBQW9DO0FBQ25DNkQsZ0JBQVcsS0FBS3ZCLEdBQUwsQ0FBUzlELFNBQVQsQ0FBbUIyQixDQUFuQixDQUFYO0FBQ0E7QUFDQTtBQUNEOztBQUVELFVBQU8wRCxRQUFQO0FBQ0E7OzsyQkFFUTtBQUFBOztBQUNSLE9BQUlyRixZQUFZLEtBQUs4RCxHQUFMLENBQVM5RCxTQUFULENBQW1CbUcsR0FBbkIsQ0FBdUIsb0JBQVk7QUFDbEQsV0FDQyxvQkFBQyxjQUFELElBQWdCLFVBQVlkLFFBQTVCLEVBQXNDLE1BQVEsT0FBS3NHLG1CQUFuRCxFQUF3RSxjQUFnQixPQUFLN0gsR0FBTCxDQUFTeEQsWUFBakcsR0FERDtBQUdBLElBSmUsQ0FBaEI7O0FBTUEsT0FBSSxDQUFDLEtBQUt3RCxHQUFMLENBQVN2RCxLQUFULENBQWVRLG1CQUFwQixFQUF5QztBQUN4QyxXQUFRLGdDQUFSO0FBQ0E7O0FBRUQsT0FBSSxLQUFLK0MsR0FBTCxDQUFTdkQsS0FBVCxDQUFlUyxlQUFmLEtBQW1DLElBQXZDLEVBQTZDO0FBQzVDLFdBQ0M7QUFBQTtBQUFBLE9BQUssV0FBWSxrQkFBakI7QUFDRWhCO0FBREYsS0FERDtBQUtBLElBTkQsTUFNTztBQUNOLFdBQ0M7QUFBQTtBQUFBLE9BQUssV0FBWSxrQkFBakI7QUFDQyx5QkFBQyxxQkFBRDtBQUNDLGdCQUFhLEtBQUs2TCxrQkFBTCxFQURkO0FBRUMsWUFBVSxLQUFLRCxVQUZoQjtBQUdDLFdBQVMsS0FBSzlILEdBSGY7QUFJQyxrQkFBZSxLQUFLQSxHQUFMLENBQVN2RCxLQUFULENBQWVTO0FBSi9CO0FBREQsS0FERDtBQVVBO0FBQ0Q7Ozs7RUE3RDRCaUMsTUFBTUMsUzs7QUFnRXBDLFNBQVM0SSxjQUFULENBQXdCck0sS0FBeEIsRUFBK0I7QUFDOUIsS0FBSTZHLFFBQVE7QUFDWHlGLG1CQUFpQnRNLE1BQU00RixRQUFOLENBQWUyRztBQURyQixFQUFaOztBQUlBLFFBQ0M7QUFBQTtBQUFBLElBQUssV0FBWSx3QkFBakIsRUFBMEMsV0FBV3ZNLE1BQU00RixRQUFOLENBQWU3RCxFQUFwRSxFQUF3RSxTQUFXL0IsTUFBTThDLElBQXpGO0FBQ0MsK0JBQUssS0FBTzlDLE1BQU1hLFlBQU4sR0FBcUJiLE1BQU00RixRQUFOLENBQWVvRSxLQUFoRCxHQUREO0FBRUM7QUFBQTtBQUFBLEtBQUssV0FBWSx1QkFBakIsRUFBeUMsT0FBUW5ELEtBQWpEO0FBQTBEN0csU0FBTTRGLFFBQU4sQ0FBZWhCO0FBQXpFO0FBRkQsRUFERDtBQU1BOztBQUVELFNBQVM0SCxxQkFBVCxDQUErQnhNLEtBQS9CLEVBQXNDO0FBQ3JDLEtBQUlxRSxNQUFNckUsTUFBTXFFLEdBQWhCO0FBQ0EsS0FBSXhELGVBQWV3RCxJQUFJeEQsWUFBdkI7QUFDQSxLQUFJTCxhQUFhLEVBQWpCO0FBQ0EsS0FBSWlNLGdCQUFnQjtBQUNuQkgsbUJBQWlCdE0sTUFBTTRGLFFBQU4sQ0FBZTJHLEtBRGI7QUFFbkJoQyxtQkFBaUIsU0FBUzFKLFlBQVQsR0FBd0IsUUFBeEIsR0FBbUNiLE1BQU00RixRQUFOLENBQWU4RyxJQUFsRCxHQUF5RCxHQUZ2RDtBQUduQkMsa0JBQWdCM00sTUFBTTRGLFFBQU4sQ0FBZWdIO0FBSFosRUFBcEI7O0FBTUEsTUFBSyxJQUFJMUssSUFBSSxDQUFiLEVBQWdCQSxJQUFJbUMsSUFBSTdELFVBQUosQ0FBZTJCLE1BQW5DLEVBQTJDRCxHQUEzQyxFQUFnRDtBQUMvQyxNQUFJbEMsTUFBTTRDLFVBQU4sSUFBb0J5QixJQUFJN0QsVUFBSixDQUFlMEIsQ0FBZixFQUFrQlUsVUFBMUMsRUFBc0Q7QUFDckRwQyxjQUFXaUYsSUFBWCxDQUFnQnBCLElBQUk3RCxVQUFKLENBQWUwQixDQUFmLENBQWhCO0FBQ0E7QUFDRDs7QUFFRDFCLGNBQWFBLFdBQVdrRyxHQUFYLENBQWUsZUFBTztBQUNsQyxTQUFPLG9CQUFDLGNBQUQ7QUFDTixpQkFBZ0IxRyxNQUFNNEYsUUFBTixDQUFlaEIsSUFEekI7QUFFTixRQUFPaUksR0FGRDtBQUdOLGNBQWF4SSxJQUFJdkQsS0FBSixDQUFVQyxTQUhqQjtBQUlOLFFBQU9zRDtBQUpELElBQVA7QUFNQSxFQVBZLENBQWI7O0FBU0EsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFZLHlDQUFqQjtBQUNDO0FBQUE7QUFBQSxLQUFRLFdBQVksdUJBQXBCLEVBQTRDLFNBQVdyRSxNQUFNOE0sSUFBN0QsRUFBbUUsT0FBU0wsYUFBNUU7QUFDQztBQUFBO0FBQUE7QUFBT3pNLFVBQU00RixRQUFOLENBQWVoQjtBQUF0QjtBQURELEdBREQ7QUFJRXBFO0FBSkYsRUFERDtBQVFBOztBQUVELFNBQVN1TSxjQUFULENBQXdCL00sS0FBeEIsRUFBK0I7QUFDOUIsS0FBSWUsWUFBWSxFQUFoQjtBQUNBLEtBQUlzRCxNQUFNckUsTUFBTXFFLEdBQWhCOztBQUVBLE1BQUssSUFBSW5DLElBQUksQ0FBYixFQUFnQkEsSUFBSWxDLE1BQU1lLFNBQU4sQ0FBZ0JvQixNQUFwQyxFQUE0Q0QsR0FBNUMsRUFBaUQ7QUFDaEQsTUFBSWxDLE1BQU02TSxHQUFOLENBQVU5SyxFQUFWLElBQWdCL0IsTUFBTWUsU0FBTixDQUFnQm1CLENBQWhCLEVBQW1CUyxVQUF2QyxFQUFtRDtBQUNsRDVCLGFBQVUwRSxJQUFWLENBQWV6RixNQUFNZSxTQUFOLENBQWdCbUIsQ0FBaEIsQ0FBZjtBQUNBO0FBQ0Q7O0FBRURuQixhQUFZQSxVQUFVMkYsR0FBVixDQUFjLG1CQUFXO0FBQ3BDLFNBQU8sb0JBQUMsYUFBRCxJQUFlLFNBQVc1RSxPQUExQixFQUFtQyxVQUFZOUIsTUFBTWdOLFlBQXJELEVBQW1FLEtBQU9oTixNQUFNNk0sR0FBTixDQUFVbEgsTUFBcEYsRUFBNEYsS0FBT3RCLEdBQW5HLEdBQVA7QUFDQSxFQUZXLENBQVo7O0FBSUEsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFZLGlCQUFqQjtBQUNDO0FBQUE7QUFBQSxLQUFLLFdBQVkseUJBQWpCO0FBQ0M7QUFBQTtBQUFBLE1BQUssV0FBWSw0QkFBakI7QUFBK0NyRSxVQUFNNk0sR0FBTixDQUFVbEg7QUFBekQsSUFERDtBQUVDO0FBQUE7QUFBQSxNQUFLLFdBQVksNEJBQWpCO0FBQStDM0YsVUFBTTZNLEdBQU4sQ0FBVXRCO0FBQXpEO0FBRkQsR0FERDtBQUtDO0FBQUE7QUFBQSxLQUFLLFdBQVksdUJBQWpCO0FBQ0V4SztBQURGO0FBTEQsRUFERDtBQVdBOztBQUVELFNBQVNrTSxhQUFULENBQXVCak4sS0FBdkIsRUFBOEI7QUFDN0IsS0FBSXFFLE1BQU1yRSxNQUFNcUUsR0FBaEI7QUFDQSxLQUFJdkMsVUFBVTlCLE1BQU04QixPQUFwQjtBQUNBLEtBQUlvTCxpQkFBaUIsbUNBQW1DcEwsUUFBUUMsRUFBaEU7QUFDQSxLQUFJb0wscUJBQXFCLHNCQUF6QjtBQUNBLEtBQUlDLHFCQUFxQixzQkFBekI7QUFDQSxLQUFJQyxjQUFjLEtBQWxCO0FBQ0EsS0FBSWhJLGlCQUFpQmhCLElBQUl2RCxLQUFKLENBQVV1RSxjQUEvQjtBQUNBLEtBQUlpSSxZQUFZO0FBQ2Z4RyxXQUFTaEYsUUFBUVUsR0FBUixHQUFjLE9BQWQsR0FBd0I7QUFEbEIsRUFBaEI7O0FBSUEsVUFBUytLLGFBQVQsQ0FBdUIxTCxDQUF2QixFQUEwQjtBQUN6QkEsSUFBRUcsYUFBRixDQUFnQnVFLFNBQWhCLENBQTBCaUgsTUFBMUIsQ0FBaUMsNkJBQWpDO0FBQ0EzTCxJQUFFRyxhQUFGLENBQWdCb0IsVUFBaEIsQ0FBMkJnRCxhQUEzQixDQUF5Qyx1QkFBekMsRUFBa0VHLFNBQWxFLENBQTRFaUgsTUFBNUUsQ0FBbUYsNkJBQW5GO0FBQ0EzTCxJQUFFRyxhQUFGLENBQWdCb0IsVUFBaEIsQ0FBMkJtRCxTQUEzQixDQUFxQ2lILE1BQXJDLENBQTRDLHVCQUE1QztBQUNBOztBQUVELFVBQVNDLGVBQVQsQ0FBeUI1TCxDQUF6QixFQUE0QjtBQUMzQixNQUFJbUUscUJBQUo7QUFDQSxNQUFJRixrQkFBSjtBQUNBLE1BQUk0SCxtQkFBSjs7QUFFQSxNQUFJLENBQUNMLFdBQUQsSUFBZ0IsQ0FBQ3hMLENBQXJCLEVBQXdCO0FBQ3ZCLFVBQU8sS0FBUDtBQUNBOztBQUVEbUUsaUJBQWUxQyxTQUFTOEMsYUFBVCxDQUF1QixTQUF2QixFQUFrQ25CLFlBQWpEO0FBQ0FhLGNBQVlqRSxFQUFFaUUsU0FBZDtBQUNBNEgsZUFBYXBLLFNBQVM4QyxhQUFULENBQXVCLHdCQUF2QixFQUFpRG5CLFlBQTlEOztBQUVBM0MsU0FBTytELE1BQVAsQ0FBYztBQUNibEYsUUFBSzJFLFlBQVlFLFlBQVosR0FBMkIwSCxVQURuQjtBQUVieE0sU0FBTSxDQUZPO0FBR2JvRixhQUFVO0FBSEcsR0FBZDtBQUtBOztBQUVELEtBQUksQ0FBQ3hFLFFBQVErSixJQUFiLEVBQW1CO0FBQ2xCL0osVUFBUStKLElBQVIsR0FBZSxDQUFmO0FBQ0EsRUFGRCxNQUVPO0FBQ04vSixVQUFRK0osSUFBUixHQUFlOEIsU0FBUzdMLFFBQVErSixJQUFqQixFQUF1QixFQUF2QixDQUFmO0FBQ0E7O0FBRUQsS0FBSS9KLFFBQVFNLFFBQVosRUFBc0I7QUFDckIrSyx3QkFBc0IsZ0NBQXRCO0FBQ0E7O0FBRURyTCxTQUFRK0osSUFBUixHQUFlL0osUUFBUStKLElBQVIsQ0FBYXJELE9BQWIsQ0FBcUIsQ0FBckIsSUFBMEIsRUFBekM7QUFDQTFHLFNBQVErSixJQUFSLEdBQWUvSixRQUFRK0osSUFBUixDQUFhRSxPQUFiLENBQXFCLEdBQXJCLEVBQTBCLEdBQTFCLENBQWY7O0FBRUEsS0FBSWpLLFFBQVFDLEVBQVIsSUFBY3NELGNBQWxCLEVBQWtDO0FBQ2pDNkgsb0JBQWtCLHdCQUFsQjtBQUNBQyx3QkFBc0IsOEJBQXRCO0FBQ0FDLHdCQUFzQiw4QkFBdEI7QUFDQUMsZ0JBQWMsSUFBZDs7QUFFQWhKLE1BQUkzRCxZQUFKLEdBQW1CLElBQW5CO0FBQ0E7O0FBRUQsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFhd00sY0FBbEI7QUFDQztBQUFBO0FBQUEsS0FBUSxXQUFhQyxrQkFBckIsRUFBeUMsU0FBV0ksYUFBcEQsRUFBbUUsS0FBT0UsZUFBMUU7QUFDQztBQUFBO0FBQUE7QUFBTzNMLFlBQVE4QztBQUFmO0FBREQsR0FERDtBQUlDO0FBQUE7QUFBQSxLQUFLLFdBQWF3SSxrQkFBbEI7QUFDQztBQUFBO0FBQUEsTUFBSyxXQUFZLDBCQUFqQjtBQUE2Q3BOLFVBQU00RixRQUFuRDtBQUFBO0FBQWdFNUYsVUFBTTZNO0FBQXRFLElBREQ7QUFFQztBQUFBO0FBQUEsTUFBSyxXQUFZLHdCQUFqQjtBQUNDO0FBQUE7QUFBQSxPQUFLLFdBQVksOEJBQWpCO0FBQUE7QUFBQSxLQUREO0FBRUM7QUFBQTtBQUFBLE9BQUssV0FBWSw4QkFBakI7QUFBaUQvSyxhQUFRb0ksT0FBekQ7QUFBQTtBQUFBO0FBRkQsSUFGRDtBQU1DO0FBQUE7QUFBQSxNQUFLLFdBQVksc0JBQWpCO0FBQ0M7QUFBQTtBQUFBLE9BQUssV0FBWSw0QkFBakI7QUFBQTtBQUFBLEtBREQ7QUFFQztBQUFBO0FBQUEsT0FBSyxXQUFZLDRCQUFqQjtBQUNFcEksYUFBUStKLElBRFY7QUFBQTtBQUFBO0FBRkQsSUFORDtBQVlDO0FBQUE7QUFBQSxNQUFHLE1BQVEvSixRQUFRVSxHQUFuQixFQUF3QixXQUFZLHNCQUFwQyxFQUEyRCxPQUFTOEssU0FBcEU7QUFBaUZ4TCxZQUFRVTtBQUF6RjtBQVpEO0FBSkQsRUFERDtBQXFCQTs7SUFFS29MLE87OztBQUNMLGtCQUFZNU4sS0FBWixFQUFtQjtBQUFBOztBQUFBLGtIQUNaQSxLQURZOztBQUVsQixVQUFLNk4sTUFBTCxHQUFjLFFBQUtBLE1BQUwsQ0FBWTNOLElBQVosU0FBZDtBQUNBLFVBQUtjLElBQUwsR0FBWSxRQUFLQSxJQUFMLENBQVVkLElBQVYsU0FBWjtBQUNBLFVBQUt3QixJQUFMLEdBQVksUUFBS0EsSUFBTCxDQUFVeEIsSUFBVixTQUFaO0FBQ0EsVUFBS21FLEdBQUwsR0FBV3JFLE1BQU1xRSxHQUFqQjtBQUxrQjtBQU1sQjs7OzsyQkFFUTtBQUNSLFFBQUtBLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJaLGdCQUFZLENBQUMsS0FBSzRDLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVc7QUFEWCxJQUFsQjtBQUdBOzs7dUJBRUlJLEMsRUFBRztBQUNQLE9BQUlpTSxPQUFPak0sRUFBRUcsYUFBRixDQUFnQkMsWUFBaEIsQ0FBNkIsV0FBN0IsQ0FBWDtBQUNBLE9BQUk4TCxPQUFPLEdBQVg7QUFDQSxPQUFJL00sT0FBTyxLQUFLcUQsR0FBTCxDQUFTdkQsS0FBVCxDQUFlRSxJQUExQjtBQUNBLE9BQUlDLGdCQUFKOztBQUVBLFdBQVE2TSxJQUFSO0FBQ0MsU0FBSyxNQUFMO0FBQ0M3TSxlQUFVRCxPQUFPK00sSUFBakI7QUFDRDtBQUNBLFNBQUssT0FBTDtBQUNDOU0sZUFBVUQsT0FBTytNLElBQWpCO0FBQ0Q7QUFORDs7QUFTQSxRQUFLMUosR0FBTCxDQUFTaEMsUUFBVCxDQUFrQjtBQUNqQnBCLGFBQVNBO0FBRFEsSUFBbEI7QUFHQTs7O3lCQUVNO0FBQ04sT0FBSVMsT0FBTyxLQUFLMkMsR0FBTCxDQUFTdkQsS0FBVCxDQUFlWSxJQUExQjtBQUNBLE9BQUl1RSxPQUFPM0MsU0FBUzRDLGdCQUFULENBQTBCLDJDQUExQixDQUFYOztBQUVBLE9BQUl4RSxRQUFRLEtBQVosRUFBbUI7QUFDbEJBLFdBQU8sTUFBUDtBQUNBLFNBQUsyQyxHQUFMLENBQVNoQyxRQUFULENBQWtCO0FBQ2pCWixpQkFBWTtBQURLLEtBQWxCO0FBR0EsSUFMRCxNQUtPO0FBQ05DLFdBQU8sS0FBUDtBQUNBOztBQUVELFFBQUsyQyxHQUFMLENBQVNoQyxRQUFULENBQWtCO0FBQ2pCWCxVQUFNQTtBQURXLElBQWxCOztBQUlBLFFBQUssSUFBSVEsSUFBSSxDQUFiLEVBQWdCQSxJQUFJK0QsS0FBSzlELE1BQXpCLEVBQWlDRCxHQUFqQyxFQUFzQztBQUNyQytELFNBQUsvRCxDQUFMLEVBQVFxRSxTQUFSLENBQWtCQyxNQUFsQixDQUF5Qix1QkFBekI7QUFDQTtBQUNEOzs7MkJBRVE7O0FBRVIsT0FBSXpELFVBQUosRUFBZ0I7QUFDZixXQUNDLGdDQUREO0FBR0E7O0FBRUQsT0FBSWlMLGdCQUFnQiwrREFBK0QsS0FBSzNKLEdBQUwsQ0FBU3ZELEtBQVQsQ0FBZVksSUFBbEc7O0FBRUEsVUFDQztBQUFBO0FBQUEsTUFBSyxXQUFZLFNBQWpCO0FBQ0M7QUFBQTtBQUFBLE9BQUssV0FBWSxnQkFBakI7QUFDQztBQUFBO0FBQUEsUUFBUSxXQUFZLHdDQUFwQixFQUE2RCxTQUFXLEtBQUsyQyxHQUFMLENBQVNqRSxZQUFqRjtBQUNDO0FBREQsTUFERDtBQUlDLHlCQUFDLFdBQUQsSUFBYSxLQUFPLEtBQUtpRSxHQUF6QixFQUE4QixNQUFRLEtBQUtyRCxJQUEzQyxHQUpEO0FBS0M7QUFBQTtBQUFBLFFBQUssV0FBWSxpQkFBakIsRUFBbUMsU0FBVyxLQUFLVSxJQUFuRDtBQUNDO0FBQUE7QUFBQSxTQUFRLFdBQWFzTSxhQUFyQjtBQUNDO0FBREQ7QUFERDtBQUxEO0FBREQsSUFERDtBQWVBOzs7O0VBbEZvQnhLLE1BQU1DLFM7O0FBcUY1QixTQUFTd0ssV0FBVCxDQUFxQmpPLEtBQXJCLEVBQTRCO0FBQzNCLEtBQUlxRSxNQUFNckUsTUFBTXFFLEdBQWhCOztBQUVBLEtBQUlBLElBQUl2RCxLQUFKLENBQVVZLElBQVYsSUFBa0IsS0FBdEIsRUFBNkI7QUFDNUIsU0FDQyxnQ0FERDtBQUdBOztBQUVELFFBQ0M7QUFBQTtBQUFBLElBQUssV0FBWSxlQUFqQjtBQUNDO0FBQUE7QUFBQSxLQUFRLFdBQVksc0NBQXBCLEVBQTJELGFBQVUsTUFBckUsRUFBNEUsU0FBVzFCLE1BQU1nQixJQUE3RjtBQUNDO0FBREQsR0FERDtBQUlDO0FBQUE7QUFBQSxLQUFRLFdBQVksdUNBQXBCLEVBQTRELGFBQVUsT0FBdEUsRUFBOEUsU0FBV2hCLE1BQU1nQixJQUEvRjtBQUNDO0FBREQ7QUFKRCxFQUREO0FBVUE7O0FBRUQsU0FBU2tOLE1BQVQsQ0FBZ0JsTyxLQUFoQixFQUF1QjtBQUN0QixLQUFJcUUsTUFBTXJFLE1BQU1xRSxHQUFoQjtBQUNBLEtBQUlvRyxVQUFVLFFBQWQ7O0FBRUEsS0FBSXBHLElBQUl2RCxLQUFKLENBQVVXLFVBQVYsSUFBd0IsQ0FBQ3NCLFVBQTdCLEVBQXlDO0FBQ3hDMEgsYUFBVyxjQUFYO0FBQ0E7O0FBRUQsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFhQSxPQUFsQjtBQUNDO0FBQUE7QUFBQSxLQUFLLFdBQVksZUFBakI7QUFDQztBQUFBO0FBQUEsTUFBSyxXQUFZLGNBQWpCO0FBQ0Msb0NBQVEsV0FBWSxlQUFwQixFQUFvQyxTQUFXcEcsSUFBSWpFLFlBQW5ELEdBREQ7QUFFQyxpQ0FBSyxXQUFZLGNBQWpCLEdBRkQ7QUFHQztBQUFBO0FBQUEsT0FBSyxXQUFZLGdCQUFqQjtBQUFBO0FBQ2dCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEaEIsS0FIRDtBQU1DO0FBQUE7QUFBQSxPQUFLLFdBQVksY0FBakI7QUFBQTtBQUFBLEtBTkQ7QUFTQztBQUFBO0FBQUEsT0FBSyxXQUFZLGNBQWpCO0FBQ0M7QUFBQTtBQUFBLFFBQUssV0FBWSxjQUFqQjtBQUNDLG1DQUFLLFdBQVksOENBQWpCLEdBREQ7QUFFQztBQUFBO0FBQUEsU0FBSyxXQUFZLG1CQUFqQjtBQUFBO0FBQUE7QUFGRCxNQUREO0FBS0M7QUFBQTtBQUFBLFFBQUssV0FBWSxjQUFqQjtBQUNDLG1DQUFLLFdBQVksOENBQWpCLEdBREQ7QUFFQztBQUFBO0FBQUEsU0FBSyxXQUFZLG1CQUFqQjtBQUFBO0FBQUE7QUFGRCxNQUxEO0FBU0M7QUFBQTtBQUFBLFFBQUssV0FBWSxjQUFqQjtBQUNDLG1DQUFLLFdBQVksMkNBQWpCLEdBREQ7QUFFQztBQUFBO0FBQUEsU0FBSyxXQUFZLG1CQUFqQjtBQUFBO0FBQUE7QUFGRCxNQVREO0FBYUM7QUFBQTtBQUFBLFFBQUssV0FBWSxjQUFqQjtBQUNDLG1DQUFLLFdBQVksOENBQWpCLEdBREQ7QUFFQztBQUFBO0FBQUEsU0FBSyxXQUFZLG1CQUFqQjtBQUFBO0FBQUE7QUFGRDtBQWJELEtBVEQ7QUEyQkM7QUFBQTtBQUFBLE9BQUssV0FBWSxlQUFqQjtBQUNDO0FBQUE7QUFBQSxRQUFLLFdBQVksbUJBQWpCO0FBQ0M7QUFBQTtBQUFBLFNBQUssV0FBWSxzQkFBakI7QUFBQTtBQUFBLE9BREQ7QUFFQyxtQ0FBSyxXQUFZLDhCQUFqQixHQUZEO0FBR0MsbUNBQUssV0FBWSw4QkFBakIsR0FIRDtBQUlDLG1DQUFLLFdBQVksOEJBQWpCLEdBSkQ7QUFLQyxtQ0FBSyxXQUFZLDhCQUFqQixHQUxEO0FBTUMsbUNBQUssV0FBWSw4QkFBakIsR0FORDtBQU9DLG1DQUFLLFdBQVksOEJBQWpCLEdBUEQ7QUFRQyxtQ0FBSyxXQUFZLDhCQUFqQixHQVJEO0FBU0MsbUNBQUssV0FBWSw4QkFBakI7QUFURCxNQUREO0FBWUM7QUFBQTtBQUFBLFFBQUssV0FBWSxtQkFBakI7QUFDQztBQUFBO0FBQUEsU0FBSyxXQUFZLHNCQUFqQjtBQUFBO0FBQUEsT0FERDtBQUVDLG1DQUFLLFdBQVksOEJBQWpCLEdBRkQ7QUFHQyxtQ0FBSyxXQUFZLDhCQUFqQixHQUhEO0FBSUMsbUNBQUssV0FBWSw4QkFBakIsR0FKRDtBQUtDLG1DQUFLLFdBQVksOEJBQWpCLEdBTEQ7QUFNQyxtQ0FBSyxXQUFZLDhCQUFqQixHQU5EO0FBT0MsbUNBQUssV0FBWSw4QkFBakIsR0FQRDtBQVFDLG1DQUFLLFdBQVksOEJBQWpCLEdBUkQ7QUFTQyxtQ0FBSyxXQUFZLDhCQUFqQjtBQVREO0FBWkQ7QUEzQkQsSUFERDtBQXFEQztBQUFBO0FBQUEsTUFBSyxXQUFZLGdCQUFqQjtBQUFBO0FBRUMsK0JBQUcsTUFBSyxxQkFBUixFQUE4QixRQUFPLFFBQXJDLEVBQThDLFdBQVksYUFBMUQ7QUFGRDtBQXJERDtBQURELEVBREQ7QUE4REE7O0lBRUsrTixLOzs7QUFDTCxnQkFBWW5PLEtBQVosRUFBbUI7QUFBQTs7QUFBQSw4R0FDWkEsS0FEWTs7QUFFbEIsVUFBS3FFLEdBQUwsR0FBV3JFLE1BQU1xRSxHQUFqQjtBQUNBLFVBQUsrSixLQUFMLEdBQWEsUUFBS0EsS0FBTCxDQUFXbE8sSUFBWCxTQUFiO0FBQ0EsVUFBS21PLElBQUwsR0FBWSxRQUFLQSxJQUFMLENBQVVuTyxJQUFWLFNBQVo7QUFDQSxVQUFLb08sUUFBTCxHQUFnQnRPLE1BQU0wQixJQUF0QjtBQUxrQjtBQU1sQjs7Ozt3QkFFS0csQyxFQUFHO0FBQ1IsUUFBS3dDLEdBQUwsQ0FBU2tLLGFBQVQsR0FBeUIxTSxDQUF6QjtBQUNBMk0sS0FBRTNNLENBQUYsRUFBS3VNLEtBQUwsQ0FBVztBQUNWSyxrQkFBYyxDQURKO0FBRVZDLG9CQUFnQixDQUZOO0FBR1ZDLGNBQVUsSUFIQTtBQUlWQyxlQUFXLG9CQUpEO0FBS1ZDLGVBQVcsb0JBTEQ7QUFNVkMsZ0JBQVksQ0FBQztBQUNaQyxpQkFBWSxJQURBO0FBRVpDLGVBQVU7QUFDVFAsb0JBQWMsQ0FETDtBQUVUQyxzQkFBZ0I7QUFGUDtBQUZFLEtBQUQsRUFNVDtBQUNGSyxpQkFBWSxJQURWO0FBRUZDLGVBQVU7QUFDVFAsb0JBQWMsQ0FETDtBQUVUQyxzQkFBZ0I7QUFGUDtBQUZSLEtBTlMsRUFZVDtBQUNGSyxpQkFBWSxJQURWO0FBRUZDLGVBQVU7QUFDVFAsb0JBQWMsQ0FETDtBQUVUQyxzQkFBZ0I7QUFGUDtBQUZSLEtBWlMsRUFrQlQ7QUFDRkssaUJBQVksSUFEVjtBQUVGQyxlQUFVO0FBQ1RQLG9CQUFjLENBREw7QUFFVEMsc0JBQWdCO0FBRlA7QUFGUixLQWxCUztBQU5GLElBQVg7QUFnQ0E7Ozt5QkFFTTtBQUNOLFFBQUtySyxHQUFMLENBQVMxQyxRQUFULEdBQW9CLEtBQXBCO0FBQ0EsUUFBSzBDLEdBQUwsQ0FBU2hDLFFBQVQsQ0FBa0I7QUFDakJWLGNBQVU7QUFETyxJQUFsQjtBQUdBOzs7eUNBRXNCO0FBQ3RCNk0sS0FBRSxLQUFLbkssR0FBTCxDQUFTa0ssYUFBWCxFQUEwQkgsS0FBMUIsQ0FBZ0MsU0FBaEM7QUFDQTs7OzJCQUVRO0FBQUE7O0FBQ1IsT0FBSTNOLFFBQVEsS0FBSzRELEdBQUwsQ0FBUzVELEtBQVQsQ0FBZWlHLEdBQWYsQ0FBbUI7QUFBQSxXQUFRLG9CQUFDLElBQUQsSUFBTSxPQUFTdUksS0FBS2pGLEtBQXBCLEVBQTJCLEtBQU9pRixLQUFLek0sR0FBdkMsRUFBNEMsUUFBVXlNLEtBQUtDLE1BQTNELEVBQW1FLEtBQU8sUUFBSzdLLEdBQS9FLEdBQVI7QUFBQSxJQUFuQixDQUFaO0FBQ0EsT0FBSW9HLGdCQUFKOztBQUVBLE9BQUksS0FBSzZELFFBQUwsSUFBaUIsTUFBckIsRUFBNkI7QUFDNUI3RCxjQUFVLGFBQVY7QUFDQSxJQUZELE1BRU87QUFDTkEsY0FBVSxNQUFWO0FBQ0E7O0FBRUQsT0FBSSxDQUFDLEtBQUtwRyxHQUFMLENBQVN2RCxLQUFULENBQWVhLFFBQXBCLEVBQThCO0FBQzdCLFFBQUksS0FBSzJNLFFBQUwsSUFBaUIsTUFBckIsRUFBNkI7QUFDNUI3RCxnQkFBVyxtQkFBWDtBQUNBLEtBRkQsTUFFTztBQUNOQSxnQkFBVyxZQUFYO0FBQ0E7QUFDRDs7QUFFRCxPQUFJLEtBQUs2RCxRQUFMLElBQWlCLEtBQUtqSyxHQUFMLENBQVN2RCxLQUFULENBQWVZLElBQXBDLEVBQTBDO0FBQ3pDLFdBQ0MsZ0NBREQ7QUFHQTs7QUFFRCxPQUFJcUIsVUFBSixFQUFnQjtBQUNmLFdBQ0MsZ0NBREQ7QUFHQTs7QUFFRCxVQUNDO0FBQUE7QUFBQSxNQUFLLFdBQWEwSCxPQUFsQjtBQUNDLG9DQUFRLFdBQVksYUFBcEIsRUFBa0MsU0FBVyxLQUFLNEQsSUFBbEQsR0FERDtBQUVDO0FBQUE7QUFBQSxPQUFRLFdBQVksZ0NBQXBCO0FBQ0M7QUFERCxLQUZEO0FBS0M7QUFBQTtBQUFBLE9BQVEsV0FBWSxnQ0FBcEI7QUFDQztBQURELEtBTEQ7QUFRQztBQUFBO0FBQUEsT0FBSyxXQUFZLGFBQWpCLEVBQStCLEtBQU8sS0FBS0QsS0FBM0M7QUFDRTNOO0FBREY7QUFSRCxJQUREO0FBY0E7Ozs7RUFwR2tCK0MsTUFBTUMsUzs7QUF1RzFCLFNBQVMwTCxJQUFULENBQWNuUCxLQUFkLEVBQXFCO0FBQ3BCLEtBQUlhLGVBQWViLE1BQU1xRSxHQUFOLENBQVV4RCxZQUE3QjtBQUNBLEtBQUltSixRQUFRbkosZUFBZSxPQUFmLEdBQXlCYixNQUFNZ0ssS0FBM0M7QUFDQSxLQUFJUyxVQUFVLEVBQWQ7O0FBRUEsS0FBSXpLLE1BQU1rUCxNQUFWLEVBQWtCO0FBQ2pCekUsWUFBVSxRQUFWO0FBQ0E7O0FBRUQsUUFDQztBQUFBO0FBQUEsSUFBSyxXQUFZLFlBQWpCO0FBQ0M7QUFBQTtBQUFBLEtBQUssV0FBWSxrQkFBakI7QUFDQztBQUFBO0FBQUEsTUFBRyxNQUFRekssTUFBTXdDLEdBQU4sSUFBYSxHQUF4QixFQUE2QixRQUFPLFFBQXBDLEVBQTZDLFdBQWFpSSxPQUExRDtBQUNDLGlDQUFLLEtBQU9ULEtBQVo7QUFERDtBQUREO0FBREQsRUFERDtBQVNBOztBQUVELFNBQVNqSCxRQUFULEdBQW9CO0FBQ25CLEtBQUlxTSxTQUFTLHFCQUFiOztBQUVBLFFBQU85TSxPQUFPK00sVUFBUCxDQUFrQkQsTUFBbEIsRUFBMEJFLE9BQWpDO0FBQ0E7O0FBRURDLFNBQVNDLE1BQVQsQ0FBZ0Isb0JBQUMsR0FBRCxPQUFoQixFQUF5QmxNLFNBQVM2QyxjQUFULENBQXdCLEtBQXhCLENBQXpCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJjbGFzcyBBcHAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG5cdGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcblx0XHRzdXBlcihwcm9wcyk7XHJcblx0XHR0aGlzLnNob3dQb3B1cCA9IHRoaXMuc2hvd1BvcHVwLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmhpZGVQb3B1cCA9IHRoaXMuaGlkZVBvcHVwLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLnRvZ2dsZUxlZ2VuZCA9IHRoaXMudG9nZ2xlTGVnZW5kLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLnJlc2l6ZUFwcCA9IHRoaXMucmVzaXplQXBwLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmhpZGVCeUNsaWNrQXJlYSBcdD0gdGhpcy5oaWRlQnlDbGlja0FyZWEuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuZGl2aXNpb25zICA9IGRpdmlzaW9ucztcclxuXHRcdHRoaXMuY2F0ZWdvcmllcyA9IGNhdGVnb3JpZXM7XHJcblx0XHR0aGlzLmxvZ29zID0gbG9nb3M7XHJcblx0XHR0aGlzLnBvcHVwQ29tcGFueSA9IHt9O1xyXG5cdFx0dGhpcy5wb3B1cENhdGVnb3J5ID0ge307XHJcblx0XHR0aGlzLnBvcHVwRGl2aXNpb24gPSB7fTtcclxuXHRcdHRoaXMuYmFzZUltYWdlRGlyID0gJy9pbWdzLydcclxuXHJcblx0XHR0aGlzLnN0YXRlID0ge1xyXG5cdFx0XHRjb21wYW5pZXM6IGNvbXBhbmllcyxcclxuXHRcdFx0em9vbTogMC43LFxyXG5cdFx0XHRuZXdab29tOiAwLjcsXHJcblx0XHRcdGxlZnQ6IDAsXHJcblx0XHRcdHRvcDogMCxcclxuXHRcdFx0c2hvd1BvcHVwOiBmYWxzZSxcclxuXHRcdFx0c2hvd01hcDogZmFsc2UsXHJcblx0XHRcdHNob3dEaXZpc2lvbnM6IGZhbHNlLFxyXG5cdFx0XHRzaG93TW9iaWxlRGl2aXNpb25zOiB0cnVlLFxyXG5cdFx0XHRjdXJyZW50RGl2aXNpb246IG51bGwsXHJcblx0XHRcdGN1cnJlbnRDYXQ6IDAsXHJcblx0XHRcdHNob3dMZWdlbmQ6IGZhbHNlLFxyXG5cdFx0XHRtb2RlOiAnbWFwJyxcclxuXHRcdFx0c2hvd0xvZ286IHRydWUsXHJcblx0XHRcdHNob3dTZWFyY2g6IGZhbHNlXHJcblx0XHR9O1xyXG5cdH1cclxuXHJcblx0c2hvd1BvcHVwKGUsIGNvbXBhbnkpIHtcclxuXHRcdGxldCBpZDtcclxuXHRcdGxldCBjb21wYW5pZXMgPSB0aGlzLnN0YXRlLmNvbXBhbmllcztcclxuXHJcblx0XHRpZiAoZSkge1xyXG5cdFx0XHRpZCA9IGUuY3VycmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGlkID0gY29tcGFueTtcclxuXHRcdH1cclxuXHJcblx0XHRmb3IgKGxldCBpID0gMDsgaSA8IGNvbXBhbmllcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAoY29tcGFuaWVzW2ldLmlkID09IGlkICYmICFjb21wYW5pZXNbaV0uZmF2b3JpdGUpIHtcclxuXHRcdFx0XHR0aGlzLnBvcHVwQ29tcGFueSA9IGNvbXBhbmllc1tpXTtcclxuXHJcblx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0XHRzaG93UG9wdXA6IHRydWUsXHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9IGVsc2UgaWYgKGNvbXBhbmllc1tpXS5pZCA9PSBpZCAmJiBjb21wYW5pZXNbaV0uZmF2b3JpdGUpIHtcclxuXHJcblx0XHRcdFx0aWYgKHRoaXMuc3RhdGUubW9kZSA9PSAnbGlzdCcpIHtcclxuXHRcdFx0XHRcdHdpbmRvdy5vcGVuKGNvbXBhbmllc1tpXS51cmwpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRpZiAoY29tcGFuaWVzW2ldLnNob3dDYXB0aW9uKSB7XHJcblx0XHRcdFx0XHRcdHdpbmRvdy5vcGVuKGNvbXBhbmllc1tpXS51cmwpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0Y29tcGFuaWVzW2ldLnNob3dDYXB0aW9uID0gIWNvbXBhbmllc1tpXS5zaG93Q2FwdGlvbjtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdFx0Y29tcGFuaWVzOiBjb21wYW5pZXNcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRpZiAodGhpcy5wb3B1cENvbXBhbnkuaWQpIHtcclxuXHRcdFx0dGhpcy5wb3B1cENhdGVnb3J5ID0gZ2V0SXRlbUJ5SWQodGhpcy5jYXRlZ29yaWVzLCB0aGlzLnBvcHVwQ29tcGFueS5jYXRlZ29yeUlkKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHRoaXMucG9wdXBDYXRlZ29yeSA9IHt9O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICh0aGlzLnBvcHVwQ2F0ZWdvcnkpIHtcclxuXHRcdFx0dGhpcy5wb3B1cERpdmlzaW9uID0gZ2V0SXRlbUJ5SWQodGhpcy5kaXZpc2lvbnMsIHRoaXMucG9wdXBDYXRlZ29yeS5kaXZpc2lvbklkKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHRoaXMucG9wdXBEaXZpc2lvbiA9IHt9O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChlKSB7XHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRcdH1cclxuXHJcblx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0c2hvd0xlZ2VuZDogZmFsc2VcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0aGlkZVBvcHVwKCkge1xyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7c2hvd1BvcHVwOiBmYWxzZX0pO1xyXG5cdH1cclxuXHJcblx0dG9nZ2xlTGVnZW5kKCkge1xyXG5cdFx0bGV0IHNob3cgPSB0aGlzLnN0YXRlLnNob3dMZWdlbmQ7XHJcblxyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdHNob3dMZWdlbmQ6ICFzaG93XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdHJlc2l6ZUFwcCgpIHtcclxuXHRcdGlmIChpc01vYmlsZSgpKSB7XHJcblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdHNob3dNYXA6IGZhbHNlLFxyXG5cdFx0XHRcdHNob3dEaXZpc2lvbnM6IGZhbHNlLFxyXG5cdFx0XHRcdHNob3dNb2JpbGVEaXZpc2lvbnM6IHRydWVcclxuXHRcdFx0fSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRzaG93TWFwOiB0cnVlLFxyXG5cdFx0XHRcdHNob3dEaXZpc2lvbnM6IHRydWUsXHJcblx0XHRcdFx0c2hvd01vYmlsZURpdmlzaW9uczogZmFsc2VcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRoaWRlQnlDbGlja0FyZWEoZSkge1xyXG5cdFx0bGV0IGN1cnJlbnRFbGVtID0gZS50YXJnZXQ7XHJcblx0XHRsZXQgY2xhc3NOYW1lO1xyXG5cclxuXHRcdHRyeSB7XHJcblx0XHRcdGRvIHtcclxuXHRcdFx0XHRjbGFzc05hbWUgPSBjdXJyZW50RWxlbS5jbGFzc05hbWU7XHJcblxyXG5cdFx0XHRcdGlmIChjbGFzc05hbWUpIHtcclxuXHRcdFx0XHRcdGlmIChjbGFzc05hbWUuaW5kZXhPZignc2VhcmNoJykgIT0gLTEpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGlmIChjbGFzc05hbWUuaW5kZXhPZignbGVnZW5kJykgIT0gLTEpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH0gd2hpbGUgKGN1cnJlbnRFbGVtID0gY3VycmVudEVsZW0ucGFyZW50Tm9kZSk7XHJcblx0XHR9IGNhdGNoIChlKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHJcblx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0c2hvd1NlYXJjaDogZmFsc2UsXHJcblx0XHRcdHNob3dMZWdlbmQ6IGZhbHNlXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGNvbXBvbmVudFdpbGxNb3VudCgpIHtcclxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLnJlc2l6ZUFwcCwgZmFsc2UpO1xyXG5cdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLmhpZGVCeUNsaWNrQXJlYSwgZmFsc2UpO1xyXG5cdFx0dGhpcy5yZXNpemVBcHAoKTtcclxuXHR9XHJcblxyXG5cdGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xyXG5cdFx0d2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMucmVzaXplQXBwLCBmYWxzZSk7XHJcblx0XHRkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGlkZUJ5Q2xpY2tBcmVhLCBmYWxzZSk7XHJcblx0fVxyXG5cclxuXHRyZW5kZXIoKSB7XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2PlxyXG5cdFx0XHRcdDxIZWFkZXIgXHRcdFx0YXBwID0ge3RoaXN9IC8+XHJcblx0XHRcdFx0PE1hcCBcdFx0XHRcdGFwcCA9IHt0aGlzfSAvPlxyXG5cdFx0XHRcdDxEaXZpc2lvbnMgXHRcdFx0YXBwID0ge3RoaXN9IC8+XHJcblx0XHRcdFx0PFBvcHVwIFx0XHRcdFx0YXBwID0ge3RoaXN9IC8+XHJcblx0XHRcdFx0PE1vYmlsZURpdmlzaW9ucyBcdGFwcCA9IHt0aGlzfSAvPlxyXG5cdFx0XHRcdDxDb250cm9sIFx0XHRcdGFwcCA9IHt0aGlzfSAvPlxyXG5cdFx0XHRcdDxMZWdlbmQgXHRcdFx0YXBwID0ge3RoaXN9IC8+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0KTtcclxuXHR9XHJcbn07XHJcblxyXG5jbGFzcyBIZWFkZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG5cdGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcblx0XHRzdXBlcihwcm9wcyk7XHJcblx0XHR0aGlzLkNoYW5nZSBcdFx0XHQ9IHRoaXMuQ2hhbmdlLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLkVudGVyIFx0XHRcdFx0PSB0aGlzLkVudGVyLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLkxlYXZlIFx0XHRcdFx0PSB0aGlzLkxlYXZlLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLkl0ZW1DbGljayBcdFx0XHQ9IHRoaXMuSXRlbUNsaWNrLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLkl0ZW1MZWF2ZSBcdFx0XHQ9IHRoaXMuSXRlbUxlYXZlLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLkNsaWNrIFx0XHRcdFx0PSB0aGlzLkNsaWNrLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmZvY3VzIFx0XHRcdFx0PSB0aGlzLmZvY3VzLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmZpbHRlckNvbXBhbmllcyBcdD0gdGhpcy5maWx0ZXJDb21wYW5pZXMuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuZ29Ub0RpdmlzaW9uIFx0XHQ9IHRoaXMuZ29Ub0RpdmlzaW9uLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmNsZWFyIFx0XHRcdFx0PSB0aGlzLmNsZWFyLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmFwcCBcdFx0XHRcdD0gcHJvcHMuYXBwO1xyXG5cclxuXHRcdHRoaXMuc3RhdGUgPSB7XHJcblx0XHRcdHNlYXJjaDogJycsXHJcblx0XHRcdHNlYXJjaExpc3Q6IFtdLFxyXG5cdFx0XHRzZWFyY2hQbGFjZWhvbGRlcjogJycsXHJcblx0XHRcdHNob3dTZWFyY2hMaXN0OiBmYWxzZVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Q2hhbmdlKGUpIHtcclxuXHRcdGxldCBzZWFyY2hMaXN0ID0gdGhpcy5maWx0ZXJDb21wYW5pZXMoZS50YXJnZXQudmFsdWUpO1xyXG5cdFx0bGV0IGNvbXBhbmllcyA9IHRoaXMuYXBwLnN0YXRlLmNvbXBhbmllcztcclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRzZWFyY2g6IGUudGFyZ2V0LnZhbHVlLFxyXG5cdFx0XHRzZWFyY2hMaXN0OiBzZWFyY2hMaXN0LFxyXG5cdFx0XHRzZWFyY2hQbGFjZWhvbGRlcjogJycsXHJcblx0XHRcdHNob3dTZWFyY2hMaXN0OiB0cnVlXHJcblx0XHR9KTtcclxuXHJcblx0XHRpZiAoZS50YXJnZXQudmFsdWUubGVuZ3RoID09IDApIHtcclxuXHRcdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBjb21wYW5pZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRjb21wYW5pZXNbaV0uc2hvd0NpcmNsZSA9IGZhbHNlO1xyXG5cdFx0XHRcdGNvbXBhbmllc1tpXS5zaG93Q2FwdGlvbiA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0Y29tcGFuaWVzOiBjb21wYW5pZXNcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRFbnRlcihlKSB7XHJcblx0XHRsZXQgaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcclxuXHRcdGxldCBjb21wYW5pZXMgPSB0aGlzLmFwcC5zdGF0ZS5jb21wYW5pZXM7XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBjb21wYW5pZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0aWYgKGlkID09IGNvbXBhbmllc1tpXS5pZCkge1xyXG5cdFx0XHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdFx0c2VhcmNoUGxhY2Vob2xkZXI6IGNvbXBhbmllc1tpXS5uYW1lLFxyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRjb21wYW5pZXNbaV0uc2hvd0NpcmNsZSA9IHRydWU7XHJcblxyXG5cdFx0XHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdGNvbXBhbmllczogY29tcGFuaWVzXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdExlYXZlKCkge1xyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7c2VhcmNoUGxhY2Vob2xkZXI6ICcnfSk7XHJcblx0fVxyXG5cclxuXHRDbGljayhlKSB7XHJcblx0XHRsZXQgc2hvdyA9ICF0aGlzLmFwcC5zdGF0ZS5zaG93U2VhcmNoO1xyXG5cdFx0dGhpcy5hcHAuc2V0U3RhdGUoe3Nob3dTZWFyY2g6IHNob3d9KTtcclxuXHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0XHRpZiAoc2hvdyAmJiB0aGlzLnN0YXRlLnNlYXJjaCkge1xyXG5cdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRzaG93U2VhcmNoTGlzdDogdHJ1ZVxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEl0ZW1DbGljayhlKSB7XHJcblx0XHRsZXQgaWQgXHRcdFx0PSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKSB8fCBlLnRhcmdldC5wYXJlbnROb2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1pZCcpO1xyXG5cdFx0bGV0IGNvbXBhbmllcyBcdD0gdGhpcy5hcHAuc3RhdGUuY29tcGFuaWVzO1xyXG5cdFx0bGV0IGNvbnRIYWxmV2lkdGg7XHJcblx0XHRsZXQgY29udEhhbGZIZWlnaHQ7XHJcblx0XHRsZXQgem9vbSA9IHRoaXMuYXBwLnN0YXRlLnpvb207XHJcblx0XHRsZXQgbGVmdDtcclxuXHRcdGxldCB0b3A7XHJcblxyXG5cdFx0aWYgKHRoaXMuYXBwLmNvbnRhaW5lcikge1xyXG5cdFx0XHRjb250SGFsZldpZHRoID0gdGhpcy5hcHAuY29udGFpbmVyLmNsaWVudFdpZHRoIC8gMjtcclxuXHRcdFx0Y29udEhhbGZIZWlnaHQgPSB0aGlzLmFwcC5jb250YWluZXIuY2xpZW50SGVpZ2h0IC8gMjtcclxuXHRcdH0gZWxzZSAge1xyXG5cdFx0XHRjb250SGFsZldpZHRoID0gMDtcclxuXHRcdFx0Y29udEhhbGZIZWlnaHQgPSAwO1xyXG5cdFx0fVxyXG5cclxuXHRcdGZvciAobGV0IGkgPSAwOyBpIDwgY29tcGFuaWVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGlmIChjb21wYW5pZXNbaV0uaWQgPT0gaWQpIHtcclxuXHRcdFx0XHRsZWZ0ID0gY29udEhhbGZXaWR0aCAvIHpvb20gLSBjb21wYW5pZXNbaV0ucG9zaXRpb24ubGVmdDtcclxuXHRcdFx0XHR0b3AgPSBjb250SGFsZkhlaWdodCAvIHpvb20gLSBjb21wYW5pZXNbaV0ucG9zaXRpb24udG9wO1xyXG5cclxuXHRcdFx0XHRpZiAoaXNNb2JpbGUoKSkge1xyXG5cdFx0XHRcdFx0Zm9yIChsZXQgaiA9IDA7IGogPCB0aGlzLmFwcC5jYXRlZ29yaWVzLmxlbmd0aDsgaisrKSB7XHJcblx0XHRcdFx0XHRcdGlmIChjb21wYW5pZXNbaV0uY2F0ZWdvcnlJZCA9PSB0aGlzLmFwcC5jYXRlZ29yaWVzW2pdLmlkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0Zm9yIChsZXQgayA9IDA7IGsgPCB0aGlzLmFwcC5kaXZpc2lvbnMubGVuZ3RoOyBrKyspIHtcclxuXHRcdFx0XHRcdFx0XHRcdGlmICh0aGlzLmFwcC5jYXRlZ29yaWVzW2pdLmRpdmlzaW9uSWQgPT0gdGhpcy5hcHAuZGl2aXNpb25zW2tdLmlkKSB7XHJcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuYXBwLnBvcHVwQ29tcGFueSA9IGNvbXBhbmllc1tpXTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0dGhpcy5hcHAuc2V0U3RhdGUoe2N1cnJlbnREaXZpc2lvbjogdGhpcy5hcHAuZGl2aXNpb25zW2tdLmlkfSk7XHJcblx0XHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRpZiAodGhpcy5hcHAuc3RhdGUubW9kZSA9PSAnbGlzdCcpIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5hcHAuc2hvd1BvcHVwKGZhbHNlLCBjb21wYW5pZXNbaV0uaWQpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHJcblx0XHRcdFx0XHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdFx0XHRsZWZ0OiBsZWZ0LFxyXG5cdFx0XHRcdFx0XHRcdHRvcDogIHRvcCxcclxuXHRcdFx0XHRcdFx0XHR6b29tOiB6b29tXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Y29tcGFuaWVzW2ldLnNob3dDaXJjbGUgID0gdHJ1ZTtcclxuXHRcdFx0XHRjb21wYW5pZXNbaV0uc2hvd0NhcHRpb24gPSB0cnVlO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGNvbXBhbmllc1tpXS5zaG93Q2lyY2xlICA9IGZhbHNlO1xyXG5cdFx0XHRcdGNvbXBhbmllc1tpXS5zaG93Q2FwdGlvbiA9IGZhbHNlO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5hcHAuc3RhdGUuY3VycmVudENvbXBhbnkgPSBpZDtcclxuXHJcblx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7XHJcblx0XHRcdGNvbXBhbmllczogY29tcGFuaWVzXHJcblx0XHR9KTtcclxuXHJcblx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0c2hvd1NlYXJjaExpc3Q6IGZhbHNlXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdEl0ZW1MZWF2ZShlKSB7XHJcblx0XHRsZXQgaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcclxuXHRcdGxldCBjb21wYW5pZXMgPSB0aGlzLmFwcC5zdGF0ZS5jb21wYW5pZXM7XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBjb21wYW5pZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0aWYgKGNvbXBhbmllc1tpXS5pZCA9PSBpZCkge1xyXG5cdFx0XHRcdGNvbXBhbmllc1tpXS5zaG93Q2lyY2xlID0gZmFsc2U7XHJcblxyXG5cdFx0XHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdGNvbXBhbmllczogY29tcGFuaWVzXHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRmaWx0ZXJDb21wYW5pZXModmFsdWUpIHtcclxuXHRcdGxldCBsaXN0IFx0XHQ9IFtdO1xyXG5cdFx0bGV0IGkgXHRcdFx0PSAwO1xyXG5cdFx0bGV0IGNvdW50IFx0XHQ9IDA7XHJcblx0XHRsZXQgY29tcGFuaWVzIFx0PSB0aGlzLmFwcC5zdGF0ZS5jb21wYW5pZXM7XHJcblx0XHRsZXQgY2F0ZWdvcmllcyBcdD0gdGhpcy5hcHAuY2F0ZWdvcmllcztcclxuXHJcblx0XHR3aGlsZSAodmFsdWUubGVuZ3RoICYmIGkgPCBjb21wYW5pZXMubGVuZ3RoICYmIGNvdW50IDwgMTApIHtcclxuXHRcdFx0aWYgKGNvbXBhbmllc1tpXS5uYW1lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZih2YWx1ZS50b0xvd2VyQ2FzZSgpKSA9PSAwKSB7XHJcblx0XHRcdFx0bGlzdC5wdXNoKGNvbXBhbmllc1tpXSk7XHJcblx0XHRcdFx0Y291bnQrKztcclxuXHRcdFx0fVxyXG5cdFx0XHRpKys7XHJcblx0XHR9XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGZvciAobGV0IGogPSAwOyBqIDwgY2F0ZWdvcmllcy5sZW5ndGg7IGorKykge1xyXG5cdFx0XHRcdGlmIChsaXN0W2ldLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcmllc1tqXS5pZCkge1xyXG5cdFx0XHRcdFx0bGlzdFtpXS5jYXRlZ29yeU5hbWUgPSBjYXRlZ29yaWVzW2pdLm5hbWVSdTtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiBsaXN0O1xyXG5cdH1cclxuXHJcblx0Z29Ub0RpdmlzaW9uKGUpIHtcclxuXHRcdGxldCBpZCA9IGUuY3VycmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcclxuXHRcdGxldCBtb2RlID0gdGhpcy5hcHAuc3RhdGUubW9kZTtcclxuXHRcdGxldCB6b29tID0gdGhpcy5hcHAuc3RhdGUuem9vbTtcclxuXHRcdGxldCBjb250YWluZXIgPSB0aGlzLmFwcC5jb250YWluZXI7XHJcblx0XHRsZXQgZGl2aXNpb24gPSB0aGlzLmFwcC5kaXZpc2lvbnM7XHJcblx0XHRsZXQgY2F0ZWdvcmllcyA9IHRoaXMuYXBwLmNhdGVnb3JpZXM7XHJcblx0XHRsZXQgY29tcGFuaWVzID0gdGhpcy5hcHAuc3RhdGUuY29tcGFuaWVzO1xyXG5cdFx0bGV0IGNvbnRIYWxmV2lkdGg7XHJcblx0XHRsZXQgY29udEhhbGZIZWlnaHQ7XHJcblx0XHRsZXQgbGVmdDtcclxuXHRcdGxldCB0b3A7XHJcblx0XHRsZXQgZGl2aXNpb25FO1xyXG5cdFx0bGV0IG9mZnNldFRvcDtcclxuXHRcdGxldCBoZWFkZXJFO1xyXG5cdFx0bGV0IGhlYWRlckhlaWdodDtcclxuXHRcdGxldCBuYXZFID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmhlYWRlcl9fbmF2LWE6bm90KC5oZWFkZXJfX25hdi1hX3NlYXJjaCknKTtcclxuXHJcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0aWYgKG1vZGUgPT0gJ2xpc3QnKSB7XHJcblx0XHRcdGRpdmlzaW9uRSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdkaXZpc2lvbi0nICsgaWQpO1xyXG5cdFx0XHRoZWFkZXJFID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhlYWRlcicpO1xyXG5cdFx0XHRvZmZzZXRUb3AgPSBkaXZpc2lvbkUub2Zmc2V0VG9wO1xyXG5cdFx0XHRoZWFkZXJIZWlnaHQgPSBoZWFkZXJFLmNsaWVudEhlaWdodDtcclxuXHRcdFx0b2Zmc2V0VG9wIC09IGhlYWRlckhlaWdodDtcclxuXHJcblx0XHRcdHdpbmRvdy5zY3JvbGwoe1xyXG5cdFx0XHRcdHRvcDogb2Zmc2V0VG9wLFxyXG5cdFx0XHRcdGxlZnQ6IDAsXHJcblx0XHRcdFx0YmVoYXZpb3I6ICdzbW9vdGgnXHJcblx0XHRcdH0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Y29udEhhbGZXaWR0aCA9IGNvbnRhaW5lci5jbGllbnRXaWR0aCAvIDI7XHJcblx0XHRcdGNvbnRIYWxmSGVpZ2h0ID0gY29udGFpbmVyLmNsaWVudEhlaWdodCAvIDI7XHJcblxyXG5cdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IGRpdmlzaW9ucy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChkaXZpc2lvbnNbaV0uaWQgPT0gaWQpIHtcclxuXHRcdFx0XHRcdGxlZnQgPSBkaXZpc2lvbnNbaV0ucG9zaXRpb24ubGVmdCAqIC0xO1xyXG5cdFx0XHRcdFx0dG9wID0gZGl2aXNpb25zW2ldLnBvc2l0aW9uLnRvcCAqIC0xO1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRmb3IgKGxldCBpID0gMDsgaSA8IGNhdGVnb3JpZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAoY2F0ZWdvcmllc1tpXS5kaXZpc2lvbklkID09IGlkKSB7XHJcblx0XHRcdFx0XHRmb3IgKGxldCBqID0gMDsgaiA8IGNvbXBhbmllcy5sZW5ndGg7IGorKykge1xyXG5cdFx0XHRcdFx0XHRpZiAoY29tcGFuaWVzW2pdLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcmllc1tpXS5pZCkge1xyXG5cdFx0XHRcdFx0XHRcdGNvbXBhbmllc1tqXS5zaG93Q2lyY2xlID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0Y29tcGFuaWVzW2pdLnNob3dDYXB0aW9uID0gdHJ1ZTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRmb3IgKGxldCBqID0gMDsgaiA8IGNvbXBhbmllcy5sZW5ndGg7IGorKykge1xyXG5cdFx0XHRcdFx0XHRpZiAoY29tcGFuaWVzW2pdLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcmllc1tpXS5pZCkge1xyXG5cdFx0XHRcdFx0XHRcdGNvbXBhbmllc1tqXS5zaG93Q2lyY2xlID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0Y29tcGFuaWVzW2pdLnNob3dDYXB0aW9uID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0XHR0b3A6IHRvcCxcclxuXHRcdFx0XHRsZWZ0OiBsZWZ0LFxyXG5cdFx0XHRcdGNvbXBhbmllczogY29tcGFuaWVzLFxyXG5cdFx0XHRcdGN1cnJlbnREaXZpc2lvbjogaWRcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBuYXZFLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdG5hdkVbaV0uY2xhc3NMaXN0LnJlbW92ZSgnaGVhZGVyX19uYXYtYV9jdXJyZW50Jyk7XHJcblx0XHR9XHJcblxyXG5cdFx0ZS50YXJnZXQuY2xhc3NMaXN0LmFkZCgnaGVhZGVyX19uYXYtYV9jdXJyZW50Jyk7XHJcblx0fVxyXG5cclxuXHRmb2N1cygpIHtcclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRzaG93U2VhcmNoTGlzdDogdHJ1ZVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRjbGVhcigpIHtcclxuXHRcdGxldCBjb21wYW5pZXMgPSB0aGlzLmFwcC5zdGF0ZS5jb21wYW5pZXM7XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBjb21wYW5pZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0Y29tcGFuaWVzW2ldLnNob3dDaXJjbGUgPSBmYWxzZTtcclxuXHRcdFx0Y29tcGFuaWVzW2ldLnNob3dDYXB0aW9uID0gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdHNlYXJjaDogJycsXHJcblx0XHRcdHNob3dTZWFyY2hMaXN0OiBmYWxzZSxcclxuXHRcdFx0c2VhcmNoTGlzdDogW11cclxuXHRcdH0pO1xyXG5cclxuXHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0Y29tcGFuaWVzOiBjb21wYW5pZXNcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0cmVuZGVyKCkge1xyXG5cdFx0bGV0IGRpdmlzaW9ucyA9IHRoaXMuYXBwLmRpdmlzaW9ucztcclxuXHRcdGRpdmlzaW9ucyA9IGRpdmlzaW9ucy5tYXAoZGl2aXNpb24gPT4ge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxhIGhyZWYgPSBcIiNcIiBjbGFzc05hbWUgPSBcImhlYWRlcl9fbmF2LWFcIiBkYXRhLWlkID0ge2RpdmlzaW9uLmlkfSBvbkNsaWNrID0ge3RoaXMuZ29Ub0RpdmlzaW9ufSA+XHJcblx0XHRcdFx0XHR7ZGl2aXNpb24ubmFtZX1cclxuXHRcdFx0XHQ8L2E+XHJcblx0XHRcdCk7XHJcblx0XHR9KTtcclxuXHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8aGVhZGVyIGNsYXNzTmFtZSA9IFwiaGVhZGVyXCI+XHJcblx0XHRcdFx0PGEgaHJlZj1cIi9cIiBjbGFzc05hbWUgPSBcImhlYWRlcl9fbG9nb1wiPiZuYnNwOzwvYT5cclxuXHRcdFx0XHQ8bmF2IGNsYXNzTmFtZSA9IFwiaGVhZGVyX19uYXZcIj5cclxuXHRcdFx0XHRcdHtkaXZpc2lvbnN9XHJcblx0XHRcdFx0XHQ8YSBocmVmPVwiI1wiIGNsYXNzTmFtZSA9IFwiaGVhZGVyX19uYXYtYSBoZWFkZXJfX25hdi1hX3NlYXJjaFwiIG9uQ2xpY2sgPSB7dGhpcy5DbGlja30gPiZuYnNwOzwvYT5cclxuXHRcdFx0XHRcdDxTZWFyY2hcclxuXHRcdFx0XHRcdFx0c2hvdyA9IHt0aGlzLmFwcC5zdGF0ZS5zaG93U2VhcmNofVxyXG5cdFx0XHRcdFx0XHRzaG93TGlzdCA9IHt0aGlzLnN0YXRlLnNob3dTZWFyY2hMaXN0fVxyXG5cdFx0XHRcdFx0XHR2YWx1ZSA9IHt0aGlzLnN0YXRlLnNlYXJjaH0gXHJcblx0XHRcdFx0XHRcdGxpc3QgPSB7dGhpcy5zdGF0ZS5zZWFyY2hMaXN0fVxyXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlciA9IHt0aGlzLnN0YXRlLnNlYXJjaFBsYWNlaG9sZGVyfVxyXG5cdFx0XHRcdFx0XHRjaGFuZ2UgPSB7dGhpcy5DaGFuZ2V9XHJcblx0XHRcdFx0XHRcdGZvY3VzID0ge3RoaXMuZm9jdXN9XHJcblx0XHRcdFx0XHRcdGVudGVyID0ge3RoaXMuRW50ZXJ9XHJcblx0XHRcdFx0XHRcdGxlYXZlID0ge3RoaXMuTGVhdmV9XHJcblx0XHRcdFx0XHRcdGNsaWNrID0ge3RoaXMuSXRlbUNsaWNrfVxyXG5cdFx0XHRcdFx0XHRpdGVtbGVhdmUgPSB7dGhpcy5JdGVtTGVhdmV9XHJcblx0XHRcdFx0XHRcdGNsZWFyID0ge3RoaXMuY2xlYXJ9XHJcblx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdDwvbmF2PlxyXG5cdFx0XHQ8L2hlYWRlcj5cclxuXHRcdCk7XHJcblx0fVxyXG59O1xyXG5cclxuZnVuY3Rpb24gU2VhcmNoKHByb3BzKSB7XHJcblx0bGV0IHBsYWNlaG9sZGVyID0gcHJvcHMucGxhY2Vob2xkZXI7XHJcblx0bGV0IHZhbHVlIFx0XHQ9IHByb3BzLnZhbHVlO1xyXG5cdGxldCBzdHlsZSBcdFx0PSB7ZGlzcGxheTogcHJvcHMuc2hvdyA/ICdibG9jaycgOiAnbm9uZSd9O1xyXG5cdGxldCBjb3VudCBcdFx0PSBwcm9wcy5saXN0Lmxlbmd0aDtcclxuXHRsZXQgbGlzdENsYXNzZXMgPSAnc2VhcmNoX19saXN0JztcclxuXHRsZXQgY2xlYXJDbGFzc2VzID0gJ3NlYXJjaF9fY2xlYXInO1xyXG5cdGxldCBsaXN0IFx0XHQ9IHByb3BzLmxpc3QubWFwKGNvbXBhbnkgPT4ge1xyXG5cdFx0bGV0IG5hbWUgXHQ9IGNvbXBhbnkubmFtZS5zdWJzdHIodmFsdWUubGVuZ3RoKTtcclxuXHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8YnV0dG9uXHJcblx0XHRcdFx0Y2xhc3NOYW1lID0gXCJzZWFyY2hfX2xpc3QtaXRlbVwiXHJcblx0XHRcdFx0ZGF0YS1pZCA9IHtjb21wYW55LmlkfVxyXG5cdFx0XHRcdG9uTW91c2VFbnRlciA9IHtwcm9wcy5lbnRlcn1cclxuXHRcdFx0XHRvbkNsaWNrID0ge3Byb3BzLmNsaWNrfVxyXG5cdFx0XHRcdG9uTW91c2VMZWF2ZSA9IHtwcm9wcy5pdGVtbGVhdmV9XHJcblx0XHRcdD5cclxuXHRcdFx0XHQ8c3BhbiBjbGFzc05hbWUgPSBcInNlYXJjaF9fbGlzdC1pdGVtLWNvbXBhbnlcIiA+XHJcblx0XHRcdFx0XHQ8Yj57dmFsdWV9PC9iPntuYW1lfVxyXG5cdFx0XHRcdDwvc3Bhbj5cclxuXHRcdFx0XHQ8c3BhbiBjbGFzc05hbWUgPSBcInNlYXJjaF9fbGlzdC1pdGVtLWNhdGVnb3J5XCI+XHJcblx0XHRcdFx0XHR7Y29tcGFueS5jYXRlZ29yeU5hbWV9XHJcblx0XHRcdFx0PC9zcGFuPlxyXG5cdFx0XHQ8L2J1dHRvbj5cclxuXHRcdCk7XHJcblx0fSk7XHJcblxyXG5cdGlmICh2YWx1ZS5sZW5ndGgpIHtcclxuXHRcdHN3aXRjaCAoY291bnQgJSAxMCkge1xyXG5cdFx0XHRjYXNlIDA6XHJcblx0XHRcdGNhc2UgOTpcclxuXHRcdFx0Y2FzZSA4OlxyXG5cdFx0XHRjYXNlIDc6XHJcblx0XHRcdGNhc2UgNjpcclxuXHRcdFx0Y2FzZSA1OlxyXG5cdFx0XHRcdGNvdW50ID0gY291bnQgKyBcIiDRgNC10LfRg9C70YzRgtCw0YLQvtCyINC/0L7QuNGB0LrQsFwiO1xyXG5cdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSA0OlxyXG5cdFx0XHRjYXNlIDM6XHJcblx0XHRcdGNhc2UgMjpcclxuXHRcdFx0XHRjb3VudCA9IGNvdW50ICsgXCIg0YDQtdC30YPQu9GM0YLQsNGC0LAg0L/QvtC40YHQutCwXCI7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlIDE6XHJcblx0XHRcdFx0Y291bnQgPSBjb3VudCArIFwiINGA0LXQt9GD0LvRjNGC0LDRgiDQv9C+0LjRgdC60LBcIjtcclxuXHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblx0fSBlbHNlIHtcclxuXHRcdGNvdW50ID0gJyc7XHJcblx0fVxyXG5cclxuXHRpZiAocHJvcHMuc2hvd0xpc3QpIHtcclxuXHRcdGxpc3RDbGFzc2VzICs9ICcgc2VhcmNoX19saXN0X3Nob3cnO1xyXG5cdH1cclxuXHJcblx0aWYgKHZhbHVlLmxlbmd0aCkge1xyXG5cdFx0Y2xlYXJDbGFzc2VzICs9ICcgc2VhcmNoX19jbGVhcl9zaG93JztcclxuXHR9XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwic2VhcmNoXCIgc3R5bGUgPSB7c3R5bGV9ID5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcInNlYXJjaF9fcGxhY2Vob2xkZXJcIj5cclxuXHRcdFx0XHR7cGxhY2Vob2xkZXJ9XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwic2VhcmNoX19jb3VudFwiPlxyXG5cdFx0XHRcdHtjb3VudH1cclxuXHRcdFx0PC9kaXY+XHJcblx0XHRcdDxpbnB1dFxyXG5cdFx0XHRcdGNsYXNzTmFtZSBcdD0gXCJzZWFyY2hfX2lucHV0XCJcclxuXHRcdFx0XHR0eXBlIFx0XHQ9IFwidGV4dFwiXHJcblx0XHRcdFx0dmFsdWUgXHRcdD0ge3ZhbHVlfVxyXG5cdFx0XHRcdG9uQ2hhbmdlIFx0PSB7cHJvcHMuY2hhbmdlfVxyXG5cdFx0XHRcdG9uRm9jdXMgXHQ9IHtwcm9wcy5mb2N1c31cclxuXHRcdFx0XHRwbGFjZWhvbGRlciA9IFwi0J3QsNC50YLQuCDQutC+0LzQv9Cw0L3QuNGOXCJcclxuXHRcdFx0Lz5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSB7bGlzdENsYXNzZXN9IG9uTW91c2VMZWF2ZSA9IHtwcm9wcy5sZWF2ZX0gPntsaXN0fTwvZGl2PlxyXG5cdFx0XHQ8YnV0dG9uIGNsYXNzTmFtZSA9IHtjbGVhckNsYXNzZXN9IG9uQ2xpY2sgPSB7cHJvcHMuY2xlYXJ9ID48L2J1dHRvbj5cclxuXHRcdDwvZGl2PlxyXG5cdCk7XHJcbn1cclxuXHJcbmNsYXNzIE1hcCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcblx0Y29uc3RydWN0b3IocHJvcHMpIHtcclxuXHRcdHN1cGVyKHByb3BzKTtcclxuXHRcdHRoaXMuYXBwIFx0XHRcdD0gcHJvcHMuYXBwO1xyXG5cdFx0dGhpcy5XaGVlbCBcdFx0XHQ9IHRoaXMuV2hlZWwuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMubW91c2VEb3duIFx0XHQ9IHRoaXMubW91c2VEb3duLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLm1vdXNlVXAgXHRcdD0gdGhpcy5tb3VzZVVwLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLm1vdXNlTW92ZSBcdFx0PSB0aGlzLm1vdXNlTW92ZS5iaW5kKHRoaXMpO1xyXG5cdFx0dGhpcy5tb3VzZU91dCBcdFx0PSB0aGlzLm1vdXNlT3V0LmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmNoZWNrVG9wIFx0XHQ9IHRoaXMuY2hlY2tUb3AuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuZ2V0TWluVG9wIFx0XHQ9IHRoaXMuZ2V0TWluVG9wLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmNoZWNrTGVmdCBcdFx0PSB0aGlzLmNoZWNrTGVmdC5iaW5kKHRoaXMpO1xyXG5cdFx0dGhpcy5nZXRNaW5MZWZ0IFx0PSB0aGlzLmdldE1pbkxlZnQuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuY2F0Q2xpY2sgXHRcdD0gdGhpcy5jYXRDbGljay5iaW5kKHRoaXMpO1xyXG5cdFx0dGhpcy5hcmVhQ2xpY2sgXHRcdD0gdGhpcy5hcmVhQ2xpY2suYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuc3RhcnREcmFnIFx0XHQ9IGZhbHNlO1xyXG5cdFx0dGhpcy5vZmZzZXRMZWZ0IFx0PSAwO1xyXG5cdFx0dGhpcy5vZmZzZXRUb3AgXHRcdD0gMDtcclxuXHRcdHRoaXMuem9vbVN0ZXAgXHRcdD0gMC4wNTtcclxuXHR9XHJcblxyXG5cdFdoZWVsKGUpIHtcclxuXHRcdGxldCB6b29tIFx0XHQ9IHRoaXMuYXBwLnN0YXRlLnpvb207XHJcblx0XHRsZXQgem9vbVN0ZXAgXHQ9IHRoaXMuem9vbVN0ZXA7XHJcblx0XHRsZXQgbmV3Wm9vbTtcclxuXHJcblx0XHRpZiAoZS5kZWx0YVkgPCAwKSB7XHJcblx0XHRcdG5ld1pvb20gPSB6b29tICsgem9vbVN0ZXA7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRuZXdab29tID0gem9vbSAtIHpvb21TdGVwO1xyXG5cdFx0fVxyXG5cclxuXHRcdG5ld1pvb20gPSBuZXdab29tLnRvRml4ZWQoMikgKiAxO1xyXG5cclxuXHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0bmV3Wm9vbTogbmV3Wm9vbVxyXG5cdFx0fSk7XHJcblxyXG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdH1cclxuXHJcblx0bW91c2VEb3duKGUpIHtcclxuXHRcdHRoaXMuc3RhcnREcmFnIFx0PSB0cnVlO1xyXG5cdFx0dGhpcy5vZmZzZXRMZWZ0ID0gZS5wYWdlWCAtIHRoaXMuYXBwLnN0YXRlLmxlZnQgKiB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cdFx0dGhpcy5vZmZzZXRUb3AgXHQ9IGUucGFnZVkgLSB0aGlzLmFwcC5zdGF0ZS50b3AgICogdGhpcy5hcHAuc3RhdGUuem9vbTtcclxuXHJcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0fVxyXG5cclxuXHRtb3VzZVVwKGUpIHtcclxuXHRcdHRoaXMuc3RhcnREcmFnID0gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRtb3VzZU1vdmUoZSkge1xyXG5cdFx0bGV0IHRvcDtcclxuXHRcdGxldCBsZWZ0O1xyXG5cclxuXHRcdGlmICh0aGlzLnN0YXJ0RHJhZykge1xyXG5cdFx0XHR0b3AgIFx0PSAoZS5wYWdlWSAtIHRoaXMub2Zmc2V0VG9wKSAgLyB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cdFx0XHRsZWZ0IFx0PSAoZS5wYWdlWCAtIHRoaXMub2Zmc2V0TGVmdCkgLyB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cclxuXHRcdFx0dGhpcy5hcHAuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdHRvcDogIHRvcCxcclxuXHRcdFx0XHRsZWZ0OiBsZWZ0XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0bW91c2VPdXQoZSkge1xyXG5cdFx0dGhpcy5zdGFydERyYWcgPSBmYWxzZTtcclxuXHR9XHJcblxyXG5cdGNhdENsaWNrKGUpIHtcclxuXHRcdGxldCBjYXRJZCA9IGUudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1jYXRlZ29yeS1pZCcpIHx8IGUudGFyZ2V0LnBhcmVudE5vZGUuZ2V0QXR0cmlidXRlKCdkYXRhLWNhdGVnb3J5LWlkJyk7XHJcblx0XHRsZXQgY29tcGFuaWVzO1xyXG5cdFx0bGV0IGN1cnJlbnRDYXQgPSB0aGlzLmFwcC5zdGF0ZS5jdXJyZW50Q2F0O1xyXG5cclxuXHRcdGlmICghY2F0SWQpIHtcclxuXHRcdFx0cmV0dXJuIGZhbHNlO1xyXG5cdFx0fVxyXG5cclxuXHJcblx0XHRjb21wYW5pZXMgPSB0aGlzLmFwcC5zdGF0ZS5jb21wYW5pZXMubWFwKGNvbXBhbnkgPT4ge1xyXG5cdFx0XHRpZiAoY29tcGFueS5jYXRlZ29yeUlkICE9IGNhdElkIHx8IGNhdElkID09IGN1cnJlbnRDYXQpIHtcclxuXHRcdFx0XHRjb21wYW55LnNob3dDYXB0aW9uID0gZmFsc2U7XHJcblx0XHRcdFx0Y29tcGFueS5zaG93Q2lyY2xlID0gZmFsc2U7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0Y29tcGFueS5zaG93Q2FwdGlvbiA9IHRydWU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdHJldHVybiBjb21wYW55O1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0aWYgKGNhdElkID09IGN1cnJlbnRDYXQpIHtcclxuXHRcdFx0Y2F0SWQgPSAwO1xyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0Y29tcGFuaWVzOiBjb21wYW5pZXMsXHJcblx0XHRcdGN1cnJlbnRDYXQ6IGNhdElkXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGFyZWFDbGljayhlKSB7XHJcblx0XHRsZXQgaWQgPSBlLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKTtcclxuXHRcdGxldCBjdXJyZW50RGl2aXNpb24gPSB0aGlzLmFwcC5zdGF0ZS5jdXJyZW50RGl2aXNpb247XHJcblx0XHRsZXQgY2F0ZWdvcmllcyA9IHRoaXMuYXBwLmNhdGVnb3JpZXM7XHJcblx0XHRsZXQgY29tcGFuaWVzID0gdGhpcy5hcHAuc3RhdGUuY29tcGFuaWVzO1xyXG5cclxuXHRcdGZvciAobGV0IGkgPSAwOyBpIDwgY2F0ZWdvcmllcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAoY2F0ZWdvcmllc1tpXS5kaXZpc2lvbklkID09IGlkKSB7XHJcblx0XHRcdFx0Zm9yIChsZXQgaiA9IDA7IGogPCBjb21wYW5pZXMubGVuZ3RoOyBqKyspIHtcclxuXHRcdFx0XHRcdGlmIChjb21wYW5pZXNbal0uY2F0ZWdvcnlJZCA9PSBjYXRlZ29yaWVzW2ldLmlkKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhbmllc1tqXS5zaG93Q2lyY2xlID0gZmFsc2U7XHJcblx0XHRcdFx0XHRcdGNvbXBhbmllc1tqXS5zaG93Q2FwdGlvbiA9IHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGZvciAobGV0IGogPSAwOyBqIDwgY29tcGFuaWVzLmxlbmd0aDsgaisrKSB7XHJcblx0XHRcdFx0XHRpZiAoY29tcGFuaWVzW2pdLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcmllc1tpXS5pZCkge1xyXG5cdFx0XHRcdFx0XHRjb21wYW5pZXNbal0uc2hvd0NpcmNsZSA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0XHRjb21wYW5pZXNbal0uc2hvd0NhcHRpb24gPSBmYWxzZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRpZiAoY3VycmVudERpdmlzaW9uID09IGlkKSB7XHJcblx0XHRcdGZvciAobGV0IGogPSAwOyBqIDwgY29tcGFuaWVzLmxlbmd0aDsgaisrKSB7XHJcblx0XHRcdFx0Y29tcGFuaWVzW2pdLnNob3dDaXJjbGUgPSBmYWxzZTtcclxuXHRcdFx0XHRjb21wYW5pZXNbal0uc2hvd0NhcHRpb24gPSBmYWxzZTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWQgPSAwO1xyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMuYXBwLnNldFN0YXRlKHtcclxuXHRcdFx0Y3VycmVudERpdmlzaW9uOiBpZFxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRjaGVja1RvcCh0b3ApIHtcclxuXHRcdGxldCBtYXhUb3AgPSAwO1xyXG5cdFx0bGV0IG1pblRvcDtcclxuXHJcblx0XHRtaW5Ub3AgXHQ9IHRoaXMuZ2V0TWluVG9wKCk7XHJcblxyXG5cdFx0aWYgKHRvcCA+IG1heFRvcCkgdG9wICA9IG1heFRvcDtcclxuXHRcdGlmICh0b3AgPCBtaW5Ub3ApIHRvcCAgPSBtaW5Ub3A7XHJcblxyXG5cdFx0cmV0dXJuIHRvcDtcclxuXHR9XHJcblxyXG5cdGdldE1pblRvcCh6b29tKSB7XHJcblx0XHRsZXQgbWluVG9wID0gMDtcclxuXHJcblx0XHRpZiAoem9vbSA9PT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdHpvb20gPSB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cdFx0fVxyXG5cclxuXHRcdG1pblRvcCA9IHRoaXMuY29udGFpbmVyLmNsaWVudEhlaWdodCAvIHpvb20gLSB0aGlzLm1hcC5jbGllbnRIZWlnaHQ7XHJcblxyXG5cdFx0cmV0dXJuIG1pblRvcDtcclxuXHR9XHJcblxyXG5cdGNoZWNrTGVmdChsZWZ0KSB7XHJcblx0XHRsZXQgbWF4TGVmdCA9IDA7XHJcblx0XHRsZXQgbWluTGVmdDtcclxuXHRcdGxldCB6b29tID0gdGhpcy5hcHAuc3RhdGUuem9vbTtcclxuXHJcblx0XHRtaW5MZWZ0ID0gdGhpcy5nZXRNaW5MZWZ0KCk7XHJcblxyXG5cdFx0aWYgKGxlZnQgPiBtYXhMZWZ0KSBsZWZ0ID0gbWF4TGVmdDtcclxuXHRcdGlmIChsZWZ0IDwgbWluTGVmdCkgbGVmdCA9IG1pbkxlZnQ7XHJcblxyXG5cdFx0cmV0dXJuIGxlZnQ7XHJcblx0fVxyXG5cclxuXHRnZXRNaW5MZWZ0KHpvb20pIHtcclxuXHRcdGxldCBtaW5MZWZ0ID0gMDtcclxuXHJcblx0XHRpZiAoem9vbSA9PT0gdW5kZWZpbmVkKSB7XHJcblx0XHRcdHpvb20gPSB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cdFx0fVxyXG5cclxuXHRcdG1pbkxlZnQgPSB0aGlzLmNvbnRhaW5lci5jbGllbnRXaWR0aCAvem9vbSAtIHRoaXMubWFwLmNsaWVudFdpZHRoO1xyXG5cclxuXHRcdHJldHVybiBtaW5MZWZ0O1xyXG5cdH1cclxuXHJcblx0cmVuZGVyKCkge1xyXG5cdFx0bGV0IHpvb20gPSB0aGlzLmFwcC5zdGF0ZS56b29tO1xyXG5cdFx0bGV0IG5ld1pvb20gPSB0aGlzLmFwcC5zdGF0ZS5uZXdab29tO1xyXG5cdFx0bGV0IGxlZnQgPSB0aGlzLmFwcC5zdGF0ZS5sZWZ0O1xyXG5cdFx0bGV0IHRvcCAgPSB0aGlzLmFwcC5zdGF0ZS50b3A7XHJcblx0XHRsZXQgY29udFdpZHRoO1xyXG5cdFx0bGV0IGNvbnRIZWlnaHQ7XHJcblx0XHRsZXQgbWFwV2lkdGg7XHJcblx0XHRsZXQgbWFwSGVpZ2h0O1xyXG5cdFx0bGV0IGNvbXBhbmllcyA9IFtdO1xyXG5cdFx0bGV0IGNhdGVnb3JpZXMgPSBbXTtcclxuXHRcdGxldCBhcmVhcyA9IFtdO1xyXG5cdFx0bGV0IHpvb21Db25kO1xyXG5cdFx0bGV0IG1hcENsYXNzZXMgPSAnbWFwJztcclxuXHJcblx0XHRpZiAoIXRoaXMuYXBwLnN0YXRlLnNob3dNYXApIHtcclxuXHRcdFx0cmV0dXJuIChcclxuXHRcdFx0XHQ8ZGl2IC8+XHJcblx0XHRcdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHRoaXMuY29udGFpbmVyICYmIHRoaXMubWFwKSB7XHJcblx0XHRcdGNvbnRXaWR0aCBcdD0gdGhpcy5jb250YWluZXIuY2xpZW50V2lkdGg7XHJcblx0XHRcdGNvbnRIZWlnaHQgXHQ9IHRoaXMuY29udGFpbmVyLmNsaWVudEhlaWdodDtcclxuXHRcdFx0bWFwV2lkdGggXHQ9IHRoaXMubWFwLmNsaWVudFdpZHRoO1xyXG5cdFx0XHRtYXBIZWlnaHQgXHQ9IHRoaXMubWFwLmNsaWVudEhlaWdodDtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAobmV3Wm9vbSA+IHpvb20pIHtcclxuXHRcdFx0aWYgKG5ld1pvb20gPiAxKSB7XHJcblx0XHRcdFx0bmV3Wm9vbSA9IHpvb207XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRpZiAobmV3Wm9vbSA8IHpvb20pIHtcclxuXHRcdFx0em9vbUNvbmQgPSAoY29udFdpZHRoIC8gKG5ld1pvb20pIDwgbWFwV2lkdGgpICYmIChjb250SGVpZ2h0IC8gKG5ld1pvb20pIDwgbWFwSGVpZ2h0KTtcclxuXHJcblx0XHRcdGlmICghem9vbUNvbmQpIHtcclxuXHRcdFx0XHRuZXdab29tID0gem9vbTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChuZXdab29tICE9IHRoaXMuYXBwLnN0YXRlLnpvb20pIHtcclxuXHRcdFx0bGVmdCA9IGNvbnRXaWR0aCAvIDIgLyBuZXdab29tIC0gKE1hdGguYWJzKGxlZnQpICsgY29udFdpZHRoIC8gMiAvIHpvb20pO1xyXG5cdFx0XHR0b3AgPSBjb250SGVpZ2h0IC8gMiAvIG5ld1pvb20gLSAoTWF0aC5hYnModG9wKSArIGNvbnRIZWlnaHQgLyAyIC8gem9vbSk7XHJcblxyXG5cdFx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7em9vbTogbmV3Wm9vbX0pO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICh0aGlzLm1hcCkge1xyXG5cdFx0XHRsZWZ0ID0gdGhpcy5jaGVja0xlZnQobGVmdCk7XHJcblx0XHRcdHRvcCAgPSB0aGlzLmNoZWNrVG9wKHRvcCk7XHJcblx0XHRcdGxlZnQgPSBsZWZ0LnRvRml4ZWQoMCkgKiAxO1xyXG5cdFx0XHR0b3AgPSB0b3AudG9GaXhlZCgwKSAqIDE7XHJcblxyXG5cdFx0XHR0aGlzLmFwcC5zdGF0ZS5sZWZ0ID0gbGVmdDtcclxuXHRcdFx0dGhpcy5hcHAuc3RhdGUudG9wICA9IHRvcDtcclxuXHRcdH1cclxuXHJcblx0XHRsZXQgc3R5bGUgPSB7XHJcblx0XHRcdHRyYW5zZm9ybTogJ3NjYWxlKCcgKyB6b29tICsgJykgdHJhbnNsYXRlWCgnICsgbGVmdCArICdweCkgdHJhbnNsYXRlWSgnICsgdG9wICsgJ3B4KSdcclxuXHRcdH1cclxuXHJcblx0XHRhcmVhcyA9IHRoaXMuYXBwLmRpdmlzaW9ucy5tYXAoYXJlYSA9PiA8TWFwQXJlYSBkYXRhID0ge2FyZWF9IGNsaWNrID0ge3RoaXMuYXJlYUNsaWNrfSAvPilcclxuXHRcdGNhdGVnb3JpZXMgPSB0aGlzLmFwcC5jYXRlZ29yaWVzLm1hcChjYXRlZ29yeSA9PiA8TWFwQnV0dG9uIGRhdGEgPSB7Y2F0ZWdvcnl9IGNsaWNrID0ge3RoaXMuY2F0Q2xpY2t9IC8+KTtcclxuXHRcdGNvbXBhbmllcyA9IHRoaXMuYXBwLnN0YXRlLmNvbXBhbmllcy5tYXAoY29tcGFueSA9PiA8Q2FyIGFwcCA9IHt0aGlzLmFwcH0gZGF0YSA9IHtjb21wYW55fSBjbGljayA9IHt0aGlzLmFwcC5zaG93UG9wdXB9IC8+KTtcclxuXHJcblx0XHRpZiAodGhpcy5hcHAuc3RhdGUubW9kZSA9PSAnbGlzdCcpIHtcclxuXHRcdFx0cmV0dXJuIChcclxuXHRcdFx0XHQ8ZGl2IC8+XHJcblx0XHRcdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0PGRpdj5cclxuXHRcdFx0XHQ8ZGl2IFxyXG5cdFx0XHRcdFx0Y2xhc3NOYW1lIFx0PSBcIm1hcC1jb250YWluZXJcIlxyXG5cdFx0XHRcdFx0b25XaGVlbCBcdD0ge3RoaXMuV2hlZWx9XHJcblx0XHRcdFx0XHRvbk1vdXNlTW92ZSA9IHt0aGlzLm1vdXNlTW92ZX1cclxuXHRcdFx0XHRcdG9uTW91c2VMZWF2ZSBcdD0ge3RoaXMubW91c2VPdXR9XHJcblx0XHRcdFx0XHRyZWYgXHRcdD0ge2NvbnRhaW5lciA9PiB0aGlzLmFwcC5jb250YWluZXIgPSB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lcn1cclxuXHRcdFx0XHQ+XHJcblx0XHRcdFx0XHQ8ZGl2XHJcblx0XHRcdFx0XHRcdGNsYXNzTmFtZSBcdD0ge21hcENsYXNzZXN9XHJcblx0XHRcdFx0XHRcdHN0eWxlIFx0XHQ9IHtzdHlsZX1cclxuXHRcdFx0XHRcdFx0b25Nb3VzZURvd24gPSB7dGhpcy5tb3VzZURvd259XHJcblx0XHRcdFx0XHRcdG9uTW91c2VVcCBcdD0ge3RoaXMubW91c2VVcH1cclxuXHRcdFx0XHRcdFx0cmVmIFx0XHQ9IHttYXAgPT4gdGhpcy5hcHAubWFwID0gdGhpcy5tYXAgPSBtYXB9XHJcblx0XHRcdFx0XHQ+XHJcblx0XHRcdFx0XHRcdHtjb21wYW5pZXN9XHJcblx0XHRcdFx0XHRcdHtjYXRlZ29yaWVzfVxyXG5cdFx0XHRcdFx0XHR7YXJlYXN9XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxMb2dvcyBcdGFwcCA9IHt0aGlzLmFwcH0gbW9kZSA9IFwibWFwXCIgLz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdH1cclxufTtcclxuXHJcbmZ1bmN0aW9uIENhcihwcm9wcykge1xyXG5cdGxldCBjYXRlZ29yaWVzID0gcHJvcHMuYXBwLmNhdGVnb3JpZXM7XHJcblx0bGV0IG9mZnNldDtcclxuXHJcblx0Zm9yIChsZXQgaSA9IDA7IGkgPCBjYXRlZ29yaWVzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRpZiAocHJvcHMuZGF0YS5jYXRlZ29yeUlkID09IGNhdGVnb3JpZXNbaV0uaWQpIHtcclxuXHRcdFx0b2Zmc2V0ID0gY2F0ZWdvcmllc1tpXS5vZmZzZXQ7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXJcIiBkYXRhLWlkID0ge3Byb3BzLmRhdGEuaWR9IG9uQ2xpY2sgPSB7cHJvcHMuY2xpY2t9ID5cclxuXHRcdFx0PENhckNpcmNsZVxyXG5cdFx0XHRcdHBvc2l0aW9uIFx0PSB7cHJvcHMuZGF0YS5wb3NpdGlvbn1cclxuXHRcdFx0XHRzaG93IFx0XHQ9IHtwcm9wcy5kYXRhLnNob3dDaXJjbGV9XHJcblx0XHRcdC8+XHJcblx0XHRcdDxDYXJJbWFnZVxyXG5cdFx0XHRcdHBvc2l0aW9uIFx0XHQ9IHtwcm9wcy5kYXRhLnBvc2l0aW9ufVxyXG5cdFx0XHRcdGltYWdlIFx0XHRcdD0ge3Byb3BzLmRhdGEuaW1hZ2V9XHJcblx0XHRcdFx0YmFzZUltYWdlRGlyIFx0PSB7cHJvcHMuYXBwLmJhc2VJbWFnZURpcn1cclxuXHRcdFx0Lz5cclxuXHRcdFx0PENhckNhcHRpb25cclxuXHRcdFx0XHRwb3NpdGlvbiBcdD0ge3Byb3BzLmRhdGEucG9zaXRpb259XHJcblx0XHRcdFx0c2hvdyBcdFx0PSB7cHJvcHMuZGF0YS5zaG93Q2FwdGlvbn1cclxuXHRcdFx0XHRvcmllbnRhdGlvbiA9IHtwcm9wcy5kYXRhLmNhcHRpb25PcmllbnRhdGlvbn1cclxuXHRcdFx0XHRuYW1lIFx0XHQ9IHtwcm9wcy5kYXRhLm5hbWV9XHJcblx0XHRcdFx0cGVyY2VudCBcdD0ge3Byb3BzLmRhdGEucGVyY2VudH1cclxuXHRcdFx0XHRmYXZvcml0ZSBcdD0ge3Byb3BzLmRhdGEuZmF2b3JpdGV9XHJcblx0XHRcdFx0b2Zmc2V0IFx0XHQ9IHtvZmZzZXR9XHJcblx0XHRcdC8+XHJcblx0XHQ8L2Rpdj5cclxuXHQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBDYXJDaXJjbGUocHJvcHMpIHtcclxuXHRsZXQgc3R5bGUgPSB7XHJcblx0XHRvcGFjaXR5OiBcdHByb3BzLnNob3cgPyAxIDogMCxcclxuXHRcdHRvcDogXHRcdHByb3BzLnBvc2l0aW9uLnRvcCxcclxuXHRcdGxlZnQ6IFx0XHRwcm9wcy5wb3NpdGlvbi5sZWZ0XHJcblx0fTtcclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXJfX2NpcmNsZVwiIHN0eWxlID0ge3N0eWxlfSA+PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gQ2FySW1hZ2UocHJvcHMpIHtcclxuXHRsZXQgZGlyID0gcHJvcHMuYmFzZUltYWdlRGlyICsgJ2NhcnMvJztcclxuXHRsZXQgc3R5bGUgXHQ9IHtcclxuXHRcdHRvcDogXHRcdFx0XHRwcm9wcy5wb3NpdGlvbi50b3AsXHJcblx0XHRsZWZ0Olx0XHRcdFx0cHJvcHMucG9zaXRpb24ubGVmdCxcclxuXHRcdGJhY2tncm91bmRJbWFnZTogXHQndXJsKCcgKyBkaXIgKyBwcm9wcy5pbWFnZSArICcpJ1xyXG5cdH07XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY2FyX19pbWFnZVwiIHN0eWxlID0ge3N0eWxlfSA+PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gQ2FyQ2FwdGlvbihwcm9wcykge1xyXG5cdGxldCBjbGFzc2VzID0gWydjYXJfX2NhcHRpb24nXTtcclxuXHRsZXQgb2Zmc2V0IFx0PSBwcm9wcy5vZmZzZXQ7XHJcblx0bGV0IHN0eWxlIFx0PSB7XHJcblx0XHR0b3A6IFx0XHRwcm9wcy5wb3NpdGlvbi50b3AsXHJcblx0XHRsZWZ0OiBcdFx0cHJvcHMucG9zaXRpb24ubGVmdCxcclxuXHRcdG9wYWNpdHk6IFx0cHJvcHMuc2hvdyA/IDEgOiAwXHJcblx0fTtcclxuXHJcblx0c3dpdGNoIChwcm9wcy5vcmllbnRhdGlvbikge1xyXG5cdFx0Y2FzZSAndGwnOlxyXG5cdFx0XHRjbGFzc2VzLnB1c2goJ2Nhcl9fY2FwdGlvbl90bCcpO1xyXG5cdFx0YnJlYWs7XHJcblx0XHRjYXNlICd0cic6XHJcblx0XHRcdGNsYXNzZXMucHVzaCgnY2FyX19jYXB0aW9uX3RyJyk7XHJcblx0XHRicmVhaztcclxuXHRcdGNhc2UgJ2JyJzpcclxuXHRcdFx0Y2xhc3Nlcy5wdXNoKCdjYXJfX2NhcHRpb25fYnInKTtcclxuXHRcdGJyZWFrO1xyXG5cdFx0Y2FzZSAnYmwnOlxyXG5cdFx0XHRjbGFzc2VzLnB1c2goJ2Nhcl9fY2FwdGlvbl9ibCcpO1xyXG5cdFx0YnJlYWs7XHJcblx0XHRkZWZhdWx0OlxyXG5cdFx0XHRjbGFzc2VzLnB1c2goJ2Nhcl9fY2FwdGlvbl90bCcpO1xyXG5cdH1cclxuXHJcblx0aWYgKG9mZnNldCkge1xyXG5cdFx0c3R5bGUudG9wICs9IG9mZnNldC50b3A7XHJcblx0XHRzdHlsZS5sZWZ0ICs9IG9mZnNldC5sZWZ0O1xyXG5cdH1cclxuXHJcblx0aWYgKHByb3BzLmZhdm9yaXRlKSB7XHJcblx0XHRjbGFzc2VzLnB1c2goJ2Nhcl9fY2FwdGlvbl9mYXZvcml0ZScpO1xyXG5cclxuXHRcdHJldHVybiAoXHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXJfX2NhcHRpb24tY29udGFpbmVyXCIgc3R5bGUgPSB7c3R5bGV9ID5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IHtjbGFzc2VzLmpvaW4oJyAnKX0gPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNhcl9fY2FwdGlvbi1jb21wYW55XCI+e3Byb3BzLm5hbWV9PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY2FyX19jYXB0aW9uLWZhdm9yaXRlXCI+PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0KTtcclxuXHR9IGVsc2Uge1xyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNhcl9fY2FwdGlvbi1jb250YWluZXJcIiBzdHlsZSA9IHtzdHlsZX0gPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0ge2NsYXNzZXMuam9pbignICcpfSA+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY2FyX19jYXB0aW9uLWNvbXBhbnlcIj57cHJvcHMubmFtZX08L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXJfX2NhcHRpb24tcGVyY2VudFwiPntwcm9wcy5wZXJjZW50fSU8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdH1cclxuXHJcbn1cclxuXHJcbmZ1bmN0aW9uIE1hcEJ1dHRvbihwcm9wcykge1xyXG5cdGxldCBzdHlsZSA9IHtcclxuXHRcdGxlZnQ6IHByb3BzLmRhdGEuYnV0dG9uUG9zaXRpb24ubGVmdCxcclxuXHRcdHRvcDogIHByb3BzLmRhdGEuYnV0dG9uUG9zaXRpb24udG9wLFxyXG5cdFx0d2lkdGg6IHByb3BzLmRhdGEuZGltZW5zaW9ucy53aWR0aCxcclxuXHRcdGhlaWdodDogcHJvcHMuZGF0YS5kaW1lbnNpb25zLmhlaWdodCxcclxuXHR9O1xyXG5cclxuXHRyZXR1cm4gKFxyXG5cdFx0PGJ1dHRvbiBjbGFzc05hbWUgPSBcImNhdGVnb3J5XCIgZGF0YS1jYXRlZ29yeS1pZCA9IHtwcm9wcy5kYXRhLmlkfSBvbkNsaWNrID0ge3Byb3BzLmNsaWNrfSBzdHlsZSA9IHtzdHlsZX0gLz5cclxuXHQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBNYXBBcmVhKHByb3BzKSB7XHJcblx0bGV0IHN0eWxlID0ge1xyXG5cdFx0dG9wOiBwcm9wcy5kYXRhLmJ1dHRvbi50b3AsXHJcblx0XHRsZWZ0OiBwcm9wcy5kYXRhLmJ1dHRvbi5sZWZ0LFxyXG5cdFx0d2lkdGg6IHByb3BzLmRhdGEuYnV0dG9uLndpZHRoLFxyXG5cdFx0aGVpZ2h0OiBwcm9wcy5kYXRhLmJ1dHRvbi5oZWlnaHRcclxuXHR9XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9ICdhcmVhJyBzdHlsZSA9IHtzdHlsZX0gZGF0YS1pZCA9IHtwcm9wcy5kYXRhLmlkfSBvbkNsaWNrID0ge3Byb3BzLmNsaWNrfSA+PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuY2xhc3MgRGl2aXNpb25zIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHRjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG5cdFx0c3VwZXIocHJvcHMpO1xyXG5cdFx0dGhpcy5hcHAgPSBwcm9wcy5hcHA7XHJcblx0fVxyXG5cclxuXHRyZW5kZXIoKSB7XHJcblx0XHRsZXQgZGl2aXNpb25zID0gdGhpcy5hcHAuZGl2aXNpb25zLm1hcChkaXZpc2lvbiA9PiA8RGl2aXNpb24gZGl2aXNpb24gPSB7ZGl2aXNpb259IGFwcCA9IHt0aGlzLmFwcH0gLz4pO1xyXG5cclxuXHRcdGlmICghdGhpcy5hcHAuc3RhdGUuc2hvd0RpdmlzaW9ucykge1xyXG5cdFx0XHRyZXR1cm4gKDxkaXYgLz4pO1xyXG5cdFx0fVxyXG5cclxuXHJcblx0XHRpZiAodGhpcy5hcHAuc3RhdGUubW9kZSAhPSAnbGlzdCcpIHtcclxuXHRcdFx0cmV0dXJuICg8ZGl2IC8+KTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImRpdmlzaW9ucy1kZXNrdG9wXCI+XHJcblx0XHRcdFx0PExvZ29zIGFwcCA9IHt0aGlzLmFwcH0gbW9kZSA9IFwibGlzdFwiIC8+XHJcblx0XHRcdFx0e2RpdmlzaW9uc31cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdH1cclxufTtcclxuXHJcbmZ1bmN0aW9uIERpdmlzaW9uKHByb3BzKSB7XHJcblx0bGV0IGNhdGVnb3JpZXMgXHQ9IFtdO1xyXG5cdGxldCBkaXZpc2lvbiBcdD0gcHJvcHMuZGl2aXNpb247XHJcblxyXG5cdHByb3BzLmFwcC5jYXRlZ29yaWVzLmZvckVhY2goY2F0ZWdvcnkgPT4ge1xyXG5cdFx0aWYgKGNhdGVnb3J5LmRpdmlzaW9uSWQgPT0gZGl2aXNpb24uaWQpIHtcclxuXHRcdFx0Y2F0ZWdvcmllcy5wdXNoKDxDYXRlZ29yeSBjb21wYW5pZXMgPSB7cHJvcHMuYXBwLnN0YXRlLmNvbXBhbmllc30gY2F0ZWdvcnkgPSB7Y2F0ZWdvcnl9IGFwcCA9IHtwcm9wcy5hcHB9IC8+KVxyXG5cdFx0fVxyXG5cdH0pO1xyXG5cdFxyXG5cdHJldHVybiAoXHJcblx0XHQ8c2VjdGlvbiBjbGFzc05hbWUgPSBcImRpdmlzaW9uXCIgaWQgPSB7J2RpdmlzaW9uLScgKyBkaXZpc2lvbi5pZH0+XHJcblx0XHRcdDxoMSAgY2xhc3NOYW1lID0gXCJkaXZpc2lvbl9faGVhZGVyXCI+e2RpdmlzaW9uLm5hbWV9PC9oMT5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImRpdmlzaW9uX19pdGVtc1wiPntjYXRlZ29yaWVzfTwvZGl2PlxyXG5cdFx0PC9zZWN0aW9uPlxyXG5cdCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIENhdGVnb3J5KHByb3BzKSB7XHJcblx0bGV0IGNhdGVnb3J5IFx0PSBwcm9wcy5jYXRlZ29yeTtcclxuXHRsZXQgY29tcGFuaWVzIFx0PSBbXTtcclxuXHJcblx0cHJvcHMuY29tcGFuaWVzLmZvckVhY2goY29tcGFueSA9PiB7XHJcblx0XHRpZiAoY29tcGFueS5jYXRlZ29yeUlkID09IGNhdGVnb3J5LmlkKSB7XHJcblx0XHRcdGNvbXBhbmllcy5wdXNoKGNvbXBhbnkpO1xyXG5cdFx0fVxyXG5cdH0pO1xyXG5cclxuXHRyZXR1cm4gKFxyXG5cdFx0PGRpdiBjbGFzc05hbWU9XCJkaXZpc2lvbl9faXRlbVwiPlxyXG5cdFx0XHQ8aDIgY2xhc3NOYW1lPVwiZGl2aXNpb25fX2l0ZW0taGVhZGVyXCI+XHJcblx0XHRcdFx0PHNwYW4gY2xhc3NOYW1lPVwiZGl2aXNpb25fX2l0ZW0taGVhZGVyLXJ1XCI+e2NhdGVnb3J5Lm5hbWVSdX08L3NwYW4+XHJcblx0XHRcdFx0PHNwYW4gY2xhc3NOYW1lPVwiZGl2aXNpb25fX2l0ZW0taGVhZGVyLWVuXCI+e2NhdGVnb3J5Lm5hbWVFbn08L3NwYW4+XHJcblx0XHRcdDwvaDI+XHJcblx0XHRcdDxDb21wYW55TGlzdCBjb21wYW5pZXMgPSB7Y29tcGFuaWVzfSBhcHAgPSB7cHJvcHMuYXBwfSAvPlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gQ29tcGFueUxpc3QocHJvcHMpIHtcclxuXHRsZXQgY29tcGFuaWVzID0gcHJvcHMuY29tcGFuaWVzLm1hcChjb21wYW55ID0+IDxDb21wYW55SXRlbSBjb21wYW55ID0ge2NvbXBhbnl9IGNsaWNrID0ge3Byb3BzLmFwcC5zaG93UG9wdXB9IC8+KTtcclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lPVwiZGl2aXNpb25fX2l0ZW0tbGlzdFwiPntjb21wYW5pZXN9PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gQ29tcGFueUl0ZW0ocHJvcHMpIHtcclxuXHRsZXQgY29tcGFueSA9IHByb3BzLmNvbXBhbnk7XHJcblx0bGV0IGNsYXNzZXMgPSAnZGl2aXNpb25fX2l0ZW0tbGluayc7XHJcblxyXG5cdGlmIChjb21wYW55LmZhdm9yaXRlKSB7XHJcblx0XHRjbGFzc2VzICs9ICcgZGl2aXNpb25fX2l0ZW0tbGlua19mYXYnO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxhIGhyZWYgPSBcIiNcIiBkYXRhLWlkID0ge2NvbXBhbnkuaWR9IGNsYXNzTmFtZSA9IHtjbGFzc2VzfSBvbkNsaWNrID0ge3Byb3BzLmNsaWNrfSA+e2NvbXBhbnkubmFtZX08L2E+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gUG9wdXAocHJvcHMpIHtcclxuXHRsZXQgYXBwID0gcHJvcHMuYXBwO1xyXG5cdGxldCBjb21wYW55ID0gYXBwLnBvcHVwQ29tcGFueSB8fCB7fTtcclxuXHRsZXQgY2F0ZWdvcnk7XHJcblx0bGV0IGRpdmlzaW9uO1xyXG5cdGxldCBjb250U3R5bGUgPSB7XHJcblx0XHRkaXNwbGF5OiBhcHAuc3RhdGUuc2hvd1BvcHVwID8gJ2ZsZXgnIDogJ25vbmUnLFxyXG5cdH07XHJcblx0bGV0IHBvcHVwU3R5bGUgPSB7XHJcblx0XHRiYWNrZ3JvdW5kSW1hZ2U6ICd1cmwoJyArIGFwcC5iYXNlSW1hZ2VEaXIgKyAnY2Fycy8nICsgY29tcGFueS5pbWFnZSArICcpJ1xyXG5cdH1cclxuXHJcblx0aWYgKCFhcHAucG9wdXBEaXZpc2lvbikge1xyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0PGRpdiAvPlxyXG5cdFx0KTtcclxuXHR9XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwicG9wdXBfX2NvbnRhaW5lclwiIHN0eWxlID0ge2NvbnRTdHlsZX0gPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwicG9wdXBfX2Nsb3NlXCIgb25DbGljayA9IHthcHAuaGlkZVBvcHVwfSA+PC9kaXY+XHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cFwiPlxyXG5cdFx0XHRcdDxidXR0b24gY2xhc3NOYW1lID0gXCJwb3B1cF9fY2xvc2UtYnV0dG9uXCIgb25DbGljayA9IHthcHAuaGlkZVBvcHVwfSA+JnRpbWVzOzwvYnV0dG9uPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cF9faW5uZXJcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cF9fbGVmdFwiIHN0eWxlID0ge3BvcHVwU3R5bGV9ID48L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cF9fcmlnaHRcIj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcInBvcHVwX19jb21wYW55XCI+e2NvbXBhbnkubmFtZX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcInBvcHVwX19jYXRzXCI+e2FwcC5wb3B1cERpdmlzaW9uLm5hbWV9L3thcHAucG9wdXBDYXRlZ29yeS5uYW1lUnV9PC9kaXY+XHJcblx0XHRcdFx0XHRcdHtjb21wYW55LnVybCAmJiA8YSBocmVmID0ge2NvbXBhbnkudXJsfSBjbGFzc05hbWUgPSBcInBvcHVwX19saW5rXCIgdGFyZ2V0ID0gXCJfYmxhbmtcIj57Y29tcGFueS51cmx9PC9hPn1cclxuXHRcdFx0XHRcdFx0eyFjb21wYW55LmZhdm9yaXRlICYmIDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cF9fcGVyY2VudFwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJwb3B1cF9fcGVyY2VudC10aXRsZVwiPlxyXG5cdFx0XHRcdFx0XHRcdFx00JTQvtC70Y8g0YHQvtGC0YDRg9C00L3QuNGH0LXRgdGC0LLQsCDQvdCwINGA0YvQvdC60LVcclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwicG9wdXBfX3BlcmNlbnQtdmFsdWVcIj57Y29tcGFueS5wZXJjZW50fSU8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+fVxyXG5cdFx0XHRcdFx0XHQ8UG9wdXBSYXRlIHJhdGUgPSB7Y29tcGFueS5yYXRlfSAvPlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gUG9wdXBSYXRlKHByb3BzKSB7XHJcblx0bGV0IHJhdGUgPSBwcm9wcy5yYXRlO1xyXG5cclxuXHRpZiAocmF0ZSA+IDApIHtcclxuXHRcdHJhdGUgPSByYXRlLnRvRml4ZWQoMikgKyAnJztcclxuXHRcdHJhdGUgPSByYXRlLnJlcGxhY2UoJy4nLCAnLCcpO1xyXG5cdH1cclxuXHJcblx0aWYgKHJhdGUpIHtcclxuXHRcdHJldHVybiAoXHJcblx0XHRcdDxkaXY+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcInBvcHVwX19yYXRlXCI+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwicG9wdXBfX3JhdGUtdGl0bGVcIj7QmtCw0YfQtdGB0YLQstC+INGB0LXRgNCy0LjRgdCwPC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwicG9wdXBfX3JhdGUtdmFsdWVcIj57cHJvcHMucmF0ZX0qPC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcInBvcHVwX19ub3RlXCI+XHJcblx0XHRcdFx0XHQq0J/QviAxMCDQsdCw0LvQu9GM0L3QvtC5INGI0LrQsNC70LUuINCf0L4g0L7RhtC10L3QutC1INC60LvQuNC10L3RgtC+0LIg0LrQvtC80L/QsNC90LjQuC5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2IC8+XHJcblx0XHQpO1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0SXRlbUJ5SWQoaXRlbXMsIGlkKSB7XHJcblx0aWYgKCFpdGVtcy5sZW5ndGgpIHJldHVybiBudWxsO1xyXG5cclxuXHRmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW1zLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRpZiAoaXRlbXNbaV0uaWQgPT0gaWQpIHtcclxuXHRcdFx0cmV0dXJuIGl0ZW1zW2ldO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuY2xhc3MgTW9iaWxlRGl2aXNpb25zIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHRjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG5cdFx0c3VwZXIocHJvcHMpO1xyXG5cdFx0dGhpcy5hcHAgXHRcdFx0XHRcdD0gcHJvcHMuYXBwO1xyXG5cdFx0dGhpcy5zaG93Q3VycmVudERpdmlzaW9uIFx0PSB0aGlzLnNob3dDdXJyZW50RGl2aXNpb24uYmluZCh0aGlzKTtcclxuXHRcdHRoaXMucmV0dXJuSG9tZSBcdFx0XHQ9IHRoaXMucmV0dXJuSG9tZS5iaW5kKHRoaXMpO1xyXG5cdFx0dGhpcy5nZXREaXZpc2lvbkN1cnJlbnRcdFx0PSB0aGlzLmdldERpdmlzaW9uQ3VycmVudC5iaW5kKHRoaXMpO1xyXG5cdH1cclxuXHJcblx0c2hvd0N1cnJlbnREaXZpc2lvbihlKSB7XHJcblx0XHRsZXQgaWQgPSBlLmN1cnJlbnRUYXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLWlkJyk7XHJcblx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7Y3VycmVudERpdmlzaW9uOiBpZH0pO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuSG9tZSgpIHtcclxuXHRcdHRoaXMuYXBwLnNldFN0YXRlKHtjdXJyZW50RGl2aXNpb246IG51bGx9KTtcclxuXHR9XHJcblxyXG5cdGdldERpdmlzaW9uQ3VycmVudCgpIHtcclxuXHRcdGxldCBkaXZpc2lvbiA9IG51bGw7XHJcblx0XHRsZXQgaWQgPSB0aGlzLmFwcC5zdGF0ZS5jdXJyZW50RGl2aXNpb247XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmFwcC5kaXZpc2lvbnMubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0aWYgKGlkID09IHRoaXMuYXBwLmRpdmlzaW9uc1tpXS5pZCkge1xyXG5cdFx0XHRcdGRpdmlzaW9uID0gdGhpcy5hcHAuZGl2aXNpb25zW2ldO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIGRpdmlzaW9uO1xyXG5cdH1cclxuXHJcblx0cmVuZGVyKCkge1xyXG5cdFx0bGV0IGRpdmlzaW9ucyA9IHRoaXMuYXBwLmRpdmlzaW9ucy5tYXAoZGl2aXNpb24gPT4ge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxNb2JpbGVEaXZpc2lvbiBkaXZpc2lvbiA9IHtkaXZpc2lvbn0gc2hvdyA9IHt0aGlzLnNob3dDdXJyZW50RGl2aXNpb259IGJhc2VJbWFnZURpciA9IHt0aGlzLmFwcC5iYXNlSW1hZ2VEaXJ9IC8+XHJcblx0XHRcdCk7XHJcblx0XHR9KTtcclxuXHJcblx0XHRpZiAoIXRoaXMuYXBwLnN0YXRlLnNob3dNb2JpbGVEaXZpc2lvbnMpIHtcclxuXHRcdFx0cmV0dXJuICg8ZGl2IC8+KTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAodGhpcy5hcHAuc3RhdGUuY3VycmVudERpdmlzaW9uID09PSBudWxsKSB7XHJcblx0XHRcdHJldHVybiAoXHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImRpdmlzaW9ucy1tb2JpbGVcIj5cclxuXHRcdFx0XHRcdHtkaXZpc2lvbnN9XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdCk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJkaXZpc2lvbnMtbW9iaWxlXCI+XHJcblx0XHRcdFx0XHQ8Q3VycmVudE1vYmlsZURpdmlzaW9uXHJcblx0XHRcdFx0XHRcdGRpdmlzaW9uIFx0PSB7dGhpcy5nZXREaXZpc2lvbkN1cnJlbnQoKX1cclxuXHRcdFx0XHRcdFx0YmFjayBcdFx0PSB7dGhpcy5yZXR1cm5Ib21lfVxyXG5cdFx0XHRcdFx0XHRhcHAgXHRcdD0ge3RoaXMuYXBwfVxyXG5cdFx0XHRcdFx0XHRkaXZpc2lvbklkIFx0PSB7dGhpcy5hcHAuc3RhdGUuY3VycmVudERpdmlzaW9ufVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0KTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIE1vYmlsZURpdmlzaW9uKHByb3BzKSB7XHJcblx0bGV0IHN0eWxlID0ge1xyXG5cdFx0YmFja2dyb3VuZENvbG9yOiBwcm9wcy5kaXZpc2lvbi5jb2xvclxyXG5cdH07XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiZGl2aXNpb24tbW9iaWxlX19jb3ZlclwiIGRhdGEtaWQgPSB7cHJvcHMuZGl2aXNpb24uaWR9IG9uQ2xpY2sgPSB7cHJvcHMuc2hvd30gPlxyXG5cdFx0XHQ8aW1nIHNyYyA9IHtwcm9wcy5iYXNlSW1hZ2VEaXIgKyBwcm9wcy5kaXZpc2lvbi5pbWFnZX0gLz5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImRpdmlzaW9uLW1vYmlsZV9fbmFtZVwiIHN0eWxlPSB7c3R5bGV9ID57cHJvcHMuZGl2aXNpb24ubmFtZX08L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEN1cnJlbnRNb2JpbGVEaXZpc2lvbihwcm9wcykge1xyXG5cdGxldCBhcHAgPSBwcm9wcy5hcHA7XHJcblx0bGV0IGJhc2VJbWFnZURpciA9IGFwcC5iYXNlSW1hZ2VEaXI7XHJcblx0bGV0IGNhdGVnb3JpZXMgPSBbXTtcclxuXHRsZXQgZGl2aXNpb25TdHlsZSA9IHtcclxuXHRcdGJhY2tncm91bmRDb2xvcjogcHJvcHMuZGl2aXNpb24uY29sb3IsXHJcblx0XHRiYWNrZ3JvdW5kSW1hZ2U6ICd1cmwoJyArIGJhc2VJbWFnZURpciArICdpY29ucy8nICsgcHJvcHMuZGl2aXNpb24uaWNvbiArICcpJyxcclxuXHRcdGJhY2tncm91bmRTaXplOiBwcm9wcy5kaXZpc2lvbi5pY29uU2l6ZVxyXG5cdH07XHJcblxyXG5cdGZvciAobGV0IGkgPSAwOyBpIDwgYXBwLmNhdGVnb3JpZXMubGVuZ3RoOyBpKyspIHtcclxuXHRcdGlmIChwcm9wcy5kaXZpc2lvbklkID09IGFwcC5jYXRlZ29yaWVzW2ldLmRpdmlzaW9uSWQpIHtcclxuXHRcdFx0Y2F0ZWdvcmllcy5wdXNoKGFwcC5jYXRlZ29yaWVzW2ldKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGNhdGVnb3JpZXMgPSBjYXRlZ29yaWVzLm1hcChjYXQgPT4ge1xyXG5cdFx0cmV0dXJuIDxNb2JpbGVDYXRlZ29yeVxyXG5cdFx0XHRkaXZpc2lvbk5hbWUgPSB7cHJvcHMuZGl2aXNpb24ubmFtZX1cclxuXHRcdFx0Y2F0ID0ge2NhdH1cclxuXHRcdFx0Y29tcGFuaWVzID0ge2FwcC5zdGF0ZS5jb21wYW5pZXN9XHJcblx0XHRcdGFwcCA9IHthcHB9XHJcblx0XHQvPlxyXG5cdH0pO1xyXG5cclxuXHRyZXR1cm4gKFxyXG5cdFx0PGRpdiBjbGFzc05hbWUgPSBcImRpdmlzaW9uLW1vYmlsZSBkaXZpc2lvbi1tb2JpbGVfY3VycmVudFwiPlxyXG5cdFx0XHQ8YnV0dG9uIGNsYXNzTmFtZSA9IFwiZGl2aXNpb24tbW9iaWxlX19iYWNrXCIgb25DbGljayA9IHtwcm9wcy5iYWNrfSBzdHlsZSA9IHtkaXZpc2lvblN0eWxlfSA+XHJcblx0XHRcdFx0PHNwYW4+e3Byb3BzLmRpdmlzaW9uLm5hbWV9PC9zcGFuPlxyXG5cdFx0XHQ8L2J1dHRvbj5cclxuXHRcdFx0e2NhdGVnb3JpZXN9XHJcblx0XHQ8L2Rpdj5cclxuXHQpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBNb2JpbGVDYXRlZ29yeShwcm9wcykge1xyXG5cdGxldCBjb21wYW5pZXMgPSBbXTtcclxuXHRsZXQgYXBwID0gcHJvcHMuYXBwO1xyXG5cclxuXHRmb3IgKGxldCBpID0gMDsgaSA8IHByb3BzLmNvbXBhbmllcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0aWYgKHByb3BzLmNhdC5pZCA9PSBwcm9wcy5jb21wYW5pZXNbaV0uY2F0ZWdvcnlJZCkge1xyXG5cdFx0XHRjb21wYW5pZXMucHVzaChwcm9wcy5jb21wYW5pZXNbaV0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Y29tcGFuaWVzID0gY29tcGFuaWVzLm1hcChjb21wYW55ID0+IHtcclxuXHRcdHJldHVybiA8TW9iaWxlQ29tcGFueSBjb21wYW55ID0ge2NvbXBhbnl9IGRpdmlzaW9uID0ge3Byb3BzLmRpdmlzaW9uTmFtZX0gY2F0ID0ge3Byb3BzLmNhdC5uYW1lUnV9IGFwcCA9IHthcHB9IC8+XHJcblx0fSk7XHJcblxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY2F0ZWdvcnktbW9iaWxlXCI+XHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXRlZ29yeS1tb2JpbGVfX2hlYWRlclwiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXRlZ29yeS1tb2JpbGVfX2hlYWRlci1ydVwiPntwcm9wcy5jYXQubmFtZVJ1fTwvZGl2PlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjYXRlZ29yeS1tb2JpbGVfX2hlYWRlci1lblwiPntwcm9wcy5jYXQubmFtZUVufTwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNhdGVnb3J5LW1vYmlsZV9fbGlzdFwiPlxyXG5cdFx0XHRcdHtjb21wYW5pZXN9XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gTW9iaWxlQ29tcGFueShwcm9wcykge1xyXG5cdGxldCBhcHAgPSBwcm9wcy5hcHA7XHJcblx0bGV0IGNvbXBhbnkgPSBwcm9wcy5jb21wYW55O1xyXG5cdGxldCBjb21wYW55Q2xhc3NlcyA9ICdjb21wYW55LW1vYmlsZSBjb21wYW55LW1vYmlsZS0nICsgY29tcGFueS5pZFxyXG5cdGxldCBjb21wYW55TmFtZUNsYXNzZXMgPSAnY29tcGFueS1tb2JpbGVfX25hbWUnO1xyXG5cdGxldCBjb21wYW55RGVzY0NsYXNzZXMgPSAnY29tcGFueS1tb2JpbGVfX2Rlc2MnO1xyXG5cdGxldCBzaG93Q29tcGFueSA9IGZhbHNlO1xyXG5cdGxldCBjdXJyZW50Q29tcGFueSA9IGFwcC5zdGF0ZS5jdXJyZW50Q29tcGFueTtcclxuXHRsZXQgbGlua1N0eWxlID0ge1xyXG5cdFx0ZGlzcGxheTogY29tcGFueS51cmwgPyAnYmxvY2snIDogJ25vbmUnXHJcblx0fTtcclxuXHJcblx0ZnVuY3Rpb24gdG9nZ2xlQ29tcGFueShlKSB7XHJcblx0XHRlLmN1cnJlbnRUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnY29tcGFueS1tb2JpbGVfX25hbWVfZXhwYW5kJyk7XHJcblx0XHRlLmN1cnJlbnRUYXJnZXQucGFyZW50Tm9kZS5xdWVyeVNlbGVjdG9yKCcuY29tcGFueS1tb2JpbGVfX2Rlc2MnKS5jbGFzc0xpc3QudG9nZ2xlKCdjb21wYW55LW1vYmlsZV9fZGVzY19leHBhbmQnKTtcclxuXHRcdGUuY3VycmVudFRhcmdldC5wYXJlbnROb2RlLmNsYXNzTGlzdC50b2dnbGUoJ2NvbXBhbnktbW9iaWxlX2V4cGFuZCcpO1xyXG5cdH1cclxuXHJcblx0ZnVuY3Rpb24gc2Nyb2xsVG9Db21wYW55KGUpIHtcclxuXHRcdGxldCBoZWFkZXJIZWlnaHQ7XHJcblx0XHRsZXQgb2Zmc2V0VG9wO1xyXG5cdFx0bGV0IGJhY2tIZWlnaHQ7XHJcblxyXG5cdFx0aWYgKCFzaG93Q29tcGFueSB8fCAhZSkge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0aGVhZGVySGVpZ2h0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhlYWRlcicpLmNsaWVudEhlaWdodDtcclxuXHRcdG9mZnNldFRvcCA9IGUub2Zmc2V0VG9wO1xyXG5cdFx0YmFja0hlaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5kaXZpc2lvbi1tb2JpbGVfX2JhY2snKS5jbGllbnRIZWlnaHQ7XHJcblxyXG5cdFx0d2luZG93LnNjcm9sbCh7XHJcblx0XHRcdHRvcDogb2Zmc2V0VG9wIC0gaGVhZGVySGVpZ2h0IC0gYmFja0hlaWdodCxcclxuXHRcdFx0bGVmdDogMCxcclxuXHRcdFx0YmVoYXZpb3I6ICdzbW9vdGgnXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGlmICghY29tcGFueS5yYXRlKSB7XHJcblx0XHRjb21wYW55LnJhdGUgPSAwO1xyXG5cdH0gZWxzZSB7XHJcblx0XHRjb21wYW55LnJhdGUgPSBwYXJzZUludChjb21wYW55LnJhdGUsIDEwKTtcclxuXHR9XHJcblxyXG5cdGlmIChjb21wYW55LmZhdm9yaXRlKSB7XHJcblx0XHRjb21wYW55TmFtZUNsYXNzZXMgKz0gJyBjb21wYW55LW1vYmlsZV9fbmFtZV9mYXZvcml0ZSc7XHJcblx0fVxyXG5cclxuXHRjb21wYW55LnJhdGUgPSBjb21wYW55LnJhdGUudG9GaXhlZCgxKSArICcnO1xyXG5cdGNvbXBhbnkucmF0ZSA9IGNvbXBhbnkucmF0ZS5yZXBsYWNlKCcuJywgJywnKTtcclxuXHJcblx0aWYgKGNvbXBhbnkuaWQgPT0gY3VycmVudENvbXBhbnkpIHtcclxuXHRcdGNvbXBhbnlDbGFzc2VzICs9ICcgY29tcGFueS1tb2JpbGVfZXhwYW5kJztcclxuXHRcdGNvbXBhbnlOYW1lQ2xhc3NlcyArPSAnIGNvbXBhbnktbW9iaWxlX19uYW1lX2V4cGFuZCc7XHJcblx0XHRjb21wYW55RGVzY0NsYXNzZXMgKz0gJyBjb21wYW55LW1vYmlsZV9fZGVzY19leHBhbmQnO1xyXG5cdFx0c2hvd0NvbXBhbnkgPSB0cnVlO1xyXG5cclxuXHRcdGFwcC5wb3B1cENvbXBhbnkgPSBudWxsO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lID0ge2NvbXBhbnlDbGFzc2VzfSA+XHJcblx0XHRcdDxidXR0b24gY2xhc3NOYW1lID0ge2NvbXBhbnlOYW1lQ2xhc3Nlc30gb25DbGljayA9IHt0b2dnbGVDb21wYW55fSByZWYgPSB7c2Nyb2xsVG9Db21wYW55fSA+XHJcblx0XHRcdFx0PHNwYW4+e2NvbXBhbnkubmFtZX08L3NwYW4+XHJcblx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IHtjb21wYW55RGVzY0NsYXNzZXN9ID5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY29tcGFueS1tb2JpbGVfX2RpdmlzaW9uXCI+e3Byb3BzLmRpdmlzaW9ufSAvIHtwcm9wcy5jYXR9PC9kaXY+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNvbXBhbnktbW9iaWxlX19tYXJrZXRcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjb21wYW55LW1vYmlsZV9fbWFya2V0LXRpdGxlXCI+0JfQsNC90LjQvNCw0LXQvNGL0Lkg0L/RgNC+0YbQtdC90YIg0YDRi9C90LrQsDwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNvbXBhbnktbW9iaWxlX19tYXJrZXQtdmFsdWVcIj57Y29tcGFueS5wZXJjZW50fSU8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY29tcGFueS1tb2JpbGVfX3JhdGVcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJjb21wYW55LW1vYmlsZV9fcmF0ZS10aXRsZVwiPtCj0LTQvtCy0LvQtdGC0LLQvtGA0LXQvdC90L7RgdGC0Ywg0YHQtdGA0LLQuNGB0L7QvDwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNvbXBhbnktbW9iaWxlX19yYXRlLXZhbHVlXCI+XHJcblx0XHRcdFx0XHRcdHtjb21wYW55LnJhdGV9LzEwXHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8YSBocmVmID0ge2NvbXBhbnkudXJsfSBjbGFzc05hbWUgPSBcImNvbXBhbnktbW9iaWxlX19saW5rXCIgc3R5bGUgPSB7bGlua1N0eWxlfSA+e2NvbXBhbnkudXJsfTwvYT5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L2Rpdj5cclxuXHQpO1xyXG59XHJcblxyXG5jbGFzcyBDb250cm9sIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHRjb25zdHJ1Y3Rvcihwcm9wcykge1xyXG5cdFx0c3VwZXIocHJvcHMpO1xyXG5cdFx0dGhpcy5sZWdlbmQgPSB0aGlzLmxlZ2VuZC5iaW5kKHRoaXMpO1xyXG5cdFx0dGhpcy56b29tID0gdGhpcy56b29tLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLm1vZGUgPSB0aGlzLm1vZGUuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMuYXBwID0gcHJvcHMuYXBwO1xyXG5cdH1cclxuXHJcblx0bGVnZW5kKCkge1xyXG5cdFx0dGhpcy5hcHAuc2V0U3RhdGUoe1xyXG5cdFx0XHRzaG93TGVnZW5kOiAhdGhpcy5hcHAuc3RhdGUuc2hvd0xlZ2VuZFxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHR6b29tKGUpIHtcclxuXHRcdGxldCB0eXBlID0gZS5jdXJyZW50VGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS10eXBlJyk7XHJcblx0XHRsZXQgc3RlcCA9IDAuMTtcclxuXHRcdGxldCB6b29tID0gdGhpcy5hcHAuc3RhdGUuem9vbTtcclxuXHRcdGxldCBuZXdab29tO1xyXG5cclxuXHRcdHN3aXRjaCAodHlwZSkge1xyXG5cdFx0XHRjYXNlICdwbHVzJzpcclxuXHRcdFx0XHRuZXdab29tID0gem9vbSArIHN0ZXA7XHJcblx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlICdtaW51cyc6XHJcblx0XHRcdFx0bmV3Wm9vbSA9IHpvb20gLSBzdGVwO1xyXG5cdFx0XHRicmVhaztcclxuXHRcdH1cclxuXHJcblx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7XHJcblx0XHRcdG5ld1pvb206IG5ld1pvb21cclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bW9kZSgpIHtcclxuXHRcdGxldCBtb2RlID0gdGhpcy5hcHAuc3RhdGUubW9kZTtcclxuXHRcdGxldCBuYXZFID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmhlYWRlcl9fbmF2LWE6bm90KC5oZWFkZXJfX25hdi1hX3NlYXJjaCknKTtcclxuXHJcblx0XHRpZiAobW9kZSA9PSAnbWFwJykge1xyXG5cdFx0XHRtb2RlID0gJ2xpc3QnO1xyXG5cdFx0XHR0aGlzLmFwcC5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0c2hvd0xlZ2VuZDogZmFsc2VcclxuXHRcdFx0fSk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRtb2RlID0gJ21hcCc7XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5hcHAuc2V0U3RhdGUoe1xyXG5cdFx0XHRtb2RlOiBtb2RlXHJcblx0XHR9KTtcclxuXHJcblx0XHRmb3IgKGxldCBpID0gMDsgaSA8IG5hdkUubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0bmF2RVtpXS5jbGFzc0xpc3QucmVtb3ZlKCdoZWFkZXJfX25hdi1hX2N1cnJlbnQnKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbmRlcigpIHtcclxuXHJcblx0XHRpZiAoaXNNb2JpbGUoKSkge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgLz5cclxuXHRcdFx0KTtcclxuXHRcdH1cclxuXHJcblx0XHRsZXQgbW9kZUNsYXNzTmFtZSA9ICdjb250cm9sX19idXR0b24gY29udHJvbF9fYnV0dG9uX21vZGUgY29udHJvbF9fYnV0dG9uX21vZGUtJyArIHRoaXMuYXBwLnN0YXRlLm1vZGU7XHJcblxyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImNvbnRyb2xcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY29udHJvbF9faW5uZXJcIj5cclxuXHRcdFx0XHRcdDxidXR0b24gY2xhc3NOYW1lID0gXCJjb250cm9sX19idXR0b24gY29udHJvbF9fYnV0dG9uX2xlZ2VuZFwiIG9uQ2xpY2sgPSB7dGhpcy5hcHAudG9nZ2xlTGVnZW5kfSA+XHJcblx0XHRcdFx0XHRcdDxzcGFuPjwvc3Bhbj5cclxuXHRcdFx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHRcdFx0PENvbnRyb2xab29tIGFwcCA9IHt0aGlzLmFwcH0gem9vbSA9IHt0aGlzLnpvb219IC8+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwiY29udHJvbF9fc3dpdGNoXCIgb25DbGljayA9IHt0aGlzLm1vZGV9ID5cclxuXHRcdFx0XHRcdFx0PGJ1dHRvbiBjbGFzc05hbWUgPSB7bW9kZUNsYXNzTmFtZX0gPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuPjwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0PC9idXR0b24+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gQ29udHJvbFpvb20ocHJvcHMpIHtcclxuXHRsZXQgYXBwID0gcHJvcHMuYXBwO1xyXG5cclxuXHRpZiAoYXBwLnN0YXRlLm1vZGUgIT0gJ21hcCcpIHtcclxuXHRcdHJldHVybiAoXHJcblx0XHRcdDxkaXYgLz5cclxuXHRcdCk7XHJcblx0fVxyXG5cclxuXHRyZXR1cm4gKFxyXG5cdFx0PGRpdiBjbGFzc05hbWUgPSBcImNvbnRyb2xfX3pvb21cIj5cclxuXHRcdFx0PGJ1dHRvbiBjbGFzc05hbWUgPSBcImNvbnRyb2xfX2J1dHRvbiBjb250cm9sX19idXR0b25fcGx1c1wiIGRhdGEtdHlwZT1cInBsdXNcIiBvbkNsaWNrID0ge3Byb3BzLnpvb219ID5cclxuXHRcdFx0XHQ8c3Bhbj48L3NwYW4+XHJcblx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHQ8YnV0dG9uIGNsYXNzTmFtZSA9IFwiY29udHJvbF9fYnV0dG9uIGNvbnRyb2xfX2J1dHRvbl9taW51c1wiIGRhdGEtdHlwZT1cIm1pbnVzXCIgb25DbGljayA9IHtwcm9wcy56b29tfSA+XHJcblx0XHRcdFx0PHNwYW4+PC9zcGFuPlxyXG5cdFx0XHQ8L2J1dHRvbj5cclxuXHRcdDwvZGl2PlxyXG5cdCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIExlZ2VuZChwcm9wcykge1xyXG5cdGxldCBhcHAgPSBwcm9wcy5hcHA7XHJcblx0bGV0IGNsYXNzZXMgPSAnbGVnZW5kJztcclxuXHJcblx0aWYgKGFwcC5zdGF0ZS5zaG93TGVnZW5kICYmICFpc01vYmlsZSgpKSB7XHJcblx0XHRjbGFzc2VzICs9ICcgbGVnZW5kX3Nob3cnO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lID0ge2NsYXNzZXN9ID5cclxuXHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faW5uZXJcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19ib2R5XCI+XHJcblx0XHRcdFx0XHQ8YnV0dG9uIGNsYXNzTmFtZSA9IFwibGVnZW5kX19jbG9zZVwiIG9uQ2xpY2sgPSB7YXBwLnRvZ2dsZUxlZ2VuZH0gPjwvYnV0dG9uPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9fbG9nb1wiPjwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faGVhZGVyXCI+XHJcblx0XHRcdFx0XHRcdFRFQ0hOT0xPR1kgTUFQIDxzcGFuPjIwMTc8L3NwYW4+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX3RleHRcIj5cclxuXHRcdFx0XHRcdFx00J3QsCDQutCw0YDRgtC1INC/0YDQtdC00YHRgtCw0LLQu9C10L3RiyDQu9C40LTQtdGA0Ysg0L/QviDQtNC+0LvQtSDRgNGL0L3QutCwINGB0YDQtdC00Lgg0L7Qv9GA0L7RiNC10L3QvdGL0YUg0LrQvtC80L/QsNC90LjQuSDQsiDRgNC10LnRgtC40L3Qs9C1IFRlY2hvbG9neSBJbmRleCAyMDE3LCDQsCDRgtCw0LrQttC1INC/0LDRgNGC0L3QtdGA0Ysg0L/RgNC+0LXQutGC0LAuXHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfc2lnbnNcIj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9fc2lnblwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX3NpZ24taWNvbiBsZWdlbmRfX3NpZ24taWNvbl9kaXZpc2lvblwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX3NpZ24tdGV4dFwiPtCh0LXQutGC0L7RgNGLINGA0YvQvdC60LA8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX3NpZ25cIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19zaWduLWljb24gbGVnZW5kX19zaWduLWljb25fY2F0ZWdvcnlcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19zaWduLXRleHRcIj7QodGE0LXRgNGLINC00LXRj9GC0LXQu9GM0L3QvtGB0YLQuCDQutC+0LzQv9Cw0L3QuNC5PC9kaXY+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19zaWduXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9fc2lnbi1pY29uIGxlZ2VuZF9fc2lnbi1pY29uX3RpdGxlXCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9fc2lnbi10ZXh0XCI+0J3QsNC30LLQsNC90LjQtSDQutC+0LzQv9Cw0L3QuNC4INC4INC00L7Qu9GPINGA0YvQvdC60LA8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX3NpZ25cIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19zaWduLWljb24gbGVnZW5kX19zaWduLWljb25fcGFydG5lcnNcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19zaWduLXRleHRcIj7Qn9Cw0YDRgtC90LXRgNGLINC/0YDQvtC10LrRgtCwPC9kaXY+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uc1wiPlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29ucy1jb2xcIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29ucy1oZWFkZXJcIj7Qn9CQ0KDQotCd0JXQoNCrINCf0KDQntCV0JrQotCQPC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbiBsZWdlbmRfX2ljb25fcDFcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9wMlwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX2ljb24gbGVnZW5kX19pY29uX3AzXCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbiBsZWdlbmRfX2ljb25fcDRcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9wNVwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX2ljb24gbGVnZW5kX19pY29uX3A2XCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbiBsZWdlbmRfX2ljb25fcDdcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9wOFwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbnMtY29sXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbnMtaGVhZGVyXCI+0JvQmNCU0JXQoNCrINCg0JXQmdCi0JjQndCT0JA8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9sMVwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX2ljb24gbGVnZW5kX19pY29uX2wyXCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbiBsZWdlbmRfX2ljb25fbDNcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9sNFwiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX2ljb24gbGVnZW5kX19pY29uX2w1XCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9faWNvbiBsZWdlbmRfX2ljb25fbDZcIj48L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IFwibGVnZW5kX19pY29uIGxlZ2VuZF9faWNvbl9sN1wiPjwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsZWdlbmRfX2ljb24gbGVnZW5kX19pY29uX2w4XCI+PC9kaXY+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWUgPSBcImxlZ2VuZF9fZm9vdGVyXCI+XHJcblx0XHRcdFx0XHTQmtCw0YDRgtCwINGB0L7Qt9C00LDQvdCwXHJcblx0XHRcdFx0XHQ8YSBocmVmPVwiaHR0cDovL2hhcHB5bGFiLnJ1L1wiIHRhcmdldD1cIl9ibGFua1wiIGNsYXNzTmFtZSA9IFwibGVnZW5kX19kZXZcIj48L2E+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufVxyXG5cclxuY2xhc3MgTG9nb3MgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG5cdGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcblx0XHRzdXBlcihwcm9wcyk7XHJcblx0XHR0aGlzLmFwcCA9IHByb3BzLmFwcDtcclxuXHRcdHRoaXMuc2xpY2sgPSB0aGlzLnNsaWNrLmJpbmQodGhpcyk7XHJcblx0XHR0aGlzLmhpZGUgPSB0aGlzLmhpZGUuYmluZCh0aGlzKTtcclxuXHRcdHRoaXMud2hlblNob3cgPSBwcm9wcy5tb2RlO1xyXG5cdH1cclxuXHJcblx0c2xpY2soZSkge1xyXG5cdFx0dGhpcy5hcHAubG9nb0NvbnRhaW5lciA9IGU7XHJcblx0XHQkKGUpLnNsaWNrKHtcclxuXHRcdFx0c2xpZGVzVG9TaG93OiA5LFxyXG5cdFx0XHRzbGlkZXNUb1Njcm9sbDogOSxcclxuXHRcdFx0YXV0b3BsYXk6IHRydWUsXHJcblx0XHRcdG5leHRBcnJvdzogJy5sb2dvX19idXR0b25fbmV4dCcsXHJcblx0XHRcdHByZXZBcnJvdzogJy5sb2dvX19idXR0b25fcHJldicsXHJcblx0XHRcdHJlc3BvbnNpdmU6IFt7XHJcblx0XHRcdFx0YnJlYWtwb2ludDogMTc1MCxcclxuXHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiA4LFxyXG5cdFx0XHRcdFx0c2xpZGVzVG9TY3JvbGw6IDhcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIHtcclxuXHRcdFx0XHRicmVha3BvaW50OiAxNjAwLFxyXG5cdFx0XHRcdHNldHRpbmdzOiB7XHJcblx0XHRcdFx0XHRzbGlkZXNUb1Nob3c6IDcsXHJcblx0XHRcdFx0XHRzbGlkZXNUb1Njcm9sbDogN1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSwge1xyXG5cdFx0XHRcdGJyZWFrcG9pbnQ6IDEyNTAsXHJcblx0XHRcdFx0c2V0dGluZ3M6IHtcclxuXHRcdFx0XHRcdHNsaWRlc1RvU2hvdzogNixcclxuXHRcdFx0XHRcdHNsaWRlc1RvU2Nyb2xsOiA2XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LCB7XHJcblx0XHRcdFx0YnJlYWtwb2ludDogMTAyNCxcclxuXHRcdFx0XHRzZXR0aW5nczoge1xyXG5cdFx0XHRcdFx0c2xpZGVzVG9TaG93OiA1LFxyXG5cdFx0XHRcdFx0c2xpZGVzVG9TY3JvbGw6IDVcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1dXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGhpZGUoKSB7XHJcblx0XHR0aGlzLmFwcC5zaG93TG9nbyA9IGZhbHNlO1xyXG5cdFx0dGhpcy5hcHAuc2V0U3RhdGUoe1xyXG5cdFx0XHRzaG93TG9nbzogZmFsc2VcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Y29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcblx0XHQkKHRoaXMuYXBwLmxvZ29Db250YWluZXIpLnNsaWNrKCd1bnNsaWNrJyk7XHJcblx0fVxyXG5cclxuXHRyZW5kZXIoKSB7XHJcblx0XHRsZXQgbG9nb3MgPSB0aGlzLmFwcC5sb2dvcy5tYXAobG9nbyA9PiA8TG9nbyBpbWFnZSA9IHtsb2dvLmltYWdlfSB1cmwgPSB7bG9nby51cmx9IGJvcmRlciA9IHtsb2dvLmJvcmRlcn0gYXBwID0ge3RoaXMuYXBwfSAvPik7XHJcblx0XHRsZXQgY2xhc3NlcztcclxuXHJcblx0XHRpZiAodGhpcy53aGVuU2hvdyA9PSAnbGlzdCcpIHtcclxuXHRcdFx0Y2xhc3NlcyA9ICdsb2dvX3N0YXRpYyc7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRjbGFzc2VzID0gJ2xvZ28nO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICghdGhpcy5hcHAuc3RhdGUuc2hvd0xvZ28pIHtcclxuXHRcdFx0aWYgKHRoaXMud2hlblNob3cgPT0gJ2xpc3QnKSB7XHJcblx0XHRcdFx0Y2xhc3NlcyArPSAnIGxvZ29faGlkZS1zdGF0aWMnO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGNsYXNzZXMgKz0gJyBsb2dvX2hpZGUnO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHRoaXMud2hlblNob3cgIT0gdGhpcy5hcHAuc3RhdGUubW9kZSkge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgLz5cclxuXHRcdFx0KTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAoaXNNb2JpbGUoKSkge1xyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgLz5cclxuXHRcdFx0KTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZSA9IHtjbGFzc2VzfSA+XHJcblx0XHRcdFx0PGJ1dHRvbiBjbGFzc05hbWUgPSBcImxvZ29fX2Nsb3NlXCIgb25DbGljayA9IHt0aGlzLmhpZGV9ID48L2J1dHRvbj5cclxuXHRcdFx0XHQ8YnV0dG9uIGNsYXNzTmFtZSA9IFwibG9nb19fYnV0dG9uIGxvZ29fX2J1dHRvbl9wcmV2XCI+XHJcblx0XHRcdFx0XHQ8c3Bhbj48L3NwYW4+XHJcblx0XHRcdFx0PC9idXR0b24+XHJcblx0XHRcdFx0PGJ1dHRvbiBjbGFzc05hbWUgPSBcImxvZ29fX2J1dHRvbiBsb2dvX19idXR0b25fbmV4dFwiPlxyXG5cdFx0XHRcdFx0PHNwYW4+PC9zcGFuPlxyXG5cdFx0XHRcdDwvYnV0dG9uPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsb2dvX19pdGVtc1wiIHJlZiA9IHt0aGlzLnNsaWNrfSA+XHJcblx0XHRcdFx0XHR7bG9nb3N9XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0KTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIExvZ28ocHJvcHMpIHtcclxuXHRsZXQgYmFzZUltYWdlRGlyID0gcHJvcHMuYXBwLmJhc2VJbWFnZURpcjtcclxuXHRsZXQgaW1hZ2UgPSBiYXNlSW1hZ2VEaXIgKyAnbG9nby8nICsgcHJvcHMuaW1hZ2U7XHJcblx0bGV0IGNsYXNzZXMgPSAnJztcclxuXHJcblx0aWYgKHByb3BzLmJvcmRlcikge1xyXG5cdFx0Y2xhc3NlcyA9ICdib3JkZXInO1xyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsb2dvX19pdGVtXCI+XHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lID0gXCJsb2dvX19pdGVtLWltYWdlXCI+XHJcblx0XHRcdFx0PGEgaHJlZiA9IHtwcm9wcy51cmwgfHwgJyMnfSB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzc05hbWUgPSB7Y2xhc3Nlc30gPlxyXG5cdFx0XHRcdFx0PGltZyBzcmMgPSB7aW1hZ2V9IC8+XHJcblx0XHRcdFx0PC9hPlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdDwvZGl2PlxyXG5cdCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGlzTW9iaWxlKCkge1xyXG5cdGxldCBtZWRpYVEgPSAnKG1heC13aWR0aDogMTAyM3B4KSc7XHJcblxyXG5cdHJldHVybiB3aW5kb3cubWF0Y2hNZWRpYShtZWRpYVEpLm1hdGNoZXNcclxufVxyXG5cclxuUmVhY3RET00ucmVuZGVyKDxBcHAgLz4sIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhcHAnKSk7Il19
